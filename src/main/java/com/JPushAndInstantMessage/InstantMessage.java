package com.JPushAndInstantMessage;

import com.google.gson.Gson;

import java.util.HashMap;

/**
 * 环信 聊天 API
 * Created by cjay on 2017/1/13.
 */
public class InstantMessage extends APIManagement {
    private static InstantMessage instance;

    private static String ORG_NAME = "1153161231115890";
    private static String APP_NAME = "zhongcaiappim";
    private static String CLIENT_ID = "YXA6iGijMM8kEeaUqyGQJc9Pjw";
    private static String CLIENT_SECRET_ID = "YXA6mqAntTfI8hTeBcFpL-4ENkqFw0Y";

    private static final String CONTENT_TYPE_TITLE = "Content-Type";
    private static final String CONTENT_TYPE_CONTENT = "application/json";
    private static String AuthorizationToken="Bear";

    public static InstantMessage getInstances(Enum_Platform whichPlatform) {
        if (instance == null) {
            instance = new InstantMessage(whichPlatform);
        }
        return instance;
    }

    public enum API_TitleType {

        TYPE_Token("token"),
        TYPE_USER("users"),;

        private String Code;

        API_TitleType(String Code) {
            this.Code = Code;
        }

        @Override
        public String toString() {
            return Code;
        }
    }

    public InstantMessage(Enum_Platform whichPlatform) {
        Common_BaseUrl = "https://a1.easemob.com/";
        RequestType = Utils_Http.RequestType.RequestTextType_JSON;

        String[] ClientName = whichPlatform.getIMClientName().split("\\|\\|\\|");
        ORG_NAME=ClientName[0].toString();
        APP_NAME=ClientName[1].toString();

        String[] ClientCode = whichPlatform.getIMClientCode().split("\\|\\|\\|");
        CLIENT_ID=ClientCode[0].toString();
        CLIENT_SECRET_ID=ClientCode[1].toString();
        AuthorizationToken = getToken();

    }

    /**
     * 取得 最新的
     * @return
     */
    private String getToken() {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_Token.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestParams = new HashMap<String, Object>();
        Common_RequestParams.put("grant_type", "client_credentials");
        Common_RequestParams.put("client_id", CLIENT_ID);
        Common_RequestParams.put("client_secret", CLIENT_SECRET_ID);
        String TokenJsonStr = SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_POST, Common_BaseUrl + Common_UrlParameter);
        if(!checkErrorReturn(TokenJsonStr)){
            Gson gs = new Gson();
            Beans_getToken tokenJS = gs.fromJson(TokenJsonStr,Beans_getToken.class);
            AuthorizationToken = tokenJS.access_token;
        }else{
            AuthorizationToken = "";
        }

        return AuthorizationToken;
    }

    /***
     * 授权注册
     * @param UserName
     * @param Password
     * @return
     */
    public String RegisterWithAuthorization(String UserName, String Password) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);


        Common_RequestParams = new HashMap<String, Object>();
        Common_RequestParams.put("username", UserName);
        Common_RequestParams.put("password", Password);

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_POST, Common_BaseUrl + Common_UrlParameter));
    }

    /**
     * 开放注册
     * @param UserName
     * @param Password
     * @param nickname
     * @return
     */
    public String RegisterWithPublic(String UserName, String Password, String nickname) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);

        Common_RequestParams = new HashMap<String, Object>();
        Common_RequestParams.put("username", UserName);
        Common_RequestParams.put("password", Password);
        Common_RequestParams.put("nickName", nickname);

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_POST, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 取得单一用户
     * @param UserName
     * @return
     */
    public String getUserInfo(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName;

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 取得多用户
     * @return
     */
    public String getUserInfo_Multi(int Limit) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();
        if(Limit >= 0){
            Common_RequestParams.put("limit",Limit);
        }

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 取得多用户
     * @param Cursor 是最后一个用户里面的节点
     * @return
     */
    public String getUserInfo_Multi(int Limit, String Cursor) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();
        if(Limit >= 0){
            Common_RequestParams.put("limit",Limit);
        }
        if(!(Cursor==null||Cursor=="")){
            Common_RequestParams.put("Cursor",Cursor);
        }

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 删除单一用户
     * @param UserName
     * @return
     */
    public String deleteUserInfo(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName;

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_DEL, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 重置单一用户密码
     * @param UserName
     * @return
     */
    public String resetUserPWD(String UserName, String UserPWD) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/"+"password";

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();
        Common_RequestParams.put("newpassword",UserPWD);

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_PUT, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 重置单一用户帐号
     * @param UserName
     * @return
     */
    public String resetUserNickName(String UserName, String UserNickname) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName;

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();
        Common_RequestParams.put("nickname",UserNickname);

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_PUT, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 与谁解除好友关系
     * @param UserName
     * @return
     */
    public String unfriendWithOthers(String UserName, String unFriendName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/contacts/"+ API_TitleType.TYPE_USER.toString()+"/"+unFriendName;

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_DEL, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 查看好友
     * @param UserName
     * @return
     */
    public String checkContactList(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/contacts/"+ API_TitleType.TYPE_USER.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 查看封锁名单
     * @param UserName
     * @return
     */
    public String checkBlockList(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/blocks/"+ API_TitleType.TYPE_USER.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }


    /***
     * 查看封锁名单
     * @param UserName
     * @return
     */
    public String addIntoBlockList(String UserName, String[] BlockingList) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/blocks/"+ API_TitleType.TYPE_USER.toString();

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();
        Common_RequestParams.put("usernames",BlockingList);

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_POST, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 查看封锁名单
     * @param UserName
     * @return
     */
    public String unBlockUser(String UserName, String unBlockUser) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/blocks/"+ API_TitleType.TYPE_USER.toString()+"/"+unBlockUser;

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_DEL, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 查看用户再线
     * @param UserName
     * @return
     */
    public String isUserOnline(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/status";

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 查看用户离线消息数
     * @param UserName
     * @return
     */
    public String getOfflineMSGCount(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/offline_msg_count";

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }


    /***
     * 查看用户离线消席状态
     * @param UserName
     * @return
     */
    public String getOfflineMSGStatus(String UserName, String MSG_ID) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/offline_msg_status/"+MSG_ID;

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 禁用帐号
     * @param UserName
     * @return
     */
    public String deactivateAccount(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/deactivate";

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_POST, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 起用帐号
     * @param UserName
     * @return
     */
    public String activateAccount(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/activate";

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_POST, Common_BaseUrl + Common_UrlParameter));
    }

    /***
     * 起用帐号
     * @param UserName
     * @return
     */
    public String disConnectAccount(String UserName) {
        Common_UrlParameter = ORG_NAME + "/" + APP_NAME + "/" + API_TitleType.TYPE_USER.toString()+ "/"+UserName+"/disconnect";

        Common_RequestHeaders = new HashMap<String, String>();
        Common_RequestHeaders.put(CONTENT_TYPE_TITLE, CONTENT_TYPE_CONTENT);
        Common_RequestHeaders.put("Authorization", "Bearer "+AuthorizationToken);

        Common_RequestParams = new HashMap<String, Object>();

        return ToCommonJsonForm(SendingHttpRequest(APIManagement.HTTP_TYPE.HTTP_TYPE_GET, Common_BaseUrl + Common_UrlParameter));
    }
    /***
     * 统一格式
     * @param ResponseString
     * @return
     */
    private String ToCommonJsonForm(String ResponseString){
        Beans_CommonResponse CommonResponse = new Beans_CommonResponse();
        Gson gson = new Gson();
        if(checkErrorReturn(ResponseString)){
            ResponseString = ResponseString.replace("Server returned HTTP response code: ","");
            CommonResponse.code = Integer.parseInt(ResponseString.substring(0,3).trim());
            CommonResponse.message = "错误请参见 代码";
            return gson.toJson(CommonResponse,Beans_CommonResponse.class);
        }else{
            CommonResponse.code = 200;
            CommonResponse.message = "Success";
            String JsonString = gson.toJson(CommonResponse,Beans_CommonResponse.class);
            JsonString = JsonString.replace("\"message\":\"Success\"", "\"message\":\"Success\", \"data\":"+ResponseString);
            return JsonString;
        }
    }

    private boolean checkErrorReturn(String Json){
        if(Json.contains("Server returned HTTP response code:")){
            return true;
        }else{
            return false;
        }
    }
}
