package com.JPushAndInstantMessage;



import java.util.HashMap;

public abstract class APIManagement {
	public enum HTTP_TYPE{
		HTTP_TYPE_POST("POST"),HTTP_TYPE_GET("GET"),HTTP_TYPE_PUT("PUT"),HTTP_TYPE_DEL("DELETE");

		private String Code;
		HTTP_TYPE(String Code){
			this.Code = Code;
		}

		@Override
		public String toString(){
			return Code;
		}
	}

	public static Utils_Http.RequestType RequestType= Utils_Http.RequestType.RequestTextType_TX;

	protected static String Common_BaseUrl = "";
	protected static String Common_LaunchGameUrl = "";
	public static String Common_UrlParameter = "";

	public static int Common_ConnectTimeout = 30000;
	public static int Common_ReadTimeout = 30000;

	public static HashMap<String, Object> Common_RequestParams = new HashMap<String, Object>();
	public static HashMap<String, String> Common_RequestHeaders = new HashMap<String, String>();

	public static String SendingHttpRequest(HTTP_TYPE HttpType, String TotalUrl) {
		String HttpResponse = "";
		Utils_Http Internet = Utils_Http.getInstance(TotalUrl,RequestType);
		System.out.println("SendingHttpRequest_____HttpType = " + HttpType);
		System.out.println("SendingHttpRequest_____TotalUrl = " + TotalUrl);
		
		switch (HttpType) {
		case HTTP_TYPE_POST:
			HttpResponse = Internet.Post(Common_RequestHeaders, Common_RequestParams, Common_ConnectTimeout,
					Common_ReadTimeout);
			break;
		case HTTP_TYPE_GET:
			HttpResponse = Internet.Get(Common_RequestHeaders, Common_RequestParams, Common_ConnectTimeout,
					Common_ReadTimeout);
			break;
		case HTTP_TYPE_PUT:
			HttpResponse = Internet.Put(Common_RequestHeaders, Common_RequestParams, Common_ConnectTimeout,
					Common_ReadTimeout);
			break;
		case HTTP_TYPE_DEL:
			HttpResponse = Internet.Del(Common_RequestHeaders, Common_RequestParams, Common_ConnectTimeout,
					Common_ReadTimeout);
			break;
		}
		System.out.println("HttpResponse = " + HttpResponse);
		return HttpResponse;
	}
}
