package com.AutoBetPlan.beans;



import java.util.List;

/**
 * 挂机 计画 内容
 * 前端 将会传送 挂机 计画的JSON 格式到后台
 * 后台 将方案加上 CODE 后 存放于 数据库
 * Created by cjay on 2017/3/21.
 */
public class API_BetPlanInfo {

    /**
     * userid : 111
     * username : androidtest
     * gameCategory : SSC
     * gameType : FFC1
     * startTime : 2017-03-23 12:00:00
     * endTime : 2017-03-24 20:00:00
     * autoPlanData : [{"planCode":"","methodCode":"1DP","content":"{\"type\":\">\",\"value\":5}","type":"MISS_NUMBER","name":"遗漏方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\"type\":\"ZHENG_TOU\",\"detail\":[{\"number\":0,\"fanTou\":\"67890\",\"zhengTou\":\"12345\"},{\"number\":1,\"fanTou\":\"78901\",\"zhengTou\":\"23456\"},{\"number\":2,\"fanTou\":\"89012\",\"zhengTou\":\"34567\"},{\"number\":3,\"fanTou\":\"90123\",\"zhengTou\":\"45678\"},{\"number\":4,\"fanTou\":\"01234\",\"zhengTou\":\"56789\"},{\"number\":5,\"fanTou\":\"12345\",\"zhengTou\":\"67890\"},{\"number\":6,\"fanTou\":\"23456\",\"zhengTou\":\"78901\"},{\"number\":7,\"fanTou\":\"34567\",\"zhengTou\":\"89012\"},{\"number\":8,\"fanTou\":\"45678\",\"zhengTou\":\"90123\"},{\"number\":9,\"fanTou\":\"56789\",\"zhengTou\":\"01234\"}]}","type":"KMTM_NUMBER","name":"開某开某投某方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\"numbers\":\"1234233431313257\"}","type":"FIX_NUMBER","name":"固定方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"##1DP","content":"{\"numbers\":100}","type":"RANDOM_NUMBER","name":"隨機方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000}]
     */

    private String userid; //用户ID
    private String username;//用户名
    private String gameCategory;//彩种组SSC
    private String gameType;//彩种类型
    private String startTime;//方案 开始时间
    private String endTime;//方案 结束时间
    private List<AutoPlanDataBean> autoPlanData;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGameCategory() {
        return gameCategory;
    }

    public void setGameCategory(String gameCategory) {
        this.gameCategory = gameCategory;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public List<AutoPlanDataBean> getAutoPlanData() {
        return autoPlanData;
    }

    public void setAutoPlanData(List<AutoPlanDataBean> autoPlanData) {
        this.autoPlanData = autoPlanData;
    }

    public static class AutoPlanDataBean {
        /**
         * planCode :
         * methodCode : 1DP
         * content : {"type":">","value":5}
         * type : MISS_NUMBER
         * name : 遗漏方案
         * mode : 0
         * multi : 1,2,4,8,16
         * win : 20000
         * lose : 20000
         */

        private String planCode; //方案CODE ！！请后台添加
        private String methodCode; //方案 玩法
        private String content;//方案 内容
        private String type;//方案 分析类型， 随机选号，开某投某 等
        private String name;//方案名称
        private int mode;//方案 圆角分
        private String multi;//方案 倍数 倍投，高级倍投
        private int win; //方案盈亏 上限
        private int lose;

        public String getPlanCode() {
            return planCode;
        }

        public void setPlanCode(String planCode) {
            this.planCode = planCode;
        }

        public String getMethodCode() {
            return methodCode;
        }

        public void setMethodCode(String methodCode) {
            this.methodCode = methodCode;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getMode() {
            return mode;
        }

        public void setMode(int mode) {
            this.mode = mode;
        }

        public String getMulti() {
            return multi;
        }

        public void setMulti(String multi) {
            this.multi = multi;
        }

        public int getWin() {
            return win;
        }

        public void setWin(int win) {
            this.win = win;
        }

        public int getLose() {
            return lose;
        }

        public void setLose(int lose) {
            this.lose = lose;
        }
    }
}
