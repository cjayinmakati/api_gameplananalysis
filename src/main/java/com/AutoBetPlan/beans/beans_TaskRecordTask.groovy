package com.AutoBetPlan.beans

/**
 * Created by cjay on 2017/3/21.
 */
class beans_TaskRecordTask {
    boolean isFinishCalculate
    int TaskList_BetTimes = 0   //全体 方案投注次数

    int TaskItem_WON      //方案盈亏上下限
    int TaskItem_LOSE

    String TaskItem_Name   //方案名
    String TaskItem_Code   //方案CODE 由 后台生成后 传至 API
    String TaskItem_Type   // 方案类型

    String TaskItem_MethodCode // 方案玩法
    String TaskItem_MethodContent  //方案内容
    int TaskItem_Mode    //方案 圆角分
    String TaskItem_Multi  //倍投，

    def TaskItem_betInfo = new beans_TaskRecord_BetInfo() //方案分析后 生成 的 投注注单
}
