package com.AutoBetPlan.beans

/**
 * Created by cjay on 2017/3/21.
 */
class beans_TaskRecordList {

    /***
     * 每一个方案的资讯
     */

    String TaskList_StartTime     //全体 方案开始及结束
    String TaskList_EndTime

    String TaskList_UserID  //用户ID
    String TaskList_UserName
    String TaskList_gameCategory //采种名及采种
    String TaskList_gameType

    Map<String, beans_TaskRecordTask> TaskList_ItemInfoList = new HashMap<String, beans_TaskRecordTask>()// 方案列表
}
