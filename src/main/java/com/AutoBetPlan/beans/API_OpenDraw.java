package com.AutoBetPlan.beans;

import java.util.List;

/**
 * Created by cjay on 2017/3/21.
 */
public class API_OpenDraw {
        /**
         * gameId : 1
         * name : 重慶時時彩
         * category : SSC
         * offSaleAdvance : 10
         * type : CQSSC
         * defaultMethodName : 直选复式
         * defaultMethodCode : ##3DC
         * periodList : [{"signature":"20170323949","numbers":"4,3,3,5,1","nowtime":"2017-03-23 10:00:00","openTime":"2017-03-23 15:49:00","statuscode":2},{"signature":"20170323950","numbers":"1,4,7,3,2","nowtime":"2017-03-23 10:00:00","openTime":"2017-03-23 14:00:00","statuscode":0},{"signature":"20170323951","numbers":null,"nowtime":"2017-03-23 13:30:41","openTime":"2017-03-23 16:51:00","statuscode":0}]
         */

        private int gameId;
        private String name;
        private String category;
        private int offSaleAdvance;
        private String type;
        private String defaultMethodName;
        private String defaultMethodCode;
        private List<PeriodListBean> periodList;

        public int getGameId() {
            return gameId;
        }

        public void setGameId(int gameId) {
            this.gameId = gameId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public int getOffSaleAdvance() {
            return offSaleAdvance;
        }

        public void setOffSaleAdvance(int offSaleAdvance) {
            this.offSaleAdvance = offSaleAdvance;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDefaultMethodName() {
            return defaultMethodName;
        }

        public void setDefaultMethodName(String defaultMethodName) {
            this.defaultMethodName = defaultMethodName;
        }

        public String getDefaultMethodCode() {
            return defaultMethodCode;
        }

        public void setDefaultMethodCode(String defaultMethodCode) {
            this.defaultMethodCode = defaultMethodCode;
        }

        public List<PeriodListBean> getPeriodList() {
            return periodList;
        }

        public void setPeriodList(List<PeriodListBean> periodList) {
            this.periodList = periodList;
        }

        public static class PeriodListBean {
            /**
             * signature : 20170323949
             * numbers : 4,3,3,5,1
             * nowtime : 2017-03-23 10:00:00
             * openTime : 2017-03-23 15:49:00
             * statuscode : 2
             */

            private String signature;
            private String numbers;
            private String nowtime;
            private String openTime;
            private int statuscode;

            public String getSignature() {
                return signature;
            }

            public void setSignature(String signature) {
                this.signature = signature;
            }

            public String getNumbers() {
                return numbers;
            }

            public void setNumbers(String numbers) {
                this.numbers = numbers;
            }

            public String getNowtime() {
                return nowtime;
            }

            public void setNowtime(String nowtime) {
                this.nowtime = nowtime;
            }

            public String getOpenTime() {
                return openTime;
            }

            public void setOpenTime(String openTime) {
                this.openTime = openTime;
            }

            public int getStatuscode() {
                return statuscode;
            }

            public void setStatuscode(int statuscode) {
                this.statuscode = statuscode;
            }
        }

}
