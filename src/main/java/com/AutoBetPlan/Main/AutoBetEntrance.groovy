package com.AutoBetPlan.Main


import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_SSC
import com.AutoBetPlan.beans.API_BetPlanInfo
import com.AutoBetPlan.beans.API_OpenDraw
import com.AutoBetPlan.beans.beans_TaskRecordList
import com.AutoBetPlan.beans.beans_TaskRecord_BetInfo
import com.common.enumList.Enum_GameDefine
import com.google.gson.Gson
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method


import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.TEXT

/**
 * Created by cjay on 2017/3/21.
 */

/***
 * 挂机软件 入口
 */
@Singleton
class AutoBetEntrance {
    /***
     * 用户所有 注单 资讯
     */
    Map<String, API_BetPlanInfo> userPlanInfoMap = new HashMap<>()
    API_OpenDraw openDrawList = new API_OpenDraw()
    /***
     * 依照用户注单资讯所产生的
     */
    Map<String, beans_TaskRecordList> AllUserTaskInfo = new HashMap<>()

    static void main(args) {
        /***
         *
         * 当用户 传送 下方JSON格式时，后台给予 plancode并传入到 挂机API后
         * 加上 至少 12期以上的开奖记录
         * API 就会开始去计算该用户 在这一期 要投注的注单内容
         *
         * 后台只要在 当期投注截止前
         * 将结果 正确投注即可
         *
         * 当奖期截止时
         * 调用下方接口更新奖期
         *
         */
        def BetInfoA = """{"userid":"111","username":"androidtest","gameCategory":"SSC","gameType":"SD11X5","startTime":"2017-03-27 12:00:00","endTime":"2017-03-28 20:00:00","autoPlanData":[{"planCode":"","methodCode":"1DP_W","content":"{\\"type\\":\\">\\",\\"value\\":5}","type":"MISS_NUMBER","name":"遗漏方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP_Q","content":"{\\"type\\":\\"ZHENG_TOU\\",\\"detail\\":[{\\"number\\":0,\\"fanTou\\":\\"6 7 8 9 0\\",\\"zhengTou\\":\\"1 2 3 4 5\\"},{\\"number\\":1,\\"fanTou\\":\\"7 8 9 0 1\\",\\"zhengTou\\":\\"2 3 4 5 6\\"},{\\"number\\":2,\\"fanTou\\":\\"8 9 0 1 2\\",\\"zhengTou\\":\\"3 4 5 6 7\\"},{\\"number\\":3,\\"fanTou\\":\\"9 0 1 2 3\\",\\"zhengTou\\":\\"4 5 6 7 8\\"},{\\"number\\":4,\\"fanTou\\":\\"0 1 2 3 4\\",\\"zhengTou\\":\\"5 6 7 8 9\\"},{\\"number\\":5,\\"fanTou\\":\\"1 2 3 4 5\\",\\"zhengTou\\":\\"6 7 8 9 0\\"},{\\"number\\":6,\\"fanTou\\":\\"2 3 4 5 6\\",\\"zhengTou\\":\\"7 8 9 0 1\\"},{\\"number\\":7,\\"fanTou\\":\\"3 4 5 6 7\\",\\"zhengTou\\":\\"8 9 0 1 2\\"},{\\"number\\":8,\\"fanTou\\":\\"4 5 6 7 8\\",\\"zhengTou\\":\\"9 0 1 2 3\\"},{\\"number\\":9,\\"fanTou\\":\\"5 6 7 8 9\\",\\"zhengTou\\":\\"0 1 2 3 4\\"}]}","type":"KMTM_NUMBER","name":"開某开某投某方案","mode":0,"multi":"2,8,16,32,64","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\\"numbers\\":\\"1234233431313257\\"}","type":"FIX_NUMBER","name":"固定方案","mode":0,"multi":"3,6,9,12","win":20000,"lose":20000},{"planCode":"","methodCode":"##3DS","content":"{\\"numbers\\":100}","type":"RANDOM_NUMBER","name":"隨機方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000}]}"""
        def BetInfoB = """{"userid":"222","username":"androidtest1950","gameCategory":"SSC","gameType":"FFC2","startTime":"2017-03-27 12:00:00","endTime":"2017-03-28 20:00:00","autoPlanData":[{"planCode":"","methodCode":"1DP_W","content":"{\\"type\\":\\">\\",\\"value\\":5}","type":"MISS_NUMBER","name":"遗漏方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP_Q","content":"{\\"type\\":\\"ZHENG_TOU\\",\\"detail\\":[{\\"number\\":0,\\"fanTou\\":\\"6 7 8 9 0\\",\\"zhengTou\\":\\"1 2 3 4 5\\"},{\\"number\\":1,\\"fanTou\\":\\"7 8 9 0 1\\",\\"zhengTou\\":\\"2 3 4 5 6\\"},{\\"number\\":2,\\"fanTou\\":\\"8 9 0 1 2\\",\\"zhengTou\\":\\"3 4 5 6 7\\"},{\\"number\\":3,\\"fanTou\\":\\"9 0 1 2 3\\",\\"zhengTou\\":\\"4 5 6 7 8\\"},{\\"number\\":4,\\"fanTou\\":\\"0 1 2 3 4\\",\\"zhengTou\\":\\"5 6 7 8 9\\"},{\\"number\\":5,\\"fanTou\\":\\"1 2 3 4 5\\",\\"zhengTou\\":\\"6 7 8 9 0\\"},{\\"number\\":6,\\"fanTou\\":\\"2 3 4 5 6\\",\\"zhengTou\\":\\"7 8 9 0 1\\"},{\\"number\\":7,\\"fanTou\\":\\"3 4 5 6 7\\",\\"zhengTou\\":\\"8 9 0 1 2\\"},{\\"number\\":8,\\"fanTou\\":\\"4 5 6 7 8\\",\\"zhengTou\\":\\"9 0 1 2 3\\"},{\\"number\\":9,\\"fanTou\\":\\"5 6 7 8 9\\",\\"zhengTou\\":\\"0 1 2 3 4\\"}]}","type":"KMTM_NUMBER","name":"開某开某投某方案","mode":0,"multi":"2,8,16,32,64","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\\"numbers\\":\\"1234233431313257\\"}","type":"FIX_NUMBER","name":"固定方案","mode":0,"multi":"3,6,9,12","win":20000,"lose":20000},{"planCode":"","methodCode":"##3DS","content":"{\\"numbers\\":100}","type":"RANDOM_NUMBER","name":"隨機方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000}]}"""
        def BetInfoC = """{"userid":"333","username":"androidtest1900","gameCategory":"SSC","gameType":"CQSSC","startTime":"2017-03-27 12:00:00","endTime":"2017-03-28 20:00:00","autoPlanData":[{"planCode":"","methodCode":"1DP_W","content":"{\\"type\\":\\">\\",\\"value\\":5}","type":"MISS_NUMBER","name":"遗漏方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP_Q","content":"{\\"type\\":\\"ZHENG_TOU\\",\\"detail\\":[{\\"number\\":0,\\"fanTou\\":\\"6 7 8 9 0\\",\\"zhengTou\\":\\"1 2 3 4 5\\"},{\\"number\\":1,\\"fanTou\\":\\"7 8 9 0 1\\",\\"zhengTou\\":\\"2 3 4 5 6\\"},{\\"number\\":2,\\"fanTou\\":\\"8 9 0 1 2\\",\\"zhengTou\\":\\"3 4 5 6 7\\"},{\\"number\\":3,\\"fanTou\\":\\"9 0 1 2 3\\",\\"zhengTou\\":\\"4 5 6 7 8\\"},{\\"number\\":4,\\"fanTou\\":\\"0 1 2 3 4\\",\\"zhengTou\\":\\"5 6 7 8 9\\"},{\\"number\\":5,\\"fanTou\\":\\"1 2 3 4 5\\",\\"zhengTou\\":\\"6 7 8 9 0\\"},{\\"number\\":6,\\"fanTou\\":\\"2 3 4 5 6\\",\\"zhengTou\\":\\"7 8 9 0 1\\"},{\\"number\\":7,\\"fanTou\\":\\"3 4 5 6 7\\",\\"zhengTou\\":\\"8 9 0 1 2\\"},{\\"number\\":8,\\"fanTou\\":\\"4 5 6 7 8\\",\\"zhengTou\\":\\"9 0 1 2 3\\"},{\\"number\\":9,\\"fanTou\\":\\"5 6 7 8 9\\",\\"zhengTou\\":\\"0 1 2 3 4\\"}]}","type":"KMTM_NUMBER","name":"開某开某投某方案","mode":0,"multi":"2,8,16,32,64","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\\"numbers\\":\\"1234233431313257\\"}","type":"FIX_NUMBER","name":"固定方案","mode":0,"multi":"3,6,9,12","win":20000,"lose":20000},{"planCode":"","methodCode":"##3DS","content":"{\\"numbers\\":100}","type":"RANDOM_NUMBER","name":"隨機方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000}]}"""
        def BetInfoD = """{"userid":"444","username":"androidtest1956","gameCategory":"SSC","gameType":"FFC2","startTime":"2017-03-27 12:00:00","endTime":"2017-03-28 20:00:00","autoPlanData":[{"planCode":"","methodCode":"1DP_W","content":"{\\"type\\":\\">\\",\\"value\\":5}","type":"MISS_NUMBER","name":"遗漏方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP_Q","content":"{\\"type\\":\\"ZHENG_TOU\\",\\"detail\\":[{\\"number\\":0,\\"fanTou\\":\\"6 7 8 9 0\\",\\"zhengTou\\":\\"1 2 3 4 5\\"},{\\"number\\":1,\\"fanTou\\":\\"7 8 9 0 1\\",\\"zhengTou\\":\\"2 3 4 5 6\\"},{\\"number\\":2,\\"fanTou\\":\\"8 9 0 1 2\\",\\"zhengTou\\":\\"3 4 5 6 7\\"},{\\"number\\":3,\\"fanTou\\":\\"9 0 1 2 3\\",\\"zhengTou\\":\\"4 5 6 7 8\\"},{\\"number\\":4,\\"fanTou\\":\\"0 1 2 3 4\\",\\"zhengTou\\":\\"5 6 7 8 9\\"},{\\"number\\":5,\\"fanTou\\":\\"1 2 3 4 5\\",\\"zhengTou\\":\\"6 7 8 9 0\\"},{\\"number\\":6,\\"fanTou\\":\\"2 3 4 5 6\\",\\"zhengTou\\":\\"7 8 9 0 1\\"},{\\"number\\":7,\\"fanTou\\":\\"3 4 5 6 7\\",\\"zhengTou\\":\\"8 9 0 1 2\\"},{\\"number\\":8,\\"fanTou\\":\\"4 5 6 7 8\\",\\"zhengTou\\":\\"9 0 1 2 3\\"},{\\"number\\":9,\\"fanTou\\":\\"5 6 7 8 9\\",\\"zhengTou\\":\\"0 1 2 3 4\\"}]}","type":"KMTM_NUMBER","name":"開某开某投某方案","mode":0,"multi":"2,8,16,32,64","win":20000,"lose":20000},{"planCode":"","methodCode":"1DP","content":"{\\"numbers\\":\\"1234233431313257\\"}","type":"FIX_NUMBER","name":"固定方案","mode":0,"multi":"3,6,9,12","win":20000,"lose":20000},{"planCode":"","methodCode":"##3DS","content":"{\\"numbers\\":100}","type":"RANDOM_NUMBER","name":"隨機方案","mode":0,"multi":"1,2,4,8,16","win":20000,"lose":20000}]}"""

        def OpenDraw = """{"gameId":6,"name":"台北两分彩","category":"SSC","offSaleAdvance":20,"type":"FFC2","defaultMethodName":"直选复式","defaultMethodCode":"##3DC","periodList":[{"signature":"20170328302","numbers":"5,3,1,6,0","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:03:00","statuscode":2},{"signature":"20170328303","numbers":"4,9,7,8,7","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:05:00","statuscode":2},{"signature":"20170328304","numbers":"6,1,4,8,8","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:07:00","statuscode":2},{"signature":"20170328305","numbers":"9,2,1,6,4","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:09:00","statuscode":2},{"signature":"20170328306","numbers":"9,9,4,8,7","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:11:00","statuscode":2},{"signature":"20170328307","numbers":"3,9,4,7,2","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:13:00","statuscode":2},{"signature":"20170328308","numbers":"3,4,9,0,4","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:15:00","statuscode":2},{"signature":"20170328309","numbers":"3,4,9,3,0","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:17:00","statuscode":2},{"signature":"20170328310","numbers":"0,0,6,9,4","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:19:00","statuscode":2},{"signature":"20170328311","numbers":"4,7,0,1,0","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:21:00","statuscode":2},{"signature":"20170328312","numbers":"1,7,7,8,7","nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:23:00","statuscode":2},{"signature":"20170328313","numbers":null,"nowtime":"2017-03-28 10:23:28","openTime":"2017-03-28 10:25:00","statuscode":0}]}"""
        Gson gs = new Gson()
        API_OpenDraw openDrawItem = gs.fromJson(OpenDraw, API_OpenDraw.class)

        /***
         * 加入 用户的挂机资料 上方BET INFO是指一个人的挂机注单
         * MAP KEY 用户ID
         */
        Map<String, API_BetPlanInfo> testData_userPlanInfoMap = new HashMap<>()

        API_BetPlanInfo userPlanInfoMapItemA = gs.fromJson(BetInfoA, API_BetPlanInfo.class)
        API_BetPlanInfo userPlanInfoMapItemB = gs.fromJson(BetInfoB, API_BetPlanInfo.class)
        API_BetPlanInfo userPlanInfoMapItemC = gs.fromJson(BetInfoC, API_BetPlanInfo.class)
        API_BetPlanInfo userPlanInfoMapItemD = gs.fromJson(BetInfoD, API_BetPlanInfo.class)

        testData_userPlanInfoMap.put(userPlanInfoMapItemA.getUserid(), userPlanInfoMapItemA)
        testData_userPlanInfoMap.put(userPlanInfoMapItemB.getUserid(), userPlanInfoMapItemB)
        testData_userPlanInfoMap.put(userPlanInfoMapItemC.getUserid(), userPlanInfoMapItemC)
        testData_userPlanInfoMap.put(userPlanInfoMapItemD.getUserid(), userPlanInfoMapItemD)

        /***
         * 进入点
         */
        def en = AutoBetEntrance.instance
        /***
         * 先初始化 传入 目前各彩种 奖期openDrawList 和 所有用户 注单userPlanInfoMap
         */
        en.initAllData(openDrawItem, testData_userPlanInfoMap)

        /***
         * 初始化后 去计算 所有用户的注单 并会生成一个MAP－－－>AllUserTaskInfo
         * 当奖期截止前，取出并投注即可
         */
        en.createUserBetMap()

        /**
         * 取出投注的接口
         */
        Map<String, beans_TaskRecordList> AllUserTaskInfo = en.getUserBetInfoMap()
        String js = gs.toJson(AllUserTaskInfo)
        println(js)

        /***
         * 取得目前支持 投注的 玩法 及方案 并生成MAP
         */

        def currentSupport = en.getCurrentSupportMethodList("SSC")
        print(currentSupport.get("RANDOM_NUM").toString())
    }

    /***
     * 初始化 所有资料
     * @param opendrawMap 奖期资料
     * @param userPlanInfoMap 投注资料
     */
    void initAllData(API_OpenDraw opendrawMap, Map<String, API_BetPlanInfo> userPlanInfoMap) {
        setOpenDrawInfo(opendrawMap)
        setBetTaskFromUser(userPlanInfoMap)
    }

    /***
     * ---------------------------------------------------查询取得目前 支持方案 的接口---------------------------------------------------
     */


    HashMap<String, ArrayList<String>> getCurrentSupportMethodList(String gameCategory) {
        switch (Enum_GameDefine.getEnumNameByCode(gameCategory)) {
            case Enum_GameDefine.GAME_SSC:
                Enum_BallGame_SSC ssc = Enum_BallGame_SSC.instance
                return ssc.getSupportAutoBetMethod()
            case Enum_GameDefine.GAME_11X5:
                break
            case Enum_GameDefine.GAME_3DP3:
                break
            case Enum_GameDefine.GAME_PK10:
                break
            case Enum_GameDefine.GAME_K3:
                break
            default:
                Enum_BallGame_SSC ssc = Enum_BallGame_SSC.instance
                return ssc.getSupportAutoBetMethod()
        }
    }

    /***
     * ---------------------------------------------------挂机 任务 新增修改区---------------------------------------------------
     */
    /***
     *  设定 目前用户 掛單任务
     */
    void setBetTaskFromUser(Map<String, API_BetPlanInfo> userPlanInfoMap) {
        this.userPlanInfoMap = userPlanInfoMap
    }

    /***
     * 取得 所有用户 掛單任务
     */
    Map<String, API_BetPlanInfo> getBetTaskFromUser() {
        return userPlanInfoMap
    }

    /***
     * 更改 單一用户 掛單任务
     */
    void setBetTaskFromUser_Singal(API_BetPlanInfo newPlanInfo) {
        userPlanInfoMap.put(newPlanInfo.getUserid(), newPlanInfo)
    }

    /***
     * 取得 单一 用户 掛單任务
     */
    API_BetPlanInfo getBetTaskFromUser_Singal(String userID) {
        return userPlanInfoMap.get(userID)
    }

    /***
     *  强制停止 所有用户 掛單任务
     */
    void setBetTaskFromUser() {
        this.userPlanInfoMap.clear()
    }
    /***
     *  强制停止 该用户 掛單任务 （当 时间 到时 或是 盈亏 达到 最高 或 最低标 时，请传入并停止该用户的 挂机单)
     */
    void stopBetTaskFromUser_Singal(String userID) {
        userPlanInfoMap.remove(userID)
    }

    /***
     * ---------------------------------------------------所有彩种 奖期资讯接口 更新区---------------------------------------------------
     */
    /***
     * 更新 所有彩种 奖期资讯接口
     * **************************(当开奖后 请立即更新该接口)**************************
     */
    void setOpenDrawInfo(API_OpenDraw openDrawList) {
        this.openDrawList = openDrawList
    }

    /***
     * ---------------------------------------------------取得 用户挂机 分析出 投注注单 MAP---------------------------------------------------
     */

    /***
     * 请在当前 奖期 截止前 取得用户挂机资讯， 并 依照里面的 模式投注
     * MAP KEY = USER ID
     * MAP VALUE = USER INFO
     */
    Map<String, beans_TaskRecordList> getUserBetInfoMap() {
        return AllUserTaskInfo
    }

    /***
     * 依照用户的 挂机资讯，开奖奖期 去分析并生成 用户当期 注单
     */
    void createUserBetMap() {
        if (userPlanInfoMap) {
            userPlanInfoMap.each { userID, userInfo ->
//                String currentBetGameType = openDrawList.getType()
//                API_OpenDraw.PeriodListBean openDrawPeriodInfo = openDrawInfo.getPeriodList().get(openDrawInfo.getPeriodList().size() - 2)

                /**
                 * 如果非当下彩种 就不去计算投注
                 */
                if (userInfo.getGameType() == openDrawList.getType()) {
                    if (!AllUserTaskInfo.get(userID)) {
                        /**
                         * 剛開始，尚無投注記錄，写入挂单资讯到 beans_TaskRecordList
                         *
                         */
                        def UserTaskList = new beans_TaskRecordList()
                        UserTaskList.with {
                            setTaskList_UserID(userInfo.getUserid())
                            setTaskList_UserName(userInfo.getUsername())
                            setTaskList_gameCategory(userInfo.getGameCategory())
                            setTaskList_gameType(userInfo.getGameType())
                            setTaskList_StartTime(userInfo.getStartTime())
                            setTaskList_EndTime(userInfo.getEndTime())
                        }

                        AnalysisBetNum analysisbetInfo = AnalysisBetNum.instance
                        AllUserTaskInfo.put(userID, analysisbetInfo.initData(userInfo, openDrawList, UserTaskList))
                    } else {
                        def UserTaskList = AllUserTaskInfo.get(userID)
                        AnalysisBetNum analysisbetInfo = AnalysisBetNum.instance
                        AllUserTaskInfo.put(userID, analysisbetInfo.initData(userInfo, openDrawList, UserTaskList))
                    }
                } else {
                    println(userInfo.getUsername() + " 投注彩种为" + userInfo.getGameType() + "不去计算")
                }

            }
        }

        /***
         * 当分析完后，开始进行 投注作业
         */
        if (AllUserTaskInfo) {
            AllUserTaskInfo.each { userID, userBetInfo ->
                userBetInfo.getTaskList_ItemInfoList()

                /**
                 * 如果非当下彩种 就不去计算投注
                 */
                if (userBetInfo.getTaskList_gameType() == openDrawList.getType()) {
                    userBetInfo.getTaskList_ItemInfoList().each { planCode, planInfo ->
                        if (planInfo.isFinishCalculate) {
                            def betInfo = planInfo.getTaskItem_betInfo() as beans_TaskRecord_BetInfo
                            def originalNumbers = betInfo.originalNumbers
                            def period = betInfo.period
                            def brokerage = betInfo.brokerage
                            def gameType = betInfo.game
                            def methodType = betInfo.method
                            def mode = betInfo.mode
                            def multi = betInfo.multi
                            def http = new HTTPBuilder('http://cdn.sxzcwj.com')
                            String json = "[{\"originalNumbers\":\"${originalNumbers}\",\"game\":\"${gameType}\",\"period\":\"${period}\",\"method\":\"${methodType}\",\"multi\":${multi},\"mode\":${mode},\"brokerage\":${brokerage}}]"
                            http.request(Method.POST) {
                                uri.path = '/api/ticket/batchAdd'
                                body = [json: json]
                                requestContentType = ContentType.URLENC
                                response.success = { resp ->
                                    println "Tweet response status: ${resp.statusLine}"
                                    assert resp.statusLine.statusCode == 200
                                }
                            }

                            planInfo.isFinishCalculate = false
                        }
                    }
                } else {
                    println(userBetInfo.getTaskList_UserName() + " 投注彩种为" + userBetInfo.getTaskList_gameType() + "不投注")
                }
            }
        }
    }
}
