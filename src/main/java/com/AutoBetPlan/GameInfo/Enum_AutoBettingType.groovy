package com.AutoBetPlan.GameInfo


/***
 * 方案种类
 */
 enum Enum_AutoBettingType {
    TYPE_RANDOM_NUM("随机出号", "RANDOM_NUMBER", true),
    TYPE_MISSING_NUM("遗漏出号", "MISS_NUMBER", true),
    TYPE_KMTM_OPEN("开某投某", "KMTM_NUMBER", true),
    TYPE_FIX_NUM("固定选号", "FIX_NUMBER", true),
    TYPE_ANALYSIS_PLAN("外接计画", "ANALYSIS_PLAN", true)


    private String Code
    private String Name
    private boolean isEnable

    Enum_AutoBettingType(String Name, String Code, boolean isEnable) {
        this.Code = Code
        this.Name = Name
        this.isEnable = isEnable
    }

     String getCode() {
        return Code
    }

     String getName() {
        return Name
    }

     boolean isEnable(){
         return isEnable
     }

    /***
     * 用code找名字 并核对返回到
     *
     * @param TypeCode
     * @return
     */
     static Enum_AutoBettingType getEnumNameByCode(String TypeCode) {

        for (Enum_AutoBettingType v : values())
            if (v.getCode().equalsIgnoreCase(TypeCode)) {
                return v
            }

        return TYPE_RANDOM_NUM
    }
}
