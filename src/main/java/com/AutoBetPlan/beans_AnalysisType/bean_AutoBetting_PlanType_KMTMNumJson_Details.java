package com.AutoBetPlan.beans_AnalysisType;

/**
 * Created by cjay on 2017/3/17.
 */

/***
 * 方案内容，开某投某 选号细节
 */
public class bean_AutoBetting_PlanType_KMTMNumJson_Details {
    /**
     * number : 1
     * fanTou : 67890
     * zhengTou : 12345
     */

    private int number;
    private String fanTou;
    private String zhengTou;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getFanTou() {
        return fanTou;
    }

    public void setFanTou(String fanTou) {
        this.fanTou = fanTou;
    }

    public String getZhengTou() {
        return zhengTou;
    }

    public void setZhengTou(String zhengTou) {
        this.zhengTou = zhengTou;
    }
}
