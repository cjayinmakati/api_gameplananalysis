package com.AutoBetPlan.GameMethodAnalysis

import com.AutoBetPlan.GameInfo.Enum_AutoBettingType

import com.AutoBetPlan.analysis.Analysis_DanShiNumRandom
import com.AutoBetPlan.beans.API_OpenDraw
import com.AutoBetPlan.beans.beans_TaskRecordTask
import com.AutoBetPlan.beans.beans_TaskRecord_BetInfo
import com.AutoBetPlan.beans_AnalysisType.*
import com.common.enumList.Enum_Type_SSC
import groovy.json.JsonSlurper

/***
 * 时时彩 方案计算中心
 * 依照 方案类型，玩法 和其他 参数 来决定 方案投注的内容
 */
@Singleton
class Enum_BallGame_SSC {

    static final LinkedHashMap<String, Boolean> BETTYPE_NONE = [
            RANDOM_NUMBER: false,
            MISS_NUMBER  : false,
            KMTM_NUMBER  : false,
            FIX_NUMBER   : false,
            ANALYSIS_PLAN: false
    ]
    static final LinkedHashMap<String, Boolean> BETTYPE_DIN_WEI_DAN = [
            RANDOM_NUMBER: false,
            MISS_NUMBER  : true,
            KMTM_NUMBER  : true,
            FIX_NUMBER   : true,
            ANALYSIS_PLAN: false
    ]

    static final LinkedHashMap<String, Boolean> BETTYPE_DAN_SHI = [
            RANDOM_NUMBER: true,
            MISS_NUMBER  : false,
            KMTM_NUMBER  : false,
            FIX_NUMBER   : true,
            ANALYSIS_PLAN: false
    ]

    static final LinkedHashMap<String, Boolean> BETTYPE_FU_SHI = [
            RANDOM_NUMBER: false,
            MISS_NUMBER  : false,
            KMTM_NUMBER  : false,
            FIX_NUMBER   : false,
            ANALYSIS_PLAN: false
    ]


    private API_OpenDraw IssueInfo
    private API_OpenDraw.PeriodListBean currentOpenDrawInfo
    private API_OpenDraw.PeriodListBean LastestOpenDrawInfo
    private String GameType
    private String TaskAnalysisType
    private int TaskBetTimes
    def currentTask = new beans_TaskRecordTask()

    void initData(String GameType, API_OpenDraw IssueInfo, beans_TaskRecordTask currentTask) {
        this.TaskBetTimes = TaskBetTimes
        this.GameType = GameType
        this.IssueInfo = IssueInfo
        this.currentTask = currentTask
        LastestOpenDrawInfo = IssueInfo.periodList.get(IssueInfo.periodList.size() - 2)
        currentOpenDrawInfo = IssueInfo.periodList.get(IssueInfo.periodList.size() - 1)
        TaskAnalysisType = currentTask.getTaskItem_Type()
    }

    /***
     * 更新方案 投注内容
     * @param currentTask
     * @return
     */
    def getBetInfoInCurrentTask() {
        def MethodCode = currentTask.getTaskItem_MethodCode()
        def newBetCode = new beans_TaskRecord_BetInfo()
        newBetCode.with {
            setBrokerage(0)
            setGame(GameType)
            setMethod(MethodCode)
            setOriginalNumbers(calCulatorBetInfo_OraginNumber(MethodCode, TaskAnalysisType))
            setMulti(calCulatorBetInfo_BeiShu())
            setMode(currentTask.getTaskItem_Mode())
            setPeriod(IssueInfo.periodList.get(IssueInfo.periodList.size() - 1).signature)
        }
        return newBetCode
    }

    /**
     * 计算倍投 高级倍投
     * @return
     */
    private int calCulatorBetInfo_BeiShu() {
        String[] multiArray = currentTask.getTaskItem_Multi().split(",")
        int SizeOfUserMulti = multiArray.length

        if (SizeOfUserMulti >= TaskBetTimes) {
            return Integer.valueOf(multiArray[TaskBetTimes])
        } else {
            return Integer.valueOf(multiArray[TaskBetTimes % SizeOfUserMulti])
        }

    }

    /***
     * 依据不同 方案种类 及 玩法 产生投注号码
     * @param singlePlanItem
     * @param MethodCode
     * @param AnalysisType
     */
    private String calCulatorBetInfo_OraginNumber(String MethodCode, String AnalysisType) {
        switch (Enum_AutoBettingType.getEnumNameByCode(AnalysisType)) {
            case Enum_AutoBettingType.TYPE_RANDOM_NUM:
                getAnalysisNum_Random(MethodCode)
                break
            case Enum_AutoBettingType.TYPE_MISSING_NUM:
                getAnalysisNum_Missing(MethodCode)
                break
            case Enum_AutoBettingType.TYPE_KMTM_OPEN:
                getAnalysisNum_KMTM(MethodCode)
                break
            case Enum_AutoBettingType.TYPE_FIX_NUM:
                getAnalysisNum_Fix(MethodCode)
                break
        }
    }
    /***
     * 遺漏開號
     * 根據 前几期开奖开出的 遗漏值 提出号码
     * @param singlePlanItem
     * @param MethodCode
     * @return
     */
    private String getAnalysisNum_Missing(String MethodCode) {
        println("MISSING " + currentTask.getTaskItem_MethodContent().toString())
        def content = currentTask.getTaskItem_MethodContent()

        def JsonObj = new JsonSlurper().parseText(content)
        def MissingNumInfo = new bean_AutoBetting_PlanType_MissingNumJson(JsonObj)

        int DinWeiDanLevel
        switch (Enum_Type_SSC.getEnumNameByCode(MethodCode)) {
            case Enum_Type_SSC.YiXing_DingWeiDan_WAN:
                DinWeiDanLevel = 0
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_QIAN:
                DinWeiDanLevel = 1
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_BAI:
                DinWeiDanLevel = 2
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_SHI:
                DinWeiDanLevel = 3
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_GE:
                DinWeiDanLevel = 4
                break
        }
        def missingArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for (i in 0..IssueInfo.getPeriodList().size() - 1) {
            def numbers = IssueInfo.getPeriodList().get(i).getNumbers()
            if (numbers) {
                def numArray = numbers.split(",")
                for (j in 0..missingArr.size() - 1) {
                    missingArr[j] += 1
                }
                missingArr[Integer.parseInt(numArray[DinWeiDanLevel])] = 0
            }
        }

        def typeSimple = MissingNumInfo.type.toString()
        def amount = MissingNumInfo.value
        StringBuffer sb = new StringBuffer()
        for (k in 0..missingArr.size()) {
            switch (typeSimple) {
                case "<":
                    if (missingArr[k] < amount) {
                        sb.append(k)
                        sb.append(",")
                    }
                    break
                case "<=":
                    if (missingArr[k] <= amount) {
                        sb.append(k)
                        sb.append(",")
                    }
                    break
                case ">":
                    if (missingArr[k] > amount) {
                        sb.append(k)
                        sb.append(",")
                    }
                    break
                case ">=":
                    if (missingArr[k] >= amount) {
                        sb.append(k)
                        sb.append(",")
                    }
                    break
                case "=":
                    if (missingArr[k] == amount) {
                        sb.append(k)
                        sb.append(",")
                    }
                    break
            }
        }
        return sb.toString().substring(0, sb.toString().length() - 1)

    }

    /***
     * 开某投某，根据上一期的 开出位数的奖期
     * 来决定这一期将要开出哪一些号码
     * @param singlePlanItem
     * @param MethodCode
     * @return
     */
    private String getAnalysisNum_KMTM(String MethodCode) {
        def content = currentTask.getTaskItem_MethodContent()
        println("KMTM " + content)

        String LastestOpenNum = LastestOpenDrawInfo.getNumbers()
        String[] openNumArray = LastestOpenNum.split(",")

        def JsonObj = new JsonSlurper().parseText(content)
        def KMTMNumInfo = new bean_AutoBetting_PlanType_KMTMNumJson(JsonObj)

        String type = KMTMNumInfo.type
        ArrayList<bean_AutoBetting_PlanType_KMTMNumJson_Details> DetailsList = KMTMNumInfo.detail

        String OriganNum = ""

        switch (Enum_Type_SSC.getEnumNameByCode(MethodCode)) {
            case Enum_Type_SSC.YiXing_DingWeiDan_WAN:
                int WanPosition = Integer.parseInt(openNumArray[0])
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(WanPosition).zhengTou : DetailsList.get(WanPosition).fanTou)

                OriganNum = "" + OriganNum.replace(" ", ",") + "||||"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_QIAN:
                int QianPosition = Integer.parseInt(openNumArray[1])
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(QianPosition).zhengTou : DetailsList.get(QianPosition).fanTou)

                OriganNum = "|" + OriganNum.replace(" ", ",") + "|||"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_BAI:
                int BaiPosition = Integer.parseInt(openNumArray[2])
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(BaiPosition).zhengTou : DetailsList.get(BaiPosition).fanTou)

                OriganNum = "||" + OriganNum.replace(" ", ",") + "||"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_SHI:
                int ShiPosition = Integer.parseInt(openNumArray[3])
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(ShiPosition).zhengTou : DetailsList.get(ShiPosition).fanTou)

                OriganNum = "|||" + OriganNum.replace(" ", ",") + "|"
                break
            case Enum_Type_SSC.YiXing_DingWeiDan_GE:
                int GePosition = Integer.parseInt(openNumArray[4])
                OriganNum = (type == "ZHENG_TOU" ? DetailsList.get(GePosition).zhengTou : DetailsList.get(GePosition).fanTou)

                OriganNum = "||||" + OriganNum.replace(" ", ",") + ""
                break
        }
        return OriganNum
    }

    /***
     * 随机出号 单式依据 用户需求的位数 来 投注
     * @param singlePlanItem
     * @param MethodCode
     * @return
     */
    private String getAnalysisNum_Random(String MethodCode) {
        def content = currentTask.getTaskItem_MethodContent()
        println("RandomNum_" + content)

        def JsonObj = new JsonSlurper().parseText(content)
        def RandomNumInfo = new bean_AutoBetting_PlanType_RandomNumJson(JsonObj)

        def RandomTotalAmount = RandomNumInfo.numbers

        String OriganNum = ""
        Analysis_DanShiNumRandom rand = new Analysis_DanShiNumRandom()
        switch (Enum_Type_SSC.getEnumNameByCode(MethodCode)) {
            case Enum_Type_SSC.ZhongSan_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 3)
                break
            case Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 2)
                break
            case Enum_Type_SSC.ErXing_QianErDanShi_ZhiXuan:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 2)
                break
            case Enum_Type_SSC.WuXing_DanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 5)
                break
            case Enum_Type_SSC.QianSan_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 3)
                break
            case Enum_Type_SSC.HouSan_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 3)
                break
            case Enum_Type_SSC.SiXing_ZhiXuanDanShi:
                OriganNum = rand.DanShiRandom(RandomTotalAmount, 4)
                break
            default:
                break
        }
        return OriganNum
    }

    /***
     * 固定开号，每一期 依据 用户号码 去 开号
     * @param singlePlanItem
     * @return
     */
    private String getAnalysisNum_Fix(String MethodCode) {
        def content = currentTask.getTaskItem_MethodContent()
        println("FIX_Num" + content)

        def JsonObj = new JsonSlurper().parseText(content)
        def FixNumInfo = new bean_AutoBetting_PlanType_FixNumJson(JsonObj)

        return FixNumInfo.numbers
    }

    /***
     * 取得目前支持的 方案 玩法列表
     * @return
     */
    HashMap<String, ArrayList<String>> getSupportAutoBetMethod() {
        HashMap<String, ArrayList<String>> supportMethodList = new HashMap<>()

        supportMethodList.put("RANDOM_NUMBER", new ArrayList<>())
        supportMethodList.put("MISS_NUMBER", new ArrayList<>())
        supportMethodList.put("KMTM_NUMBER", new ArrayList<>())
        supportMethodList.put("FIX_NUMBER", new ArrayList<>())
        supportMethodList.put("ANALYSIS_PLAN", new ArrayList<>())

        for (Enum_Type_SSC v : Enum_Type_SSC.values()) {
            LinkedHashMap<String, Boolean> supportItem = v.getSupportAutoBetMethod()

            supportItem.each { PlanType, SSCValue ->
                if (SSCValue) {
                    supportMethodList.get(PlanType).add(v.getMethodCode())
                }
            }
        }
        return supportMethodList
    }

}
