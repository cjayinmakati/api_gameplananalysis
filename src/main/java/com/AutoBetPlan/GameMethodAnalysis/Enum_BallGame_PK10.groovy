package com.AutoBetPlan.GameMethodAnalysis

import com.common.enumList.Enum_Type_PK10;


class Enum_BallGame_PK10 {
    static final LinkedHashMap<String, Boolean> BETTYPE_NONE = [
            RANDOM_NUMBER: false,
            MISS_NUMBER  : false,
            KMTM_NUMBER  : false,
            FIX_NUMBER   : false,
            ANALYSIS_PLAN: false
    ]
    static final LinkedHashMap<String, Boolean> BETTYPE_DIN_WEI_DAN = [
            RANDOM_NUMBER: false,
            MISS_NUMBER  : true,
            KMTM_NUMBER  : true,
            FIX_NUMBER   : true,
            ANALYSIS_PLAN: false
    ]

    static final LinkedHashMap<String, Boolean> BETTYPE_DAN_SHI = [
            RANDOM_NUMBER: true,
            MISS_NUMBER  : false,
            KMTM_NUMBER  : false,
            FIX_NUMBER   : true,
            ANALYSIS_PLAN: false
    ]

    static final LinkedHashMap<String, Boolean> BETTYPE_FU_SHI = [
            RANDOM_NUMBER: false,
            MISS_NUMBER  : false,
            KMTM_NUMBER  : false,
            FIX_NUMBER   : false,
            ANALYSIS_PLAN: false
    ]
    static void test(String GameCategory, String MethodCode) {
       switch (Enum_Type_PK10.getEnumNameByCode(MethodCode)) {

       }

   }

}
