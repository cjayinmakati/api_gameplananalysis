package com.common.enumList

import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_PK10

enum Enum_Type_PK10 {

    YiXing_DingWeiDan_1("DP_1", "冠军", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_2("DP_2", "亚军", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_3("DP_3", "季军", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_4("DP_4", "第四名", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_5("DP_5", "第五名", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_6("DP_6", "第六名", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_7("DP_7", "第七名", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_8("DP_8", "第八名", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_9("DP_9", "第九名", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_10("DP_10", "第十名", Enum_BallGame_PK10.BETTYPE_NONE),

    YiXing_DingWeiDan_QianWu("DP", "定位胆-前五", Enum_BallGame_PK10.BETTYPE_NONE),
    YiXing_DingWeiDan_HouWu("DP", "定位胆-后五", Enum_BallGame_PK10.BETTYPE_NONE), //网页上的是DP

    QianSan_DanShi("3*DS", "前三-单式", Enum_BallGame_PK10.BETTYPE_NONE),
    QianSan_FuShi("3*EDC", "前三-复式", Enum_BallGame_PK10.BETTYPE_NONE),

    QianEr_DanShi("2*DS", "前二-单式", Enum_BallGame_PK10.BETTYPE_NONE),
    QianEr_FuShi("2*EDC", "前二-复式", Enum_BallGame_PK10.BETTYPE_NONE),

    CaiDanShuang_CaiDanShuang1("GO1", "猜单双_冠军", Enum_BallGame_PK10.BETTYPE_NONE),
    CaiDanShuang_CaiDanShuang2("GO2", "猜单双_亚军", Enum_BallGame_PK10.BETTYPE_NONE),
    CaiDanShuang_CaiDanShuang3("GO3", "猜单双_季军", Enum_BallGame_PK10.BETTYPE_NONE),

    CaiDaXiao_CaiDaXiao1("GB1", "猜大小_冠军", Enum_BallGame_PK10.BETTYPE_NONE),
    CaiDaXiao_CaiDaXiao2("GB2", "猜大小_亚军", Enum_BallGame_PK10.BETTYPE_NONE),
    CaiDaXiao_CaiDaXiao3("GB3", "猜大小_季军", Enum_BallGame_PK10.BETTYPE_NONE),

    ShouWei_FuShi("1*DC", "", Enum_BallGame_PK10.BETTYPE_NONE)

    public String CurrentMethodCode
    public String CurrentMethodName
    public LinkedHashMap<String, Boolean> isSupportAutoBetMethod

    Enum_Type_PK10(String MethodCode, String MethodName, LinkedHashMap<String, Boolean> isSupportAutoBetMethod) {
        this.CurrentMethodCode = MethodCode
        this.CurrentMethodName = MethodName
        this.isSupportAutoBetMethod = isSupportAutoBetMethod
    }

    String getMethodCode() {
        return CurrentMethodCode
    }

    String getMethodName() {
        return CurrentMethodName
    }

    HashMap<String, Boolean> getSupportAutoBetMethod() {
        return isSupportAutoBetMethod
    }

    @Override
    String toString() {
        return this.getMethodCode()
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    static Enum_Type_PK10 getEnumNameByCode(String PlanCode) {

        for (Enum_Type_PK10 v : Enum_Type_PK10.values()) {
            if (v.toString().equalsIgnoreCase(PlanCode)) {
                return v
            }
        }
        return YiXing_DingWeiDan_QianWu
    }
}
