package com.common.enumList

import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_11X5
enum Enum_Type_11X5 {

	DingWeiDan_DingWeiDan_FIRTH("DP_1", "定位胆", Enum_BallGame_11X5.BETTYPE_NONE),
	DingWeiDan_DingWeiDan_SECOND("DP_2", "定位胆",Enum_BallGame_11X5.BETTYPE_NONE),
	DingWeiDan_DingWeiDan_THIRD("DP_3", "定位胆",Enum_BallGame_11X5.BETTYPE_NONE),
	DingWeiDan_DingWeiDan_FOURTH("DP_4", "定位胆",Enum_BallGame_11X5.BETTYPE_NONE),
	DingWeiDan_DingWeiDan_FIFTH("DP_5", "定位胆",Enum_BallGame_11X5.BETTYPE_NONE),

	BuDingWei_HouSanBuDingWei("*3F", "后三-不定位", Enum_BallGame_11X5.BETTYPE_NONE),
	BuDingWei_QianSanBuDingWei("F", "前三-不定位", Enum_BallGame_11X5.BETTYPE_NONE),

	ErMa_QianErZhiXuanDanShi("2*DS", "前2星-直选单式", Enum_BallGame_11X5.BETTYPE_NONE),
	ErMa_QianErZhiXuanFuShi("2*EDC", "前2星-直选复式", Enum_BallGame_11X5.BETTYPE_NONE),
	ErMa_QianErZuXuanFuShi("2*GC", "前2星-组选复式", Enum_BallGame_11X5.BETTYPE_NONE),
	ErMa_QianErZuXuanDanTuo("2*GK", "前2星-组选胆拖", Enum_BallGame_11X5.BETTYPE_NONE),
	ErMa_QianErZuXuanDanShi("2*GS", "前2星-组选单式", Enum_BallGame_11X5.BETTYPE_NONE),

	RenXuanDanShi_RenXuanYiZhongYi("1O1S", "任选单式-一中一", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanShi_RenXuanErZhongEr("2O2S", "任选单式-二中二", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanShi_RenXuanSanZhongSan("3O3S", "任选单式-三中三", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanShi_RenXuanSiZhongSi("4O4S", "任选单式-四中四", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanShi_RenXuanWuZhongWu("5O5S", "任选单式-五中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanShi_RenXuanLiuZhongWu("5O6S", "任选单式-六中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanShi_RenXuanQiZhongWu("5O7S", "任选单式-七中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanShi_RenXuanBaZhongWu("5O8S", "任选单式-八中五", Enum_BallGame_11X5.BETTYPE_NONE),

	RenXuanFuShi_RenXuanYiZhongYi("1O1C", "任选复式-一中一", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanFuShi_RenXuanErZhongEr("2O2C", "任选复式-二中二", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanFuShi_RenXuanSanZhongSan("3O3C", "任选复式-三中三", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanFuShi_RenXuanSiZhongSi("4O4C", "任选复式-四中四", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanFuShi_RenXuanWuZhongWu("5O5C", "任选复式-五中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanFuShi_RenXuanLiuZhongWu("5O6C", "任选复式-六中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanFuShi_RenXuanQiZhongWu("5O7C", "任选复式-七中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanFuShi_RenXuanBaZhongWu("5O8C", "任选复式-八中五", Enum_BallGame_11X5.BETTYPE_NONE),

	RenXuanDanTuo_RenXuanYiZhongYi("2O2K", "任选胆拖-一中一", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanTuo_RenXuanErZhongEr("2O2K", "任选胆拖-二中二", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanTuo_RenXuanSanZhongSan("3O3K", "任选胆拖-三中三", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanTuo_RenXuanSiZhongSi("4O4K", "任选胆拖-四中四", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanTuo_RenXuanWuZhongWu("5O5K", "任选胆拖-五中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanTuo_RenXuanLiuZhongWu("5O6K", "任选胆拖-六中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanTuo_RenXuanQiZhongWu("5O7K", "任选胆拖-七中五", Enum_BallGame_11X5.BETTYPE_NONE),
	RenXuanDanTuo_RenXuanBaZhongWu("5O8K", "任选胆拖-八中五", Enum_BallGame_11X5.BETTYPE_NONE),

	QianSan_QianSanZhiXuanDanShi("3##DS", "前三-直选单式", Enum_BallGame_11X5.BETTYPE_NONE),
	QianSan_QianSanZhiXuanFuShi("3##EDC", "前三-直选复式", Enum_BallGame_11X5.BETTYPE_NONE),
	QianSan_QianSanZuXuanFuShi("3##GC", "前三-组选复式", Enum_BallGame_11X5.BETTYPE_NONE),
	QianSan_QianSanZuXuanDanTuo("3##GK","前三-組选胆拖", Enum_BallGame_11X5.BETTYPE_NONE),
	QianSan_QianSanZuXuanDanShi("3##GS","前三-组选单式", Enum_BallGame_11X5.BETTYPE_NONE),

	HouSan_HouSanZhiXuanDanShi("##3DS", "后三-直选单式", Enum_BallGame_11X5.BETTYPE_NONE),
	HouSan_HouSanZhiXuanFuShi("##3EDC", "后三-直选复式", Enum_BallGame_11X5.BETTYPE_NONE),
	HouSan_HouSanZuXuanFuShi("##3GC", "后三-组选复式", Enum_BallGame_11X5.BETTYPE_NONE),
	HouSan_HouSanZuXuanDanTuo("##3GK","后三-组选胆拖", Enum_BallGame_11X5.BETTYPE_NONE),
	HouSan_HouSanZuXuanDanShi("##3GS","后三-后三组选单式", Enum_BallGame_11X5.BETTYPE_NONE),


	QuWeiXing_CaiZhongWei("M", "猜中位", Enum_BallGame_11X5.BETTYPE_NONE),
	QuWeiXing_DingDanShuang("S", "定单双", Enum_BallGame_11X5.BETTYPE_NONE)
	

	public String CurrentMethodCode
	public String CurrentMethodName
	public LinkedHashMap<String, Boolean>  isSupportAutoBetMethod

    Enum_Type_11X5(String MethodCode, String MethodName, LinkedHashMap<String, Boolean> isSupportAutoBetMethod) {
		this.CurrentMethodCode = MethodCode
		this.CurrentMethodName = MethodName
		this.isSupportAutoBetMethod = isSupportAutoBetMethod
	}
	 String getMethodCode() {
		return CurrentMethodCode
	}

	 String getMethodName() {
		return CurrentMethodName
	}

	HashMap<String, Boolean> getSupportAutoBetMethod() {
		return isSupportAutoBetMethod
	}
	@Override
	 String toString() {
		return this.getMethodCode()
	}
	
	/***
	 * 用code找名字 并核对返回到
	 * 
	 * @param MethodCode
	 * @return
	 */
	 static Enum_Type_11X5 getEnumNameByCode(String PlanCode) {
		 for (Enum_Type_11X5 v : Enum_Type_11X5.values()) {
			 if (v.toString().equalsIgnoreCase(PlanCode)) {
				 return v
			 }
		 }
		return BuDingWei_HouSanBuDingWei

	}

}
