package com.common.enumList

import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_K3
import com.game_plan.EnumList.Common_Method_Define

enum Enum_Type_K3 {
    /*KuaDu("K3KD", "跨度", Common_Method_GameAnalysis .MODE_K3_KUA_DU), */

    CaiYiGeHaoJiuZhongJiang("K3G1", "猜一个号就中奖", Enum_BallGame_K3.BETTYPE_NONE),
    HerZhi("K3DT", "和值", Enum_BallGame_K3.BETTYPE_NONE),
    HerZhiDaXiao("K3GB", "和值大小", Enum_BallGame_K3.BETTYPE_NONE),
    HerZhiDanShuang("K3GO", "和值单双", Enum_BallGame_K3.BETTYPE_NONE)

    //HerZhi_S("K3HZ*", "合值", Common_Method_GameAnalysis .MODE_K3_HER_ZHI), //相加后取个位数
    //HerZhiDaXiao_S("K3HZDX*", "合值大小", Common_Method_GameAnalysis .MODE_K3_HER_ZHI_DA_XIAO),//相加后取个位数
    //HerZhiDanShuang_S("K3HZDS*","合值单双", Common_Method_GameAnalysis .MODE_K3_HER_ZHI_DAN_SHUANG) //相加后取个位数


    public String CurrentMethodCode
    public String CurrentMethodName
    public int CurrentGameMode = 0

    Enum_Type_K3(String MethodCode, String MethodName, int CurrentGameMode) {
        this.CurrentMethodCode = MethodCode
        this.CurrentMethodName = MethodName
        this.CurrentGameMode = CurrentGameMode
    }

    String getMethodCode() {
        return CurrentMethodCode
    }

    String getMethodName() {
        return CurrentMethodName
    }

    int getMethodMode() {
        return CurrentGameMode
    }

    @Override
    String toString() {
        return this.getMethodCode()
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    static Enum_Type_K3 getEnumNameByCode(String PlanCode) {

        for (Enum_Type_K3 v : Enum_Type_K3.values()) {
            if (v.toString().equalsIgnoreCase(PlanCode)) {
                return v
            }
        }
        return HerZhi
    }

    static int[] Plan_LevelStartAndEnd(String PlanCode) {
        // 将这些随基数放入maps内
        int InitFirstLevel = 0
        int InitLastLevel = 2
        int[] NumAmount = new int[2]

        switch (getEnumNameByCode(PlanCode)) {

            default:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break


        }
        return NumAmount
    }

    static int[] Plan_NumStartAndEnd(String MethodCode) {

        int InitFirstLevel = 1
        int InitLastLevel = 6
        int[] LevelAmount = new int[2]
        // Log.e("NumberStartAndEnd", "start")
        switch (getEnumNameByCode(MethodCode)) {

            default:
                LevelAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                LevelAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }
        return LevelAmount
    }


    static int Plan_CurrentAnalysisInfo(String MethodCode) {
        return getEnumNameByCode(MethodCode).getMethodMode()
    }

}
