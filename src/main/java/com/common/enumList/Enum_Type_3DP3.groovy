package com.common.enumList

import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_3DAndP3


enum Enum_Type_3DP3 {

    YiXing_DingWeiDan("1DP", "定位胆", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    SanMa_FuShi("3DC", "三码-复式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    SanMa_DanShi("3DS", "三码-单式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    SanMa_HeZhi("3DT", "三码-和值", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    SanMa_ZuSan("3G3", "三码-组三", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    SanMa_ZuLiu("3G6", "三码-组六", Enum_BallGame_3DAndP3.BETTYPE_NONE),

    SanMa_HunHeZuXuan("3GM", "", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    BuDingWei_YiMaBuDingWei("3F1", "一码不定位", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    BuDingWei_ErMaBuDingWei("3F2", "二码不定位", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    QianEr_ZhiXuan_FuShi("2*DC", "前二-直选复式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    QianEr_ZhiXuan_DanShi("2*DS", "前二-直选单式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    QianEr_ZhiXuan_HeZhi("2*DT", "前二-直选合值", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    QianEr_ZuXuan_FuShi("2*GC", "前二-组选复式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    QianEr_ZuXuan_DanShi("2*GS", "前二-组选单式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    QianEr_ZuXuan_HeZhi("2*GT", "前二-组选合值", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    HouEr_ZhiXuan_FuShi("*2DC", "后二-直选复式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    HouEr_ZhiXuan_DanShi("*2DS", "后二-直选单式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    HouEr_ZhiXuan_HeZhi("*2DT", "后二-直选和值", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    HouEr_ZuXuan_FuShi("*2GC", "后二-组选复式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    HouEr_ZuXuan_DanShi("*2GS", "后二-组选单式", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    HouEr_ZuXuan_HeZhi("*2GT", "后二-组选和值", Enum_BallGame_3DAndP3.BETTYPE_NONE),

    DaXiaoDanShuang_HouErDaXiaoDanShuang("*2S", "后二大小单双", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    DaXiaoDanShuang_QianErDaXiaoDanShuang("2*S", "前二大小单双", Enum_BallGame_3DAndP3.BETTYPE_NONE),
    DaXiaoDanShuang_SanMaDaXiaoDanShuang("3S", "三码大小单双", Enum_BallGame_3DAndP3.BETTYPE_NONE)


    public String CurrentMethodCode
    public String CurrentMethodName
    public int CurrentGameMode = 0

    Enum_Type_3DP3(String MethodCode, String MethodName, int CurrentGameMode) {

        this.CurrentMethodCode = MethodCode
        this.CurrentMethodName = MethodName
        this.CurrentGameMode = CurrentGameMode

    }

    String getMethodCode() {
        return CurrentMethodCode
    }

    String getMethodName() {
        return CurrentMethodName
    }

    int getMethodMode() {
        return CurrentGameMode
    }

    @Override
    String toString() {
        return this.getMethodCode()
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    static Enum_Type_3DP3 getEnumNameByCode(String PlanCode) {
        for (Enum_Type_3DP3 v : Enum_Type_3DP3.values()) {
            if (v.toString().equalsIgnoreCase(PlanCode)) {
                return v
            }
        }
        return YiXing_DingWeiDan
    }
}
