package com.common.enumList

/**
 * Created by cjay on 2017/3/28.
 */
enum Enum_GameDefine {
    GAME_SSC("SSC"), GAME_11X5("11X5"), GAME_3DP3("3DP3"), GAME_PK10("PK10"), GAME_K3("K3")

    private String code

    Enum_GameDefine(String code) {
        this.code = code
    }

    String getCode() {
        return code
    }

    @Override
    String toString() {
        return this.getCode()
    }

    /***
     * 用code找名字 并核对返回到
     * @return
     */
    static Enum_GameDefine getEnumNameByCode(String GameCategory) {
        for (Enum_GameDefine v : Enum_GameDefine.values()) {
            if (v.toString().equalsIgnoreCase(GameCategory)) {
                return v
            }
        }
        return Enum_GameDefine.GAME_SSC
    }
}