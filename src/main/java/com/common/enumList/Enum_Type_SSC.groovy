package com.common.enumList

import com.AutoBetPlan.GameMethodAnalysis.Enum_BallGame_SSC

enum Enum_Type_SSC {

    YiXing_DingWeiDan_WAN("1DP_W", "万位定位胆", Enum_BallGame_SSC.BETTYPE_DIN_WEI_DAN),
    YiXing_DingWeiDan_QIAN("1DP_Q", "千位定位胆", Enum_BallGame_SSC.BETTYPE_DIN_WEI_DAN),
    YiXing_DingWeiDan_BAI("1DP_B", "百位定位胆", Enum_BallGame_SSC.BETTYPE_DIN_WEI_DAN),
    YiXing_DingWeiDan_SHI("1DP_S", "十位定位胆", Enum_BallGame_SSC.BETTYPE_DIN_WEI_DAN),
    YiXing_DingWeiDan_GE("1DP_G", "个位定位胆", Enum_BallGame_SSC.BETTYPE_DIN_WEI_DAN),

    /***
     * 不定位
     */
    BuDingWei_HouSanYiMaBuDingWei("*3F1", "后三一码不定位", Enum_BallGame_SSC.BETTYPE_NONE),
    BuDingWei_HouSanErMaBuDingWei("*3F2", "后三二码不定位", Enum_BallGame_SSC.BETTYPE_NONE),
    BuDingWei_QianSanYiMaBuDingWei("3*F1", "前三一码不定位", Enum_BallGame_SSC.BETTYPE_NONE),
    BuDingWei_QianSanErMaBuDingWei("3*F2", "前三二码不定位", Enum_BallGame_SSC.BETTYPE_NONE),
    BuDingWei_SiXingYiMaBuDingWei("4F1", "四星一码不定位", Enum_BallGame_SSC.BETTYPE_NONE),
    BuDingWei_SiXingErMaBuDingWei("4F2", "四星二码不定位", Enum_BallGame_SSC.BETTYPE_NONE),
    BuDingWei_WuXingErMaBuDingWei("5F2", "五星二码不定位", Enum_BallGame_SSC.BETTYPE_NONE),
    BuDingWei_WuXingSanMaBuDingWei("5F3", "五星三码不定位", Enum_BallGame_SSC.BETTYPE_NONE),

    /***
     *  中三
     */
            ZhongSan_ZhiXuanFuShi("#3#DC", "中三-直选复式", Enum_BallGame_SSC.BETTYPE_FU_SHI),
    ZhongSan_KuaDu("#3#DD", "中三-跨度", Enum_BallGame_SSC.BETTYPE_NONE),

    ZhongSan_ZhiXuanDanShi("#3#DS", "中三-直选单式", Enum_BallGame_SSC.BETTYPE_DAN_SHI),
    ZhongSan_ZhiXuanHeZhi("#3#DT", "中三-直选合值", Enum_BallGame_SSC.BETTYPE_NONE),
    ZhongSan_ZuSan("#3#G3", "中三-组选组三", Enum_BallGame_SSC.BETTYPE_NONE),
    ZhongSan_ZuLiu("#3#G6", "中三-组选组六", Enum_BallGame_SSC.BETTYPE_NONE),
    ZhongSan_BaoDan("#3#GA", "中三-包胆", Enum_BallGame_SSC.BETTYPE_NONE),

    ZhongSan_HunHeZuXuan("#3#GM", "中三-混和组选", Enum_BallGame_SSC.BETTYPE_NONE),
    ZhongSan_TeShuHaoMa("#3#GP", "中三-特殊号码", Enum_BallGame_SSC.BETTYPE_NONE),
    ZhongSan_ZuXuanHeZhi("#3#GT", "中三-组选合值", Enum_BallGame_SSC.BETTYPE_NONE),

    ErXing_HouErFuShi_ZhiXuan("*2DC", "后二-直选复式", Enum_BallGame_SSC.BETTYPE_FU_SHI),
    ErXing_HouErKuaDu_ZhiXuan("*2DD", "后二-直选跨度", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_HouErDanShi_ZhiXuan("*2DS", "后二-直选单式", Enum_BallGame_SSC.BETTYPE_DAN_SHI),
    ErXing_HouErHeZhi_ZhiXuan("*2DT", "后二-直选合值", Enum_BallGame_SSC.BETTYPE_NONE),

    ErXing_HouErBaoDan_ZuXuan("*2GA", "后二-组选包胆", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_HouErFuShi_ZuXuan("*2GC", "后二-组选复式", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_HouErDanShi_ZuXuan("*2GS", "后二-组选单式", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_HouErHeZhi_ZuXuan("*2GT", "后二-组选合值", Enum_BallGame_SSC.BETTYPE_NONE),

    ErXing_QianErFuShi_ZhiXuan("2*DC", "前二星-直选复式", Enum_BallGame_SSC.BETTYPE_FU_SHI),
    ErXing_QianErKuaDu_ZhiXuan("2*DD", "前二星-直选跨度", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_QianErDanShi_ZhiXuan("2*DS", "前二星-直选单式", Enum_BallGame_SSC.BETTYPE_DAN_SHI),
    ErXing_QianErHeZhi_ZhiXuan("2*DT", "前二星-直选合值", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_QianErBaoDan_ZuXuan("2*GA", "前二星-组选包胆", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_QianErFuShi_ZuXuan("2*GC", "前二星-组选复式", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_QianErDanShi_ZuXuan("2*GS", "前二星-组选单式", Enum_BallGame_SSC.BETTYPE_NONE),
    ErXing_QianErHeZhi_ZuXuan("2*GT", "前二星-组选合值", Enum_BallGame_SSC.BETTYPE_NONE),

    /***
     *  五星
     */
            WuXing_FuShi("5DC", "五星-直选复式", Enum_BallGame_SSC.BETTYPE_FU_SHI),
    WuXing_DanShi("5DS", "五星-直选单式", Enum_BallGame_SSC.BETTYPE_DAN_SHI),
    WuXing_ZuXuan10("5G10", "五星-组选10", Enum_BallGame_SSC.BETTYPE_NONE),
    WuXing_ZuXuan120("5G120", "五星-组选120", Enum_BallGame_SSC.BETTYPE_NONE),
    WuXing_ZuXuan20("5G20", "五星-组选20", Enum_BallGame_SSC.BETTYPE_NONE),
    WuXing_ZuXuan30("5G30", "五星-组选30", Enum_BallGame_SSC.BETTYPE_NONE),
    WuXing_ZuXuan5("5G5", "五星-组选5", Enum_BallGame_SSC.BETTYPE_NONE),
    WuXing_ZuXuan60("5G60", "五星-组选60", Enum_BallGame_SSC.BETTYPE_NONE),

    /***
     * 任選3
     */
            RenXuanSan_ZhiXuanFuShi("A3DC", "任选三-直选复式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_ZhiXuanDanShi("A3DS", "任选三-直选单式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_ZhiXuanHeZhi("A3DT", "任选三-直选合值", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_ZuSanFuShi("A3G3C", "任选三-组三复式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_ZuSanDanShi("A3G3S", "任选三-组三单式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_ZuLiuFuShi("A3G6C", "任选三-组六复式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_ZuLiuDanShi("A3G6S", "任选三-组六单式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_HunHeZuXuan("A3GM", "任选三-混合组选", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSan_ZuXuanHeZhi("A3GT", "任选三-组选合值", Enum_BallGame_SSC.BETTYPE_NONE),
    /***
     * 任選2
     */
            RenXuanEr_ZhiXuanFuShi("A2DC", "任选二-直选复式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanEr_ZhiXuanDanShi("A2DS", "任选二-直选单式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanEr_ZhiXuanHeZhi("A2DT", "任选二-直选合值", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanEr_ZuXuanFuShi("A2GC", "任选二-组选复式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanEr_ZuXuanDanShi("A2GS", "任选二-组选单式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanEr_ZuXuanHeZhi("A2GT", "任选二-组选合值", Enum_BallGame_SSC.BETTYPE_NONE),

    // 任選4
            RenXuanSi_ZhiXuanFuShi("A4DC", "任选四-直选复式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSi_ZhiXuanDanShi("A4DS", "任选四-直选单式", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSi_ZuXuan12("A4G12", "任选四-组选12", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSi_ZuXuan24("A4G24", "任选四-组选24", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSi_ZuXuan4("A4G4", "任选四-组选4", Enum_BallGame_SSC.BETTYPE_NONE),
    RenXuanSi_ZuXuan6("A4G6", "任选四-组选6", Enum_BallGame_SSC.BETTYPE_NONE),

    // 前三
            QianSan_ZhiXuanFuShi("3##DC", "前三星-直选复式", Enum_BallGame_SSC.BETTYPE_FU_SHI),
    QianSan_KuaDu("3##DD", "前三星-跨度", Enum_BallGame_SSC.BETTYPE_NONE),
    QianSan_ZhiXuanDanShi("3##DS", "前三星-单式", Enum_BallGame_SSC.BETTYPE_DAN_SHI),
    QianSan_ZhiXuanHeZhi("3##DT", "前三星-合值", Enum_BallGame_SSC.BETTYPE_NONE),
    QianSan_ZuSan("3##G3", "前三星-组选组三", Enum_BallGame_SSC.BETTYPE_NONE),
    QianSan_ZuLiu("3##G6", "前三星-组选组六", Enum_BallGame_SSC.BETTYPE_NONE),
    QianSan_BaoDan("3##GA", "前三星-包胆", Enum_BallGame_SSC.BETTYPE_NONE),
    QianSan_HunHeZuXuan("3##GM", "前三星-混合组选", Enum_BallGame_SSC.BETTYPE_NONE),
    QianSan_TeShuHaoMa("3##GP", "前三星-特殊号码", Enum_BallGame_SSC.BETTYPE_NONE),
    QianSan_ZuXuanHeZhi("3##GT", "前三星-组选合值", Enum_BallGame_SSC.BETTYPE_NONE),

    // 後三
            HouSan_ZhiXuanFuShi("##3DC", "后三星-直选复式", Enum_BallGame_SSC.BETTYPE_FU_SHI),
    HouSan_KuaDu("##3DD", "后三星-跨度", Enum_BallGame_SSC.BETTYPE_NONE),
    HouSan_ZhiXuanDanShi("##3DS", "后三星-直选单式", Enum_BallGame_SSC.BETTYPE_DAN_SHI),
    HouSan_ZhiXuanHeZhi("##3DT", "后三星-直选合值", Enum_BallGame_SSC.BETTYPE_NONE),
    HouSan_ZuSan("##3G3", "后三星-组选组三", Enum_BallGame_SSC.BETTYPE_NONE),
    HouSan_ZuLiu("##3G6", "后三星-组选组六", Enum_BallGame_SSC.BETTYPE_NONE),
    HouSan_BaoDan("##3GA", "后三星-包胆", Enum_BallGame_SSC.BETTYPE_NONE),
    HouSan_HunHeZuXuan("##3GM", "后三星-混合组选", Enum_BallGame_SSC.BETTYPE_NONE),
    HouSan_TeShuHaoMa("##3GP", "后三星-特殊号码", Enum_BallGame_SSC.BETTYPE_NONE),
    HouSan_ZuXuanHeZhi("##3GT", "后三星-组选合值", Enum_BallGame_SSC.BETTYPE_NONE),

    // 四星
            SiXing_ZhiXuanFuShi("4DC", "四星-直选复式", Enum_BallGame_SSC.BETTYPE_FU_SHI),
    SiXing_ZhiXuanDanShi("4DS", "四星-单式", Enum_BallGame_SSC.BETTYPE_DAN_SHI),
    SiXing_ZuXuan12("4G12", "四星-组选12", Enum_BallGame_SSC.BETTYPE_NONE),
    SiXing_ZuXuan24("4G24", "四星-组选24", Enum_BallGame_SSC.BETTYPE_NONE),
    SiXing_ZuXuan4("4G4", "四星-组选4", Enum_BallGame_SSC.BETTYPE_NONE),
    SiXing_ZuXuan6("4G6", "四星-组选6", Enum_BallGame_SSC.BETTYPE_NONE),

    DaXiaoDanShuang_HouErDaXiaoDanShuang("*2S", "后二-大小单双", Enum_BallGame_SSC.BETTYPE_NONE),
    DaXiaoDanShuang_QianErDaXiaoDanShuang("2*S", "前二-大小单双", Enum_BallGame_SSC.BETTYPE_NONE),

    QuWei_YiFanFengShun("S1", "趣味-一帆风顺", Enum_BallGame_SSC.BETTYPE_NONE),
    QuWei_HaoShiChengShuang("S2", "趣味-好事成双", Enum_BallGame_SSC.BETTYPE_NONE),
    QuWei_SanXingBaoXi("S3", "趣味-三星报喜", Enum_BallGame_SSC.BETTYPE_NONE),
    QuWei_SiJiFaCai("S4", "趣味-四季发财", Enum_BallGame_SSC.BETTYPE_NONE),

    LongHuHe_WanQian("##DAT", "龙虎合-万千", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_WanBai("#D#AT", "龙虎合-万百", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_WanShi("#DA#T", "龙虎合-万十", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_WanGe("#DAT#", "龙虎合-万个", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_QianBai("D##AT", "龙虎合-千百", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_QianShi("D#A#T", "龙虎合-千十", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_QianGe("D#AT#", "龙虎合-千个", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_BaiShi("DA##T", "龙虎合-百十", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_BaiGe("DA#T#", "龙虎合-百个", Enum_BallGame_SSC.BETTYPE_NONE),
    LongHuHe_ShiGe("DAT##", "龙虎合-十个", Enum_BallGame_SSC.BETTYPE_NONE),


    public String CurrentMethodCode
    public String CurrentMethodName
    public LinkedHashMap<String, Boolean> isSupportAutoBetMethod

    Enum_Type_SSC(String MethodCode, String MethodName, LinkedHashMap<String, Boolean> isSupportAutoBetMethod) {
        this.CurrentMethodCode = MethodCode
        this.CurrentMethodName = MethodName
        this.isSupportAutoBetMethod = isSupportAutoBetMethod
    }

    String getMethodCode() {
        return CurrentMethodCode
    }

    String getMethodName() {
        return CurrentMethodName
    }

    HashMap<String, Boolean> getSupportAutoBetMethod() {
        return isSupportAutoBetMethod
    }


    @Override
    String toString() {
        return this.getMethodCode()
    }
    /***
     * 用code找名字 并核对返回到
     *
     * @param MethodCode
     * @return
     */
    static Enum_Type_SSC getEnumNameByCode(String PlanCode) {
        for (Enum_Type_SSC v : Enum_Type_SSC.values()) {
            if (v.toString().equalsIgnoreCase(PlanCode)) {
                return v
            }
        }
        return YiXing_DingWeiDan_GE
    }


}
