package com.game_plan.jsonapi;

import java.util.List;

/**
 * Created by cjay on 2017/3/27.
 */
public class OpenDrawFromScr {

    /**
     * code : 200
     * data : [{"id":4122685,"actualOpenTime":"2017-03-27 13:51:00","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 13:51:02","numberFetchedTime":"2017-03-27 13:51:00","numbers":"5,3,7,8,3","openTime":"2017-03-27 13:50:00","seq":23208,"signature":"20170327047","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122684,"actualOpenTime":"2017-03-27 13:41:06","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 13:41:09","numberFetchedTime":"2017-03-27 13:41:06","numbers":"1,8,7,6,8","openTime":"2017-03-27 13:40:00","seq":23207,"signature":"20170327046","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122683,"actualOpenTime":"2017-03-27 13:31:06","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 13:31:09","numberFetchedTime":"2017-03-27 13:31:06","numbers":"9,9,0,8,1","openTime":"2017-03-27 13:30:00","seq":23206,"signature":"20170327045","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122682,"actualOpenTime":"2017-03-27 13:21:05","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 13:21:07","numberFetchedTime":"2017-03-27 13:21:05","numbers":"8,0,1,8,6","openTime":"2017-03-27 13:20:00","seq":23205,"signature":"20170327044","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122681,"actualOpenTime":"2017-03-27 13:11:05","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 13:11:07","numberFetchedTime":"2017-03-27 13:11:05","numbers":"2,4,0,9,5","openTime":"2017-03-27 13:10:00","seq":23204,"signature":"20170327043","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122680,"actualOpenTime":"2017-03-27 13:01:01","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 13:01:03","numberFetchedTime":"2017-03-27 13:01:01","numbers":"2,1,7,4,9","openTime":"2017-03-27 13:00:00","seq":23203,"signature":"20170327042","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122679,"actualOpenTime":"2017-03-27 12:51:06","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 12:51:09","numberFetchedTime":"2017-03-27 12:51:06","numbers":"9,3,9,6,4","openTime":"2017-03-27 12:50:00","seq":23202,"signature":"20170327041","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122678,"actualOpenTime":"2017-03-27 12:41:01","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 12:41:03","numberFetchedTime":"2017-03-27 12:41:01","numbers":"6,8,5,0,0","openTime":"2017-03-27 12:40:00","seq":23201,"signature":"20170327040","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122677,"actualOpenTime":"2017-03-27 12:31:05","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 12:31:08","numberFetchedTime":"2017-03-27 12:31:05","numbers":"1,5,9,2,8","openTime":"2017-03-27 12:30:00","seq":23200,"signature":"20170327039","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122676,"actualOpenTime":"2017-03-27 12:21:06","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 12:21:08","numberFetchedTime":"2017-03-27 12:21:06","numbers":"0,2,9,7,5","openTime":"2017-03-27 12:20:00","seq":23199,"signature":"20170327038","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122675,"actualOpenTime":"2017-03-27 12:11:06","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 12:11:08","numberFetchedTime":"2017-03-27 12:11:06","numbers":"1,7,8,5,1","openTime":"2017-03-27 12:10:00","seq":23198,"signature":"20170327037","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122674,"actualOpenTime":"2017-03-27 12:01:01","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 12:01:03","numberFetchedTime":"2017-03-27 12:01:01","numbers":"7,8,3,1,8","openTime":"2017-03-27 12:00:00","seq":23197,"signature":"20170327036","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122673,"actualOpenTime":"2017-03-27 11:51:11","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 11:51:13","numberFetchedTime":"2017-03-27 11:51:11","numbers":"5,5,6,5,8","openTime":"2017-03-27 11:50:00","seq":23196,"signature":"20170327035","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122672,"actualOpenTime":"2017-03-27 11:41:00","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 11:41:02","numberFetchedTime":"2017-03-27 11:41:00","numbers":"3,1,3,8,5","openTime":"2017-03-27 11:40:00","seq":23195,"signature":"20170327034","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122671,"actualOpenTime":"2017-03-27 11:31:01","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 11:31:03","numberFetchedTime":"2017-03-27 11:31:01","numbers":"7,0,3,2,5","openTime":"2017-03-27 11:30:00","seq":23194,"signature":"20170327033","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122670,"actualOpenTime":"2017-03-27 11:21:01","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 11:21:02","numberFetchedTime":"2017-03-27 11:21:01","numbers":"1,2,7,4,4","openTime":"2017-03-27 11:20:00","seq":23193,"signature":"20170327032","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122669,"actualOpenTime":"2017-03-27 11:11:06","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 11:11:07","numberFetchedTime":"2017-03-27 11:11:06","numbers":"2,5,4,1,1","openTime":"2017-03-27 11:10:00","seq":23192,"signature":"20170327031","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122668,"actualOpenTime":"2017-03-27 11:01:01","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 11:01:03","numberFetchedTime":"2017-03-27 11:01:01","numbers":"9,5,2,1,3","openTime":"2017-03-27 11:00:00","seq":23191,"signature":"20170327030","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122667,"actualOpenTime":"2017-03-27 10:50:57","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 10:50:59","numberFetchedTime":"2017-03-27 10:50:57","numbers":"9,0,7,7,0","openTime":"2017-03-27 10:50:00","seq":23190,"signature":"20170327029","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122666,"actualOpenTime":"2017-03-27 10:41:01","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 10:41:02","numberFetchedTime":"2017-03-27 10:41:01","numbers":"8,3,8,0,7","openTime":"2017-03-27 10:40:00","seq":23189,"signature":"20170327028","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122665,"actualOpenTime":"2017-03-27 10:31:03","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 10:31:04","numberFetchedTime":"2017-03-27 10:31:03","numbers":"8,9,1,7,6","openTime":"2017-03-27 10:30:00","seq":23188,"signature":"20170327027","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122664,"actualOpenTime":"2017-03-27 10:21:07","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 10:21:08","numberFetchedTime":"2017-03-27 10:21:07","numbers":"1,6,4,1,4","openTime":"2017-03-27 10:20:00","seq":23187,"signature":"20170327026","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122663,"actualOpenTime":"2017-03-27 10:11:00","creator":"system","dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 10:11:01","numberFetchedTime":"2017-03-27 10:11:00","numbers":"2,1,9,6,4","openTime":"2017-03-27 10:10:00","seq":23186,"signature":"20170327025","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4122662,"actualOpenTime":"2017-03-27 10:01:05","creator":null,"dateCreated":"2017-03-26 02:30:00","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 10:01:06","numberFetchedTime":"2017-03-27 10:01:05","numbers":"5,7,9,3,6","openTime":"2017-03-27 10:00:00","seq":23185,"signature":"20170327024","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4116985,"actualOpenTime":"2017-03-27 01:55:55","creator":"system","dateCreated":"2017-03-25 02:30:01","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 01:55:56","numberFetchedTime":"2017-03-27 01:55:55","numbers":"7,2,4,2,2","openTime":"2017-03-27 01:55:00","seq":23304,"signature":"20170327023","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4116984,"actualOpenTime":"2017-03-27 01:50:55","creator":"system","dateCreated":"2017-03-25 02:30:01","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 01:50:56","numberFetchedTime":"2017-03-27 01:50:55","numbers":"7,0,2,9,0","openTime":"2017-03-27 01:50:00","seq":23303,"signature":"20170327022","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4116983,"actualOpenTime":"2017-03-27 01:45:56","creator":"system","dateCreated":"2017-03-25 02:30:01","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 01:45:57","numberFetchedTime":"2017-03-27 01:45:56","numbers":"6,5,4,7,0","openTime":"2017-03-27 01:45:00","seq":23302,"signature":"20170327021","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4116982,"actualOpenTime":"2017-03-27 01:40:51","creator":"system","dateCreated":"2017-03-25 02:30:01","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 01:40:52","numberFetchedTime":"2017-03-27 01:40:51","numbers":"5,9,3,1,0","openTime":"2017-03-27 01:40:00","seq":23301,"signature":"20170327020","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4116981,"actualOpenTime":"2017-03-27 01:35:56","creator":"system","dateCreated":"2017-03-25 02:30:01","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 01:35:57","numberFetchedTime":"2017-03-27 01:35:56","numbers":"7,1,5,6,8","openTime":"2017-03-27 01:35:00","seq":23300,"signature":"20170327019","status":{"name":"BUNDLE_DEALT"},"updater":null},{"id":4116980,"actualOpenTime":"2017-03-27 01:30:56","creator":"system","dateCreated":"2017-03-25 02:30:01","game":{"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9},"lastUpdated":"2017-03-27 01:30:57","numberFetchedTime":"2017-03-27 01:30:56","numbers":"2,5,5,1,8","openTime":"2017-03-27 01:30:00","seq":23299,"signature":"20170327018","status":{"name":"BUNDLE_DEALT"},"updater":null}]
     * message : null
     * totalCount : 0
     */

    private String code;
    private Object message;
    private int totalCount;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 4122685
         * actualOpenTime : 2017-03-27 13:51:00
         * creator : system
         * dateCreated : 2017-03-26 02:30:00
         * game : {"id":1,"autoNumberFetch":true,"category":"SSC","cnt":5,"defaultMethod":null,"enabled":true,"endHourOffset":24,"lower":0,"maxPrize":500000000,"mobileEnabled":true,"name":"重庆时时彩","offSaleAdvance":60,"periodDatePattern":"yyyyMMdd","periodNumberPattern":"000","startHourOffset":0,"type":{"name":"CQSSC"},"upper":9}
         * lastUpdated : 2017-03-27 13:51:02
         * numberFetchedTime : 2017-03-27 13:51:00
         * numbers : 5,3,7,8,3
         * openTime : 2017-03-27 13:50:00
         * seq : 23208
         * signature : 20170327047
         * status : {"name":"BUNDLE_DEALT"}
         * updater : null
         */

        private int id;
        private String actualOpenTime;
        private String creator;
        private String dateCreated;
        private GameBean game;
        private String lastUpdated;
        private String numberFetchedTime;
        private String numbers;
        private String openTime;
        private int seq;
        private String signature;
        private StatusBean status;
        private Object updater;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getActualOpenTime() {
            return actualOpenTime;
        }

        public void setActualOpenTime(String actualOpenTime) {
            this.actualOpenTime = actualOpenTime;
        }

        public String getCreator() {
            return creator;
        }

        public void setCreator(String creator) {
            this.creator = creator;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public GameBean getGame() {
            return game;
        }

        public void setGame(GameBean game) {
            this.game = game;
        }

        public String getLastUpdated() {
            return lastUpdated;
        }

        public void setLastUpdated(String lastUpdated) {
            this.lastUpdated = lastUpdated;
        }

        public String getNumberFetchedTime() {
            return numberFetchedTime;
        }

        public void setNumberFetchedTime(String numberFetchedTime) {
            this.numberFetchedTime = numberFetchedTime;
        }

        public String getNumbers() {
            return numbers;
        }

        public void setNumbers(String numbers) {
            this.numbers = numbers;
        }

        public String getOpenTime() {
            return openTime;
        }

        public void setOpenTime(String openTime) {
            this.openTime = openTime;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public StatusBean getStatus() {
            return status;
        }

        public void setStatus(StatusBean status) {
            this.status = status;
        }

        public Object getUpdater() {
            return updater;
        }

        public void setUpdater(Object updater) {
            this.updater = updater;
        }

        public static class GameBean {
            /**
             * id : 1
             * autoNumberFetch : true
             * category : SSC
             * cnt : 5
             * defaultMethod : null
             * enabled : true
             * endHourOffset : 24
             * lower : 0
             * maxPrize : 500000000
             * mobileEnabled : true
             * name : 重庆时时彩
             * offSaleAdvance : 60
             * periodDatePattern : yyyyMMdd
             * periodNumberPattern : 000
             * startHourOffset : 0
             * type : {"name":"CQSSC"}
             * upper : 9
             */

            private int id;
            private boolean autoNumberFetch;
            private String category;
            private int cnt;
            private Object defaultMethod;
            private boolean enabled;
            private int endHourOffset;
            private int lower;
            private double maxPrize;
            private boolean mobileEnabled;
            private String name;
            private int offSaleAdvance;
            private String periodDatePattern;
            private String periodNumberPattern;
            private int startHourOffset;
            private TypeBean type;
            private int upper;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public boolean isAutoNumberFetch() {
                return autoNumberFetch;
            }

            public void setAutoNumberFetch(boolean autoNumberFetch) {
                this.autoNumberFetch = autoNumberFetch;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public int getCnt() {
                return cnt;
            }

            public void setCnt(int cnt) {
                this.cnt = cnt;
            }

            public Object getDefaultMethod() {
                return defaultMethod;
            }

            public void setDefaultMethod(Object defaultMethod) {
                this.defaultMethod = defaultMethod;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public int getEndHourOffset() {
                return endHourOffset;
            }

            public void setEndHourOffset(int endHourOffset) {
                this.endHourOffset = endHourOffset;
            }

            public int getLower() {
                return lower;
            }

            public void setLower(int lower) {
                this.lower = lower;
            }

            public double getMaxPrize() {
                return maxPrize;
            }

            public void setMaxPrize(double maxPrize) {
                this.maxPrize = maxPrize;
            }

            public boolean isMobileEnabled() {
                return mobileEnabled;
            }

            public void setMobileEnabled(boolean mobileEnabled) {
                this.mobileEnabled = mobileEnabled;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getOffSaleAdvance() {
                return offSaleAdvance;
            }

            public void setOffSaleAdvance(int offSaleAdvance) {
                this.offSaleAdvance = offSaleAdvance;
            }

            public String getPeriodDatePattern() {
                return periodDatePattern;
            }

            public void setPeriodDatePattern(String periodDatePattern) {
                this.periodDatePattern = periodDatePattern;
            }

            public String getPeriodNumberPattern() {
                return periodNumberPattern;
            }

            public void setPeriodNumberPattern(String periodNumberPattern) {
                this.periodNumberPattern = periodNumberPattern;
            }

            public int getStartHourOffset() {
                return startHourOffset;
            }

            public void setStartHourOffset(int startHourOffset) {
                this.startHourOffset = startHourOffset;
            }

            public TypeBean getType() {
                return type;
            }

            public void setType(TypeBean type) {
                this.type = type;
            }

            public int getUpper() {
                return upper;
            }

            public void setUpper(int upper) {
                this.upper = upper;
            }

            public static class TypeBean {
                /**
                 * name : CQSSC
                 */

                private String name;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }
        }

        public static class StatusBean {
            /**
             * name : BUNDLE_DEALT
             */

            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
