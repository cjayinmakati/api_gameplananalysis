package com.game_plan.beans

/***
 * 玩法内 每几期一分析的资料
 */
class GameSubItem {
    String GameSubItem_CurrentRecommendCode//当下几期玩法的 推荐号码

    String GameSubItem_WonIssue//当下几期玩法的 中奖期号（没中奖显示最后一期）例如 12 13 14 期中 中13期

    String GameSubItem_WonCode//当下几期玩法的  中奖号码（没中奖显示最后一期）

    String GameSubItem_WonMSG//当下几期玩法的  中奖号码（没中奖显示最后一期）

    boolean GameSubItem_WonStatus//当下几期玩法的  中奖状态

    String GameSubItem_StartIssue//当下几期玩法的  开始计算的奖期 例如 12 13 14 开始为12期

    String GameSubItem_EndIssue//当下几期玩法的  结束计算的奖期 例如 12 13 14 结束为14期

}
