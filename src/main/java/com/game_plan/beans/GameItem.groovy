package com.game_plan.beans

/***
 * 计画底下 玩法内容
 * 例如 后三 3期5码 胆拖
 */
class GameItem {
//玩法 码
    String GameItem_CategoryCode

    String GameItem_PlanCode
//玩法 码
    String GameItem_PlanName
//玩法 码
    String GameItem_MethodCode

    String GameItem_MethodName
//玩法 计算中奖的 類型
    String GameItem_GameRecommendType
//玩法 需求推荐几个号码 5码 码
    int GameItem_GameRequireNumAmount
//玩法 每几期一次 三期一计画 五其一计画
    int GameItem_GameIssueGapNumAmount

//玩法推荐列表 开始计算的奖期
    String GameItem_GameStartIssue
//玩法推荐列表 结束计算的奖期
    String GameItem_GameEndIssue

//玩法推荐列表 最大连 中 期数
    int GameItem_GameMaxContinuousWIN
//玩法推荐列表 最大连 挂 期数
    int GameItem_GameMaxContinuousDEAD
//玩法推荐列表 当前连中期数（计算到当下）
    int GameItem_GameCurrentContinuousWIN
//玩法推荐列表 当前连挂期数
    int GameItem_GameCurrentContinuousDEAD
//玩法推荐列表 当前总 中奖次数
    int GameItem_GameTotalWONIssue//
//玩法推荐列表 所有计算次数
    int GameItem_GameTotalCalIssue

    int[] GameItem_AnalysisData_ShowLV
    int[] GameItem_AnalysisData_ShowNum

    //推荐每几期一个区间的玩法分析 列表
    ArrayList<GameSubItem> GameDetails_RecommendList = new ArrayList<GameSubItem>()//推荐 计画列表





//    def planInfo = new Bean_CurrentPlanInfo()
//    planInfo.with {
//        it.setContainNumAmount(gamePlan.getGameItem_GameRequireNumAmount())
//        it.setCurrentIssueGap(gamePlan.getGameItem_GameIssueGapNumAmount())
//        it.setCurrentCategoryCode(CurrentGameCategory)
//        it.setCurrentMethodCode(gamePlan.getGameItem_GameCode())
//        it.setCurrentMethodName(getMethodNameByCode(CurrentGameCategory, gamePlan.getGameItem_GameCode()) as String)
//        it.setShowLV(Plan_LevelStartAndEnd())
//        it.setShowNum(Plan_NumStartAndEnd())
//        it.setCurrentGameAnalysisMode()
}
