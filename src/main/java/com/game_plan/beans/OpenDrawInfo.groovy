package com.game_plan.beans

/**
 * 获取开奖列表后 开始解析
 */
 class OpenDrawInfo {
	 String game_NameCode// cqssc
	 String game_CName //重庆时时彩
	 String game_Issue//奖期
	 String game_Code//开奖号码
	 String game_Category//ssc 时时彩
	 String game_Id
	 String game_StatusCode//开奖状态
	 String game_OpenTime//是否开奖
}
