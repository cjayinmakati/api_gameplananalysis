package com.game_plan.beans

/**
 * 彩种资讯
 */
class CategoryInfo {

    String Category_Code//彩种码 CQSSC

    String Category_Name//彩种名称 重庆时时彩

    ArrayList<String> ProjectList = new ArrayList<String>() //彩种中包含 哪一些 计画的名称列表 例如 时时彩 下有 三码不定位 五码复视

    ArrayList<ProjectInfo> ProjectInfoList = new ArrayList<ProjectInfo>() //彩种中包含 哪一些 计画所有资讯列表 例如 时时彩 下有 三码不定位 五码复视



}
