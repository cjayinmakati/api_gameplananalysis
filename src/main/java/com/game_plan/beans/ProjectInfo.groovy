package com.game_plan.beans

/***
 * 计算软件 计画内容
 */
class ProjectInfo {
    String Project_Code//计画名称

    String Project_Name//计画名称

    String Project_Group//计画分组 三星 四星 五星 组选 胆码

    ArrayList<GameItem> ProjectContainMethodCodeList = new ArrayList<GameItem>() //用于暂存部分计画内容（）

    ArrayList<GameItem> ProjectContainInfoList = new ArrayList<GameItem>()// 用于永存计画内容


}
