package com.game_plan.EnumList

import com.common.enumList.Enum_Type_K3

class Common_Method_K3 {
    int[] Plan_LevelStartAndEnd(String PlanCode) {
        // 将这些随基数放入maps内
        int InitFirstLevel = 0
        int InitLastLevel = 2
        int[] NumAmount = new int[2]

        switch (Enum_Type_K3.getEnumNameByCode(PlanCode)) {

            default:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break


        }
        return NumAmount
    }

    int[] Plan_NumStartAndEnd(String MethodCode) {

        int InitFirstLevel = 1
        int InitLastLevel = 6
        int[] LevelAmount = new int[2]
        // Log.e("NumberStartAndEnd", "start")
        switch (Enum_Type_K3.getEnumNameByCode(MethodCode)) {

            default:
                LevelAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                LevelAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }
        return LevelAmount
    }


    int Plan_CurrentAnalysisInfo(String MethodCode) {
        return Enum_Type_K3.getEnumNameByCode(MethodCode).getMethodMode()
    }

}
