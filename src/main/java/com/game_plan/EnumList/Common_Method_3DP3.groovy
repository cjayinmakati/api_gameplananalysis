package com.game_plan.EnumList

import com.common.enumList.Enum_Type_3DP3

@Singleton
class Common_Method_3DP3 {
    int[] Plan_LevelStartAndEnd(String PlanCode) {
        // 将这些随基数放入maps内
        int InitFirstLevel = 1
        int InitLastLevel = 3
        int[] NumAmount = new int[2]
        switch (Enum_Type_3DP3.getEnumNameByCode(PlanCode)) {
            case Enum_Type_3DP3.BuDingWei_ErMaBuDingWei:
            case Enum_Type_3DP3.BuDingWei_YiMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_3DP3.DaXiaoDanShuang_HouErDaXiaoDanShuang:
            case Enum_Type_3DP3.DaXiaoDanShuang_QianErDaXiaoDanShuang:
            case Enum_Type_3DP3.DaXiaoDanShuang_SanMaDaXiaoDanShuang:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_3DP3.HouEr_ZhiXuan_DanShi:
            case Enum_Type_3DP3.HouEr_ZhiXuan_FuShi:
            case Enum_Type_3DP3.HouEr_ZhiXuan_HeZhi:
            case Enum_Type_3DP3.HouEr_ZuXuan_DanShi:
            case Enum_Type_3DP3.HouEr_ZuXuan_FuShi:
            case Enum_Type_3DP3.HouEr_ZuXuan_HeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_3DP3.QianEr_ZhiXuan_DanShi:
            case Enum_Type_3DP3.QianEr_ZhiXuan_FuShi:
            case Enum_Type_3DP3.QianEr_ZhiXuan_HeZhi:
            case Enum_Type_3DP3.QianEr_ZuXuan_DanShi:
            case Enum_Type_3DP3.QianEr_ZuXuan_FuShi:
            case Enum_Type_3DP3.QianEr_ZuXuan_HeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_3DP3.SanMa_DanShi:
            case Enum_Type_3DP3.SanMa_FuShi:
            case Enum_Type_3DP3.SanMa_HeZhi:
            case Enum_Type_3DP3.SanMa_HunHeZuXuan:
            case Enum_Type_3DP3.SanMa_ZuLiu:
            case Enum_Type_3DP3.SanMa_ZuSan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_3DP3.YiXing_DingWeiDan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            default:
                break

        }
        return NumAmount
    }

    int[] Plan_NumStartAndEnd(String MethodCode) {

        int InitFirstLevel = 0
        int InitLastLevel = 9
        int[] LevelAmount = new int[2]
        // Log.e("NumberStartAndEnd", "start")
        switch (Enum_Type_3DP3.getEnumNameByCode(MethodCode)) {

            default:
                LevelAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                LevelAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }
        return LevelAmount
    }


    int Plan_CurrentAnalysisInfo(String MethodCode) {
        return Enum_Type_3DP3.getEnumNameByCode(MethodCode).getMethodMode()
    }

}
