package com.game_plan.EnumList

import com.common.enumList.Enum_Type_11X5

class Common_Method_11X5 {
    int[] Plan_LevelStartAndEnd(String PlanCode) {
        // 将这些随基数放入maps内
        int InitFirstLevel = 0
        int InitLastLevel = 4
        int[] NumAmount = new int[2]

        switch (Enum_Type_11X5.getEnumNameByCode(PlanCode)) {

            case Enum_Type_11X5.DingWeiDan_DingWeiDan_FIRTH:
                NumAmount[Common_Method_Define.FirstNumShow] = 0
                NumAmount[Common_Method_Define.LastNumShow] = 0
                break
            case Enum_Type_11X5.DingWeiDan_DingWeiDan_SECOND:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_11X5.DingWeiDan_DingWeiDan_THIRD:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_11X5.DingWeiDan_DingWeiDan_FOURTH:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_11X5.DingWeiDan_DingWeiDan_FIFTH:
                NumAmount[Common_Method_Define.FirstNumShow] = 4
                NumAmount[Common_Method_Define.LastNumShow] = 4
                break

            case Enum_Type_11X5.BuDingWei_HouSanBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.BuDingWei_QianSanBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.ErMa_QianErZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_11X5.ErMa_QianErZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_11X5.ErMa_QianErZuXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_11X5.ErMa_QianErZuXuanDanTuo:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_11X5.ErMa_QianErZuXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.HouSan_HouSanZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.HouSan_HouSanZuXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.HouSan_HouSanZuXuanDanTuo:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.HouSan_HouSanZuXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.QianSan_QianSanZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_11X5.QianSan_QianSanZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_11X5.QianSan_QianSanZuXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_11X5.QianSan_QianSanZuXuanDanTuo:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_11X5.QianSan_QianSanZuXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_11X5.QuWeiXing_CaiZhongWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.QuWeiXing_DingDanShuang:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanBaZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanErZhongEr:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanLiuZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanQiZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanSanZhongSan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanSiZhongSi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanWuZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanShi_RenXuanYiZhongYi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanTuo_RenXuanBaZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanTuo_RenXuanErZhongEr:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanTuo_RenXuanLiuZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanTuo_RenXuanQiZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanTuo_RenXuanSanZhongSan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanTuo_RenXuanSiZhongSi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanDanTuo_RenXuanWuZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanBaZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanErZhongEr:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanLiuZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanQiZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanSanZhongSan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanSiZhongSi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanWuZhongWu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_11X5.RenXuanFuShi_RenXuanYiZhongYi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            default:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break


        }
        return NumAmount
    }

    int[] Plan_NumStartAndEnd(String MethodCode) {

        int InitFirstLevel = 1
        int InitLastLevel = 11
        int[] LevelAmount = new int[2]
        // Log.e("NumberStartAndEnd", "start")
        switch (Enum_Type_11X5.getEnumNameByCode(MethodCode)) {

            default:
                LevelAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                LevelAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }
        return LevelAmount
    }

}
