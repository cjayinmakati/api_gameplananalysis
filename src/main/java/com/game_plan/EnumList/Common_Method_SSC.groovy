package com.game_plan.EnumList

import com.AutoBetPlan.GameInfo.Enum_AutoBettingType
import com.common.enumList.Enum_Type_SSC

class Common_Method_SSC {

     int[] Plan_LevelStartAndEnd(String PlanCode) {

        // 将这些随基数放入maps内
        int InitFirstLevel = 0
        int InitLastLevel = 4
        int[] NumAmount = new int[2]

        switch (Enum_Type_SSC.getEnumNameByCode(PlanCode)) {
            case Enum_Type_SSC.YiXing_DingWeiDan_WAN:
                NumAmount[Common_Method_Define.FirstNumShow] = 0
                NumAmount[Common_Method_Define.LastNumShow] = 0
                break

            case Enum_Type_SSC.YiXing_DingWeiDan_QIAN:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break

            case Enum_Type_SSC.YiXing_DingWeiDan_BAI:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break

            case Enum_Type_SSC.YiXing_DingWeiDan_SHI:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break

            case Enum_Type_SSC.YiXing_DingWeiDan_GE:
                NumAmount[Common_Method_Define.FirstNumShow] = 4
                NumAmount[Common_Method_Define.LastNumShow] = 4
                break
            case Enum_Type_SSC.BuDingWei_HouSanErMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.BuDingWei_HouSanYiMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.BuDingWei_QianSanErMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.BuDingWei_QianSanYiMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.BuDingWei_SiXingErMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.BuDingWei_SiXingYiMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.BuDingWei_WuXingErMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.BuDingWei_WuXingSanMaBuDingWei:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.DaXiaoDanShuang_HouErDaXiaoDanShuang:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.DaXiaoDanShuang_QianErDaXiaoDanShuang:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_HouErBaoDan_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_HouErDanShi_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_HouErFuShi_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_HouErHeZhi_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_HouErHeZhi_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_HouErKuaDu_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ErXing_QianErBaoDan_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_QianErDanShi_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_QianErDanShi_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_QianErFuShi_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_QianErFuShi_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_QianErHeZhi_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_QianErHeZhi_ZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.ErXing_QianErKuaDu_ZhiXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_SSC.HouSan_BaoDan:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_HunHeZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_KuaDu:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_TeShuHaoMa:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_ZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_ZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_ZhiXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_ZuLiu:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_ZuSan:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.HouSan_ZuXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_BaiGe:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_BaiShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_QianBai:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_QianGe:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_QianShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_ShiGe:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_WanBai:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_WanGe:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_WanQian:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.LongHuHe_WanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.QianSan_BaoDan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_HunHeZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_KuaDu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_TeShuHaoMa:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_ZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_ZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_ZhiXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_ZuLiu:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_ZuSan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QianSan_ZuXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_SSC.QuWei_HaoShiChengShuang:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.QuWei_SanXingBaoXi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.QuWei_SiJiFaCai:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.QuWei_YiFanFengShun:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanEr_ZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanEr_ZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanEr_ZhiXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanEr_ZuXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanEr_ZuXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanEr_ZuXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_HunHeZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZhiXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZuLiuDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZuLiuFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZuSanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZuSanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSan_ZuXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSi_ZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSi_ZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSi_ZuXuan12:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSi_ZuXuan24:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSi_ZuXuan4:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.RenXuanSi_ZuXuan6:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.SiXing_ZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.SiXing_ZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.SiXing_ZuXuan12:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.SiXing_ZuXuan24:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.SiXing_ZuXuan4:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.SiXing_ZuXuan6:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_DanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_FuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_ZuXuan10:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_ZuXuan120:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_ZuXuan20:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_ZuXuan30:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_ZuXuan5:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.WuXing_ZuXuan60:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
            case Enum_Type_SSC.ZhongSan_BaoDan:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_HunHeZuXuan:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_KuaDu:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_TeShuHaoMa:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_ZhiXuanDanShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_ZhiXuanFuShi:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_ZhiXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_ZuLiu:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_ZuSan:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_SSC.ZhongSan_ZuXuanHeZhi:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break

            default:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }
        return NumAmount
    }

    int[] Plan_NumStartAndEnd(String MethodCode) {
        int InitFirstLevel = 0
        int InitLastLevel = 9
        int[] LevelAmount = new int[2]
        // Log.e("NumberStartAndEnd", "start")
        switch (Enum_Type_SSC.getEnumNameByCode(MethodCode)) {
            default:
                LevelAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                LevelAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }
        return LevelAmount
    }




}
