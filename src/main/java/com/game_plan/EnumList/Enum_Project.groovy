package com.game_plan.EnumList

import com.common.enumList.Enum_GameDefine
import com.game_plan.beans.CategoryInfo
import com.game_plan.beans.GameItem
import com.game_plan.beans.ProjectInfo

/***
 * 计画A 内含有 1234567的玩法
 */
enum Enum_Project {
    /***
     * -------------------------------------
     */
    P_SSC_HOU_YI_DIN_WEI_5_MA("PCHYDW5M", "后一5码定位", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_SSC_HOU_YI_DIN_WEI_6_MA("PCHYDW6M", "后一6码定位", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_SSC_HOU_YI_DIN_WEI_8_MA("PCHYDW8M", "后一8码定位", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),

    P_SSC_HOU_ER_FU_SHI_8_MA("PCHEFS8M", "后二复式8码", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_SSC_HOU_ER_FU_SHI_9_MA("PCHEFS9M", "后二复式9码", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_SSC_HOU_ER_FU_SHI_7_MA("PCHEFS7M", "后二复式7码", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),


    P_SSC_HOU_ER_DAN_SHI_9X("PQHEDS9X", "后二单式9X注", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_SSC_HOU_ER_DAN_SHI_8X("PQHEDS8X", "后二单式8X注", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_SSC_HOU_ER_DAN_SHI_7X("PQHEDS7X", "后二单式7X注", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_SSC_HOU_ER_DAN_SHI_6X("PQHEDS6X", "后二单式6X注以下", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),

    P_SSC_HOU_SAN_FU_SHI_8_MA("PCHSFS8M", "后三复式8码", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_SSC_HOU_SAN_FU_SHI_9_MA("PCHSFS9M", "后三复式9码", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),

    P_SSC_HOU_SAN_DAN_SHI_9XX("PCHSAS9XX", "后三单式9XX注", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_SSC_HOU_SAN_DAN_SHI_8XX("PCHSAS8XX", "后三单式8XX注", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_SSC_HOU_SAN_DAN_SHI_7XX("PCHSAS7XX", "后三单式7XX注", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_SSC_HOU_SAN_DAN_SHI_6XX("PCHSAS6XX", "后三单式6XX注以下", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),

    P_SSC_SI_XING_ZHI_XIAN_7_MA("PCSXZX7M", "四星直选7码", Common_Method_Define.GAME_GROUP_SSC_SI_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_SSC_SI_XING_ZHI_XIAN_8_MA("PCSXZX8M", "四星直选8码", Common_Method_Define.GAME_GROUP_SSC_SI_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_SSC_SI_XING_ZHI_XIAN_9_MA("PCSXZX9M", "四星直选9码", Common_Method_Define.GAME_GROUP_SSC_SI_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),

    P_SSC_WU_XING_ZHI_XIAN_7_MA("PCWXZX7M", "五星直选7码", Common_Method_Define.GAME_GROUP_SSC_WU_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_SSC_WU_XING_ZHI_XIAN_8_MA("PCWXZX8M", "五星直选8码", Common_Method_Define.GAME_GROUP_SSC_WU_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_SSC_WU_XING_ZHI_XIAN_9_MA("PCWXZX9M", "五星直选9码", Common_Method_Define.GAME_GROUP_SSC_WU_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),

    P_SSC_ZHONG_SAN_HER_ZHI("PCZSHZ", "中三和值", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_HER_ZHI),

    P_SSC_ZHONG_SAN_KUA_DU("PCZSKD", "中三跨度", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_KUA_DU),

    /***
     *  11X5
     */

            P_11X5_HOU_YI_DIN_WEI_5_MA("P11X5HYDW5M", "后一5码定位", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_11X5_HOU_YI_DIN_WEI_6_MA("P11X5HYDW6M", "后一6码定位", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_11X5_HOU_YI_DIN_WEI_8_MA("P11X5HYDW8M", "后一8码定位", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),

    P_11X5_QIAN_ER_FU_SHI_8_MA("P11X5QEFS8M", "前二复式8码", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_11X5_QIAN_ER_FU_SHI_9_MA("P11X5QEFS9M", "前二复式9码", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_11X5_QIAN_ER_FU_SHI_7_MA("P11X5QEFS7M", "前二复式7码", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),


    P_11X5_QIAN_ER_DAN_SHI_9X("P11X5QEDS9X", "前二单式9X注", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_11X5_QIAN_ER_DAN_SHI_8X("P11X5QEDS8X", "前二单式8X注", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_11X5_QIAN_ER_DAN_SHI_7X("P11X5QEDS7X", "前二单式7X注", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_11X5_QIAN_ER_DAN_SHI_6X("P11X5QEDS6X", "前二单式6X注以下", Common_Method_Define.GAME_GROUP_SSC_ER_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),

    P_11X5_HOU_SAN_FU_SHI_8_MA("P11X5HSFS8M", "后三复式8码", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),
    P_11X5_HOU_SAN_FU_SHI_9_MA("P11X5HSFS9M", "后三复式9码", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI),

    P_11X5_HOU_SAN_DAN_SHI_9XX("P11X5HSAS9XX", "后三单式9XX注", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_11X5_HOU_SAN_DAN_SHI_8XX("P11X5HSAS8XX", "后三单式8XX注", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_11X5_HOU_SAN_DAN_SHI_7XX("P11X5HSAS7XX", "后三单式7XX注", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),
    P_11X5_HOU_SAN_DAN_SHI_6XX("P11X5HSAS6XX", "后三单式6XX注以下", Common_Method_Define.GAME_GROUP_SSC_SAN_XIN, Enum_GameAnalysisType.MODE_DAN_SHI),

    /***
     * PK10
     */
            P_PK10_DIN_WEI_DAN_NO1("PPK10DWDNO1", "冠军 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO2("PPK10DWDNO2", "亚军 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO3("PPK10DWDNO3", "季军 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO4("PPK10DWDNO4", "第四名 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO5("PPK10DWDNO5", "第五名 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO6("PPK10DWDNO6", "第六名 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO7("PPK10DWDNO7", "第七名 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO8("PPK10DWDNO8", "第八名 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO9("PPK10DWDNO9", "第九名 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),
    P_PK10_DIN_WEI_DAN_NO10("PPK10DWDNO10", "第十名 X码", Common_Method_Define.GAME_GROUP_SSC_YI_XIN, Enum_GameAnalysisType.MODE_DIN_WEI_DAN),


    private String Name
    private String code
    private String gameGroup
    private Enum_GameAnalysisType gameAnalysisType

    Enum_Project(String code, String Name, String gameGroup, Enum_GameAnalysisType gameAnalysisType) {
        this.code = code
        this.Name = Name
        this.gameGroup = gameGroup
        this.gameAnalysisType = gameAnalysisType
    }

    String getCode() {
        return code
    }

    String getName() {
        return Name
    }

    String getGroup() {
        return gameGroup
    }

    Enum_GameAnalysisType getGameAnalysisType() {
        return gameAnalysisType
    }

    @Override
    String toString() {
        return this.getCode()
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param ProjectCode
     * @return
     */
    private static Enum_Project getEnumNameByCode(String ProjectCode) {
        for (Enum_Project v : values())
            if (v.getCode().equalsIgnoreCase(ProjectCode)) {
                return v
            }
        return P_SSC_HOU_YI_DIN_WEI_5_MA
    }

/***
 * 取得 每一个游戏彩种 的 有几种 计画列表
 * @param GameCode
 * @param GameName
 * @return
 */
    static CategoryInfo getGameProjectListInfo(String CategoryCode, String CategoryName) {
        def ProjectListInCategory = new ArrayList<String>()
        def GameInfo = new CategoryInfo()
        GameInfo.Category_Code = CategoryCode
        GameInfo.Category_Name = CategoryName

        switch (Enum_GameDefine.getEnumNameByCode(CategoryCode)) {
            case Enum_GameDefine.GAME_SSC:

                ProjectListInCategory.add(P_SSC_HOU_YI_DIN_WEI_5_MA.toString())
                ProjectListInCategory.add(P_SSC_HOU_YI_DIN_WEI_6_MA.toString())
                ProjectListInCategory.add(P_SSC_HOU_YI_DIN_WEI_8_MA.toString())

                ProjectListInCategory.add(P_SSC_HOU_ER_FU_SHI_8_MA.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_FU_SHI_9_MA.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_FU_SHI_7_MA.toString())

                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_9X.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_8X.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_7X.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_6X.toString())

                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_9X.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_8X.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_7X.toString())
                ProjectListInCategory.add(P_SSC_HOU_ER_DAN_SHI_6X.toString())

                ProjectListInCategory.add(P_SSC_HOU_SAN_DAN_SHI_9XX.toString())
                ProjectListInCategory.add(P_SSC_HOU_SAN_DAN_SHI_8XX.toString())
                ProjectListInCategory.add(P_SSC_HOU_SAN_DAN_SHI_7XX.toString())
                ProjectListInCategory.add(P_SSC_HOU_SAN_DAN_SHI_6XX.toString())

                ProjectListInCategory.add(P_SSC_ZHONG_SAN_HER_ZHI)
                ProjectListInCategory.add(P_SSC_ZHONG_SAN_KUA_DU)
                GameInfo.ProjectList = ProjectListInCategory
                break
            case Enum_GameDefine.GAME_11X5:

                ProjectListInCategory.add(P_11X5_HOU_YI_DIN_WEI_5_MA.toString())
                ProjectListInCategory.add(P_11X5_HOU_YI_DIN_WEI_6_MA.toString())
                ProjectListInCategory.add(P_11X5_HOU_YI_DIN_WEI_8_MA.toString())

                ProjectListInCategory.add(P_11X5_QIAN_ER_FU_SHI_8_MA.toString())
                ProjectListInCategory.add(P_11X5_QIAN_ER_FU_SHI_9_MA.toString())
                ProjectListInCategory.add(P_11X5_QIAN_ER_FU_SHI_7_MA.toString())

                ProjectListInCategory.add(P_11X5_QIAN_ER_DAN_SHI_9X.toString())
                ProjectListInCategory.add(P_11X5_QIAN_ER_DAN_SHI_8X.toString())
                ProjectListInCategory.add(P_11X5_QIAN_ER_DAN_SHI_7X.toString())
                ProjectListInCategory.add(P_11X5_QIAN_ER_DAN_SHI_6X.toString())

                ProjectListInCategory.add(P_11X5_HOU_SAN_FU_SHI_8_MA)
                ProjectListInCategory.add(P_11X5_HOU_SAN_FU_SHI_9_MA)

                ProjectListInCategory.add(P_11X5_HOU_SAN_DAN_SHI_9XX.toString())
                ProjectListInCategory.add(P_11X5_HOU_SAN_DAN_SHI_8XX.toString())
                ProjectListInCategory.add(P_11X5_HOU_SAN_DAN_SHI_7XX.toString())
                ProjectListInCategory.add(P_11X5_HOU_SAN_DAN_SHI_6XX.toString())
                GameInfo.ProjectList = ProjectListInCategory
                break
            case Enum_GameDefine.GAME_3DP3:
                break
            case Enum_GameDefine.GAME_PK10:
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO1.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO2.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO3.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO4.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO5.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO6.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO7.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO8.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO9.toString())
                ProjectListInCategory.add(P_PK10_DIN_WEI_DAN_NO10.toString())
                GameInfo.ProjectList = ProjectListInCategory
                break
            default:
                break
        }
        return GameInfo
    }

    /***
     * 在这边设定计画包含的方案内容
     *  3期5码
     * setGameItem_GameIssueGapNumAmount  ＝ 方案分析的期数间隔 3期
     * setGameItem_GameRequireNumAmount  ＝ 推荐的号码数量 5码
     *
     * @param ProjectCode
     * @return
     */
    static ProjectInfo getInitProjectData(String CategoryCode, ProjectCode) {
        Common_Method_Define gameDefine = Common_Method_Define.instance

        def PlanCodeList = new ProjectInfo()

        Enum_Project project = getEnumNameByCode(ProjectCode)
        PlanCodeList.ProjectContainMethodCodeList = new ArrayList<GameItem>()
        PlanCodeList.Project_Code = project.getCode()
        PlanCodeList.Project_Name = project.getName()
        PlanCodeList.Project_Group = project.getGroup()

        ArrayList<Enum_Plan> planList = Enum_Plan.getPlanListByCode(project.getCode())

        for (i in 0..planList.size() - 1) {
            def planitem = planList.get(i)
            def planInfo = new GameItem()

            gameDefine.initData(CategoryCode, planitem.getAnalysisGameCode())
            planInfo.with {
                setGameItem_CategoryCode(CategoryCode)
                setGameItem_PlanCode(planitem.getPlanCode())
                setGameItem_PlanName(planitem.getPlanName())

                setGameItem_MethodCode(planitem.getAnalysisGameCode())
                setGameItem_GameRecommendType(project.getGameAnalysisType().toString())

                setGameItem_GameIssueGapNumAmount(planitem.getIssueGap())
                setGameItem_GameRequireNumAmount(planitem.getRequireNumAmount())

                setGameItem_AnalysisData_ShowNum(gameDefine.Plan_NumStartAndEnd())
                setGameItem_AnalysisData_ShowLV(gameDefine.Plan_LevelStartAndEnd())
            }
            PlanCodeList.ProjectContainMethodCodeList.add(planInfo)
        }
        return PlanCodeList
    }

}
