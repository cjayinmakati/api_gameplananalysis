package com.game_plan.EnumList

import com.common.enumList.Enum_Type_11X5
import com.common.enumList.Enum_Type_PK10
import com.common.enumList.Enum_Type_SSC

/***
 * 计画A 内含有 1234567的玩法
 */
enum Enum_Plan {
    /***
     * SSC
     */

    PCHYDW5M_3QI("PCHYDW5M", "后一3期5码_个位", "PCHYDW5M_3QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 3, 5),
    PCHYDW5M_4QI("PCHYDW5M", "后一4期5码_个位", "PCHYDW5M_4QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 4, 5),
//    PCHYDW5M_5QI("PCHYDW5M", "后一5期5码_个位", "PCHYDW5M_5QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 5, 5),

    PCHYDW6M_3QI("PCHYDW6M", "后一3期6码_个位", "PCHYDW6M_3QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 3, 6),
    PCHYDW6M_4QI("PCHYDW6M", "后一4期6码_个位", "PCHYDW6M_4QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 4, 6),
//    PCHYDW6M_5QI("PCHYDW6M", "后一5期6码_个位", "PCHYDW6M_5QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 5, 6),

    PCHYDW8M_3QI("PCHYDW8M", "后一3期8码_个位", "PCHYDW8M_3QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 3, 8),
    PCHYDW8M_4QI("PCHYDW8M", "后一4期8码_个位", "PCHYDW8M_4QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 4, 8),
//    PCHYDW8M_5QI("PCHYDW8M", "后一5期8码", "PCHYDW8M_5QI", Enum_Type_SSC.YiXing_DingWeiDan_GE.toString(), 5, 8),


    PCHEFS8M_3QI("PCHEFS8M", "后二复式3期8码", "PCHEFS8M_3QI", Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan.toString(), 3, 8),
    PCHEFS8M_4QI("PCHEFS8M", "后二复式4期8码", "PCHEFS8M_4QI", Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan.toString(), 4, 8),

    PCHEFS9M_2QI("PCHEFS9M", "后二复式2期9码", "PCHEFS9M_2QI", Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan.toString(), 2, 9),
    PCHEFS9M_1QI("PCHEFS9M", "后二复式1期9码", "PCHEFS9M_1QI", Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan.toString(), 1, 9),


    PCHEFS7M_3QI("PCHEFS7M", "后二复式3期7码", "PCHEFS7M_3QI", Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan.toString(), 3, 7),
    PCHEFS7M_2QI("PCHEFS7M", "后二复式2期7码", "PCHEFS7M_2QI", Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan.toString(), 2, 7),
    PCHEFS7M_1QI("PCHEFS7M", "后二复式1期7码", "PCHEFS7M_1QI", Enum_Type_SSC.ErXing_HouErFuShi_ZhiXuan.toString(), 1, 7),


    PQHEDS9X_9XA("PQHEDS9X", "后二单式9X注", "PQHEDS9X_9XA", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 93),
    PQHEDS9X_9XB("PQHEDS9X", "后二单式9X注", "PQHEDS9X_9XB", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 97),

    PQHEDS8X_8XA("PQHEDS8X", "后二单式8X注", "PQHEDS8X_8XA", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 82),
    PQHEDS8X_8XB("PQHEDS8X", "后二单式8X注", "PQHEDS8X_8XB", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 86),

    PQHEDS7X_7XA("PQHEDS7X", "后二单式7X注", "PQHEDS7X_7XA", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 75),
    PQHEDS7X_7XB("PQHEDS7X", "后二单式7X注", "PQHEDS7X_7XB", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 77),

    PQHEDS6X_6XA("PQHEDS6X", "后二单式6X注", "PQHEDS6X_6XA", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 60),
    PQHEDS6X_5XA("PQHEDS6X", "后二单式5X注", "PQHEDS6X_5XA", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 50),
    PQHEDS6X_4XA("PQHEDS6X", "后二单式4X注", "PQHEDS6X_4XA", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 40),
    PQHEDS6X_3XA("PQHEDS6X", "后二单式3X注", "PQHEDS6X_3XA", Enum_Type_SSC.ErXing_HouErDanShi_ZhiXuan.toString(), 3, 30),


    PCHSFS8M_2QI("PCHSFS8M", "后三复式2期8码", "PCHSFS8M_2QI", Enum_Type_SSC.HouSan_ZhiXuanFuShi.toString(), 2, 8),
    PCHSFS8M_3QI("PCHSFS8M", "后三复式3期8码", "PCHSFS8M_3QI", Enum_Type_SSC.HouSan_ZhiXuanFuShi.toString(), 3, 8),

    PCHSFS9M_1QI("PCHSFS9M", "后三复式1期9码", "PCHSFS9M_1QI", Enum_Type_SSC.HouSan_ZhiXuanFuShi.toString(), 1, 9),
    PCHSFS9M_2QI("PCHSFS9M", "后三复式2期9码", "PCHSFS9M_2QI", Enum_Type_SSC.HouSan_ZhiXuanFuShi.toString(), 2, 9),

    PCHSAS9XX_9XXA("PCHSAS9XX", "后三单式9XX注", "PCHSAS9XX_9XXA", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 932),
    PCHSAS9XX_9XXB("PCHSAS9XX", "后三单式9XX注", "PCHSAS9XX_9XXB", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 968),
    PCHSAS9XX_9XXC("PCHSAS9XX", "后三单式9XX注", "PCHSAS9XX_9XXC", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 981),

    PCHSAS8XX_8XXA("PCHSAS8XX", "后三单式8XX注", "PCHSAS8XX_8XXA", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 814),
    PCHSAS8XX_8XXB("PCHSAS8XX", "后三单式8XX注", "PCHSAS8XX_8XXB", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 823),
    PCHSAS8XX_8XXC("PCHSAS8XX", "后三单式8XX注", "PCHSAS8XX_8XXC", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 842),

    PCHSAS7XX_7XXA("PCHSAS7XX", "后三单式7XX注", "PCHSAS7XX_7XXA", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 713),
    PCHSAS7XX_7XXB("PCHSAS7XX", "后三单式7XX注", "PCHSAS7XX_7XXB", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 743),

    PCHSAS6XX_6XX("PCHSAS6XX", "后三单式6XX注", "PCHSAS6XX_6XX", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 631),
    PCHSAS6XX_5XX("PCHSAS6XX", "后三单式5XX注", "PCHSAS6XX_5XX", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 582),
    PCHSAS6XX_4XX("PCHSAS6XX", "后三单式4XX注", "PCHSAS6XX_4XX", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 422),
    PCHSAS6XX_3XX("PCHSAS6XX", "后三单式3XX注", "PCHSAS6XX_3XX", Enum_Type_SSC.HouSan_ZhiXuanDanShi.toString(), 3, 319),

    PCSXZX7M("PCSXZX7M", "四星直选7码", "PCSXZX7M", Enum_Type_SSC.SiXing_ZhiXuanFuShi.toString(), 3, 7),
    PCSXZX8M("PCSXZX8M", "四星直选8码", "PCSXZX8M", Enum_Type_SSC.SiXing_ZhiXuanFuShi.toString(), 3, 8),
    PCSXZX9M("PCSXZX9M", "四星直选9码", "PCSXZX9M", Enum_Type_SSC.SiXing_ZhiXuanFuShi.toString(), 3, 9),

    PCWXZX7M("PCWXZX7M", "五星直选7码", "PCWXZX7M", Enum_Type_SSC.WuXing_FuShi.toString(), 3, 7),
    PCWXZX8M("PCWXZX8M", "五星直选8码", "PCWXZX8M", Enum_Type_SSC.WuXing_FuShi.toString(), 3, 8),
    PCWXZX9M("PCWXZX9M", "五星直选9码", "PCWXZX9M", Enum_Type_SSC.WuXing_FuShi.toString(), 3, 9),

    PCWXZX9M_5M("PCZSHZ", "中三和值5码", "PCWXZX9M_5M", Enum_Type_SSC.ZhongSan_ZhiXuanHeZhi.toString(), 3, 5),
    PCWXZX9M_6M("PCZSHZ", "中三和值6码", "PCWXZX9M_6M", Enum_Type_SSC.ZhongSan_ZhiXuanHeZhi.toString(), 3, 6),
    PCWXZX9M_7M("PCZSHZ", "中三和值7码", "PCWXZX9M_7M", Enum_Type_SSC.ZhongSan_ZhiXuanHeZhi.toString(), 2, 7),


    PCZSKD_5M("PCZSKD", "中三跨度5码", "PCZSKD_5M", Enum_Type_SSC.ZhongSan_ZhiXuanHeZhi.toString(), 3, 5),
    PCZSKD_6M("PCZSKD", "中三跨度6码", "PCZSKD_6M", Enum_Type_SSC.ZhongSan_ZhiXuanHeZhi.toString(), 3, 6),
    PCZSKD_7M("PCZSKD", "中三跨度7码", "PCZSKD_7M", Enum_Type_SSC.ZhongSan_ZhiXuanHeZhi.toString(), 2, 7),

    /***
     * 11X5
     */

            P11X5HYDW5M_3QI("P11X5HYDW5M", "后一3期5码", "P11X5HYDW5M_3QI", Enum_Type_11X5.DingWeiDan_DingWeiDan_FIFTH.toString(), 3, 5),
    P11X5HYDW5M_4QI("P11X5HYDW5M", "后一4期5码", "P11X5HYDW5M_4QI", Enum_Type_11X5.DingWeiDan_DingWeiDan_FIFTH.toString(), 4, 5),

    P11X5HYDW6M_3QI("P11X5HYDW6M", "后一3期6码", "P11X5HYDW6M_3QI", Enum_Type_11X5.DingWeiDan_DingWeiDan_FIFTH.toString(), 3, 6),
    P11X5HYDW6M_4QI("P11X5HYDW6M", "后一4期6码", "P11X5HYDW6M_4QI", Enum_Type_11X5.DingWeiDan_DingWeiDan_FIFTH.toString(), 4, 6),

    P11X5HYDW8M_3QI("P11X5HYDW8M", "后一3期8码", "P11X5HYDW8M_3QI", Enum_Type_11X5.DingWeiDan_DingWeiDan_FIFTH.toString(), 3, 8),
    P11X5HYDW8M_4QI("P11X5HYDW8M", "后一4期8码", "P11X5HYDW8M_4QI", Enum_Type_11X5.DingWeiDan_DingWeiDan_FIFTH.toString(), 4, 8),


    P11X5QEFS8M_3QI("P11X5QEFS8M", "前二复式3期8码", "P11X5QEFS7M_3QI", Enum_Type_11X5.ErMa_QianErZhiXuanFuShi.toString(), 3, 8),
    P11X5QEFS8M_4QI("P11X5QEFS8M", "前二复式4期8码", "P11X5QEFS7M_4QI", Enum_Type_11X5.ErMa_QianErZhiXuanFuShi.toString(), 4, 8),

    P11X5QEFS9M_2QI("P11X5QEFS9M", "前二复式2期9码", "P11X5QEFS7M_2QI", Enum_Type_11X5.ErMa_QianErZhiXuanFuShi.toString(), 2, 9),
    P11X5QEFS9M_1QI("P11X5QEFS9M", "前二复式1期9码", "P11X5QEFS7M_1QI", Enum_Type_11X5.ErMa_QianErZhiXuanFuShi.toString(), 1, 9),


    P11X5QEFS7M_3QI("P11X5QEFS7M", "前二复式3期7码", "P11X5QEFS7M_3QI", Enum_Type_11X5.ErMa_QianErZhiXuanFuShi.toString(), 3, 7),
    P11X5QEFS7M_2QI("P11X5QEFS7M", "前二复式2期7码", "P11X5QEFS7M_2QI", Enum_Type_11X5.ErMa_QianErZhiXuanFuShi.toString(), 2, 7),
    P11X5QEFS7M_1QI("P11X5QEFS7M", "前二复式1期7码", "P11X5QEFS7M_1QI", Enum_Type_11X5.ErMa_QianErZhiXuanFuShi.toString(), 1, 7),


    P11X5QEDS9X_9XA("P11X5QEDS9X", "前二单式9X注", "P11X5QEDS9X_9XA", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 93),
    P11X5QEDS9X_9XB("P11X5QEDS9X", "前二单式9X注", "P11X5QEDS9X_9XB", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 97),

    P11X5QEDS8X_8XA("P11X5QEDS8X", "前二单式8X注", "P11X5QEDS8X_8XA", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 82),
    P11X5QEDS8X_8XB("P11X5QEDS8X", "前二单式8X注", "P11X5QEDS8X_8XB", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 86),

    P11X5QEDS7X_7XA("P11X5QEDS7X", "前二单式7X注", "P11X5QEDS7X_7XA", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 75),
    P11X5QEDS7X_7XB("P11X5QEDS7X", "前二单式7X注", "P11X5QEDS7X_7XB", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 77),

    P11X5QEDS6X_6XA("P11X5QEDS6X", "前二单式6X注", "P11X5QEDS6X_6XA", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 60),
    P11X5QEDS6X_5XA("P11X5QEDS6X", "前二单式5X注", "P11X5QEDS6X_5XA", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 50),
    P11X5QEDS6X_4XA("P11X5QEDS6X", "前二单式4X注", "P11X5QEDS6X_4XA", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 40),
    P11X5QEDS6X_3XA("P11X5QEDS6X", "前二单式3X注", "P11X5QEDS6X_3XA", Enum_Type_11X5.ErMa_QianErZhiXuanDanShi.toString(), 3, 30),


    P11X5HSFS8M_2QI("P11X5HSFS8M", "后三复式2期8码", "P11X5HSFS8M_2QI", Enum_Type_11X5.HouSan_HouSanZhiXuanFuShi.toString(), 2, 8),
    P11X5HSFS8M_3QI("P11X5HSFS8M", "后三复式3期8码", "P11X5HSFS8M_3QI", Enum_Type_11X5.HouSan_HouSanZhiXuanFuShi.toString(), 3, 8),

    P11X5HSFS9M_1QI("P11X5HSFS9M", "后三复式1期9码", "P11X5HSFS9M_1QI", Enum_Type_11X5.HouSan_HouSanZhiXuanFuShi.toString(), 1, 9),
    P11X5HSFS9M_2QI("P11X5HSFS9M", "后三复式2期9码", "P11X5HSFS9M_2QI", Enum_Type_11X5.HouSan_HouSanZhiXuanFuShi.toString(), 2, 9),

    P11X5HSAS9XX_9XXA("P11X5HSAS9XX", "后三单式9XX注", "P11X5HSAS9XX_9XXA", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 932),
    P11X5HSAS9XX_9XXB("P11X5HSAS9XX", "后三单式9XX注", "P11X5HSAS9XX_9XXB", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 968),
    P11X5HSAS9XX_9XXC("P11X5HSAS9XX", "后三单式9XX注", "P11X5HSAS9XX_9XXC", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 981),

    P11X5HSAS8XX_8XXA("P11X5HSAS8XX", "后三单式8XX注", "P11X5HSAS8XX_8XXA", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 814),
    P11X5HSAS8XX_8XXB("P11X5HSAS8XX", "后三单式8XX注", "P11X5HSAS8XX_8XXB", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 823),
    P11X5HSAS8XX_8XXC("P11X5HSAS8XX", "后三单式8XX注", "P11X5HSAS8XX_8XXC", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 842),

    P11X5HSAS7XX_7XXA("P11X5HSAS7XX", "后三单式7XX注", "P11X5HSAS7XX_7XXA", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 713),
    P11X5HSAS7XX_7XXB("P11X5HSAS7XX", "后三单式7XX注", "P11X5HSAS7XX_7XXB", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 743),

    P11X5HSAS6XX_6XX("P11X5HSAS6XX", "后三单式6XX注", "P11X5HSAS6XX_6XX", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 631),
    P11X5HSAS6XX_5XX("P11X5HSAS6XX", "后三单式5XX注", "P11X5HSAS6XX_5XX", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 582),
    P11X5HSAS6XX_4XX("P11X5HSAS6XX", "后三单式4XX注", "P11X5HSAS6XX_4XX", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 422),
    P11X5HSAS6XX_3XX("P11X5HSAS6XX", "后三单式3XX注", "P11X5HSAS6XX_3XX", Enum_Type_11X5.HouSan_HouSanZhiXuanDanShi.toString(), 3, 319),

    /***
     * PK10
     */


            PPK10DWDNO1_4M("PPK10DWDNO1", "冠军 4码", "PPK10DWDNO1_4M", Enum_Type_PK10.YiXing_DingWeiDan_1.toString(), 3, 4),
    PPK10DWDNO1_5M("PPK10DWDNO1", "冠军 5码", "PPK10DWDNO1_5M", Enum_Type_PK10.YiXing_DingWeiDan_1.toString(), 3, 5),
    PPK10DWDNO1_6M("PPK10DWDNO1", "冠军 6码", "PPK10DWDNO1_6M", Enum_Type_PK10.YiXing_DingWeiDan_1.toString(), 3, 6),

    PPK10DWDNO2_4M("PPK10DWDNO2", "亚军 4码", "PPK10DWDNO2_4M", Enum_Type_PK10.YiXing_DingWeiDan_2.toString(), 3, 4),
    PPK10DWDNO2_5M("PPK10DWDNO2", "亚军 5码", "PPK10DWDNO2_5M", Enum_Type_PK10.YiXing_DingWeiDan_2.toString(), 3, 5),
    PPK10DWDNO2_6M("PPK10DWDNO2", "亚军 6码", "PPK10DWDNO2_6M", Enum_Type_PK10.YiXing_DingWeiDan_2.toString(), 3, 6),

    PPK10DWDNO3_4M("PPK10DWDNO3", "季军 4码", "PPK10DWDNO3_4M", Enum_Type_PK10.YiXing_DingWeiDan_3.toString(), 3, 4),
    PPK10DWDNO3_5M("PPK10DWDNO3", "季军 5码", "PPK10DWDNO3_5M", Enum_Type_PK10.YiXing_DingWeiDan_3.toString(), 3, 5),
    PPK10DWDNO3_6M("PPK10DWDNO3", "季军 6码", "PPK10DWDNO3_6M", Enum_Type_PK10.YiXing_DingWeiDan_3.toString(), 3, 6),

    PPK10DWDNO4_4M("PPK10DWDNO4", "第四名 4码", "PPK10DWDNO4_4M", Enum_Type_PK10.YiXing_DingWeiDan_4.toString(), 3, 4),
    PPK10DWDNO4_5M("PPK10DWDNO4", "第四名 5码", "PPK10DWDNO4_5M", Enum_Type_PK10.YiXing_DingWeiDan_4.toString(), 3, 5),
    PPK10DWDNO4_6M("PPK10DWDNO4", "第四名 6码", "PPK10DWDNO4_6M", Enum_Type_PK10.YiXing_DingWeiDan_4.toString(), 3, 6),

    PPK10DWDNO5_4M("PPK10DWDNO5", "第五名 4码", "PPK10DWDNO5_4M", Enum_Type_PK10.YiXing_DingWeiDan_5.toString(), 3, 4),
    PPK10DWDNO5_5M("PPK10DWDNO5", "第五名 5码", "PPK10DWDNO5_5M", Enum_Type_PK10.YiXing_DingWeiDan_5.toString(), 3, 5),
    PPK10DWDNO5_6M("PPK10DWDNO5", "第五名 6码", "PPK10DWDNO5_6M", Enum_Type_PK10.YiXing_DingWeiDan_5.toString(), 3, 6),

    PPK10DWDNO6_4M("PPK10DWDNO6", "第六名 4码", "PPK10DWDNO6_4M", Enum_Type_PK10.YiXing_DingWeiDan_6.toString(), 3, 4),
    PPK10DWDNO6_5M("PPK10DWDNO6", "第六名 5码", "PPK10DWDNO6_5M", Enum_Type_PK10.YiXing_DingWeiDan_6.toString(), 3, 5),
    PPK10DWDNO6_6M("PPK10DWDNO6", "第六名 6码", "PPK10DWDNO6_6M", Enum_Type_PK10.YiXing_DingWeiDan_6.toString(), 3, 6),

    PPK10DWDNO7_4M("PPK10DWDNO7", "第七名 4码", "PPK10DWDNO7_4M", Enum_Type_PK10.YiXing_DingWeiDan_7.toString(), 3, 4),
    PPK10DWDNO7_5M("PPK10DWDNO7", "第七名 5码", "PPK10DWDNO7_5M", Enum_Type_PK10.YiXing_DingWeiDan_7.toString(), 3, 5),
    PPK10DWDNO7_6M("PPK10DWDNO7", "第七名 6码", "PPK10DWDNO7_6M", Enum_Type_PK10.YiXing_DingWeiDan_7.toString(), 3, 6),

    PPK10DWDNO8_4M("PPK10DWDNO8", "第八名 4码", "PPK10DWDNO8_4M", Enum_Type_PK10.YiXing_DingWeiDan_8.toString(), 3, 4),
    PPK10DWDNO8_5M("PPK10DWDNO8", "第八名 5码", "PPK10DWDNO8_5M", Enum_Type_PK10.YiXing_DingWeiDan_8.toString(), 3, 5),
    PPK10DWDNO8_6M("PPK10DWDNO8", "第八名 6码", "PPK10DWDNO8_6M", Enum_Type_PK10.YiXing_DingWeiDan_8.toString(), 3, 6),

    PPK10DWDNO9_4M("PPK10DWDNO9", "第九名 4码", "PPK10DWDNO9_4M", Enum_Type_PK10.YiXing_DingWeiDan_9.toString(), 3, 4),
    PPK10DWDNO9_5M("PPK10DWDNO9", "第九名 5码", "PPK10DWDNO9_5M", Enum_Type_PK10.YiXing_DingWeiDan_9.toString(), 3, 5),
    PPK10DWDNO9_6M("PPK10DWDNO9", "第九名 6码", "PPK10DWDNO9_6M", Enum_Type_PK10.YiXing_DingWeiDan_9.toString(), 3, 6),

    PPK10DWDNO10_4M("PPK10DWDNO10", "第十名 4码", "PPK10DWDNO10_4M", Enum_Type_PK10.YiXing_DingWeiDan_10.toString(), 3, 4),
    PPK10DWDNO10_5M("PPK10DWDNO10", "第十名 5码", "PPK10DWDNO10_5M", Enum_Type_PK10.YiXing_DingWeiDan_10.toString(), 3, 5),
    PPK10DWDNO10_6M("PPK10DWDNO10", "第十名 6码", "PPK10DWDNO10_6M", Enum_Type_PK10.YiXing_DingWeiDan_10.toString(), 3, 6),

    private String PlanName
    private String GroupCode
    private String planCode
    private String AnalysisGameCode
    private int IssueGap
    private int requireNumAmount


    Enum_Plan(String GroupCode, String PlanName, String planCode, String AnalysisGameCode, int IssueGap, int requireNumAmount) {
        this.GroupCode = GroupCode
        this.PlanName = PlanName
        this.planCode = planCode
        this.AnalysisGameCode = AnalysisGameCode
        this.IssueGap = IssueGap
        this.requireNumAmount = requireNumAmount

    }

    String getGroupCode() {
        return GroupCode
    }

    String getPlanName() {
        return PlanName
    }

    String getPlanCode() {
        return planCode
    }

    String getAnalysisGameCode() {
        return AnalysisGameCode
    }

    int getIssueGap() {
        return IssueGap
    }

    int getRequireNumAmount() {
        return requireNumAmount
    }


    @Override
    String toString() {
        return this.getGroupCode()
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param ProjectCode
     * @return
     */
    static Enum_Plan getEnumNameByCode(String PlanCode) {
        for (Enum_Plan v : values())
            if (v.getPlanCode().equalsIgnoreCase(PlanCode)) {
                return v
            }
        return PCHYDW5M_3QI
    }


    static ArrayList<Enum_Plan> getPlanListByCode(String ProjectCode) {
        ArrayList<Enum_Plan> planList = new ArrayList<>()
        for (Enum_Plan v : values())
            if (v.getGroupCode().equalsIgnoreCase(ProjectCode)) {
                planList.add(v)
            }
        return planList
    }

}
