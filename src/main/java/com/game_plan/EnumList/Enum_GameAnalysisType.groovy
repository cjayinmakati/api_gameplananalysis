package com.game_plan.EnumList


/***
 * 定义所有彩种下 玩法 对应的功能及算法
 */

enum Enum_GameAnalysisType {
    //尚未定义
    MODE_X("MODE_X"),
    //直选 复试
    MODE_ZHI_XUAN_FU_SHI("MODE_ZHI_XUAN_FU_SHI"),
    //组选 复试
    MODE_ZHU_XUAN_FU_SHI("MODE_ZHU_XUAN_FU_SHI"),
    //任选 复试
    MODE_REN_XUAN("MODE_REN_XUAN"),
    //定位胆
    MODE_DIN_WEI_DAN("MODE_DIN_WEI_DAN"),
    //不定位
    MODE_BU_DIN_WEI("MODE_BU_DIN_WEI"),
    //包胆
    MODE_BAO_DAN("MODE_BAO_DAN"),
    //大小
    MODE_DA_XIAO("MODE_DA_XIAO"),
    //单双
    MODE_DAN_SHUANG("MODE_DAN_SHUANG"),
    //单试
    MODE_DAN_SHI("MODE_DAN_SHI"),
    //组三
    MODE_SHU_SAN("MODE_SHU_SAN"),
    //组六
    MODE_SHU_LIU("MODE_SHU_LIU"),
    //和直
    MODE_HER_ZHI("MODE_HER_ZHI"),
    //和直大小
    MODE_HER_ZHI_DA_XIAO("MODE_HER_ZHI_DA_XIAO"),
    //和直 单双
    MODE_HER_ZHI_DAN_SHUANG("MODE_HER_ZHI_DAN_SHUANG"),
    //合直 大小
    MODE_HER_DA_XIAO("MODE_HER_DA_XIAO"),
    //合直 单双
    MODE_HER_DAN_SHUANG("MODE_HER_DAN_SHUANG"),
    //跨度
    MODE_KUA_DU("MODE_KUA_DU"),
    //龙虎
    MODE_PK10_LongHu("MODE_PK10_LongHu"),
    //猜数字
    MODE_PK10_CAI_SHUZI("MODE_PK10_CAI_SHUZI"),
    //猜单双
    MODE_PK10_CAI_DAN_SHUANG("MODE_PK10_CAI_DAN_SHUANG"),
    //猜大小
    MODE_PK10_CAI_DA_XIAO("MODE_PK10_CAI_DA_XIAO"),
    //K3定位胆
    MODE_K3_DIN_WEI_DAN("MODE_K3_DIN_WEI_DAN"),
    //K3和直
    MODE_K3_HER_ZHI("MODE_K3_HER_ZHI"),

    MODE_K3_HER_ZHI_DA_XIAO("MODE_K3_HER_ZHI_DA_XIAO"),

    MODE_K3_HER_ZHI_DAN_SHUANG("MODE_K3_HER_ZHI_DAN_SHUANG");

    String typeCode
    Enum_GameAnalysisType(String code){
        typeCode = code
    }

    def getTypeCode(){
        return typeCode
    }

    @Override
    String toString(){
        return getTypeCode()
    }

    /***
     * 用code找名字 并核对返回到
     *
     * @param ProjectCode
     * @return
     */
    static Enum_GameAnalysisType getEnumNameByCode(String code) {
        for (Enum_GameAnalysisType v : values())
            if (v.getTypeCode().equalsIgnoreCase(code)) {
                return v
            }
        return MODE_X
    }
//    /***
//     * 推荐的 计画号码 与 奖期核对 确认是否中奖
//     * @param opendrawMap
//     * @param recommendArrayList
//     * @param StartIssue
//     * @return
//     */
//    def matchOpenDrawWithRecommendNumber(GameItem GMITEM,
//                                         Map<String, OpenDrawInfo> opendrawMap,
//                                         String[] RecommendString,
//                                         double StartIssue) {
//        NumWonAnalysis RecommendMatchRecommend = NumWonAnalysis.instance
//        RecommendMatchRecommend.initData(GMITEM, opendrawMap, RecommendString, StartIssue)
//
//        return RecommendMatchRecommend.getIssueWONInfo.call()
//    }

//    /***
//     * 取得每一期已开奖的 推荐号码
//     * @param MethodAnalysisInfo 玩法分析资讯
//     * @param AnalysisIssueInfo
//     * @return
//     */
//    String[] getRecommend(GameItem GMITEM, ArrayList<OpenDrawInfo> AnalysisIssueInfo) {
//        HashMap<Integer, HashMap<Integer, BettingGameDetails>> recommendResult = new HashMap<Integer, HashMap<Integer, BettingGameDetails>>()
//
//        NumBallRecommend numRecommond = NumBallRecommend.instance
//
//        NumTrendAnalysis numTrendAnalysis = NumTrendAnalysis.instance
//        numTrendAnalysis.initData(GMITEM, AnalysisIssueInfo)
//
//        switch (GMITEM.getGameItem_GameRecommendType()) {
//            case Enum_GameAnalysisType.MODE_ZHU_XUAN_FU_SHI:
//                println("GetRecommend ===> 组选复式")
//                HashMap<Integer, BettingGameDetails> SingalResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
//                recommendResult.put(0, SingalResult)
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendZuXuanString()
//            case Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI:
//                println("GetRecommend ===> 直选复式")
//                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
//                recommendResult = MultiResult
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendZiXuanString()
//            case Enum_GameAnalysisType.MODE_REN_XUAN:
//                println("GetRecommend ===> 任选")
//                HashMap<Integer, BettingGameDetails> RenXuanResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
//                recommendResult.put(0, RenXuanResult)
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendZuXuanString()
//
//            case Enum_GameAnalysisType.MODE_DIN_WEI_DAN:
//            case Enum_GameAnalysisType.MODE_BU_DIN_WEI:
//            case Enum_GameAnalysisType.MODE_BAO_DAN:
//                println("GetRecommend ===> 定位胆")
//                HashMap<Integer, BettingGameDetails> DinWeiResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
//                recommendResult.put(0, DinWeiResult)
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendDinWeiDan_BuDinWeiString()
//            case Enum_GameAnalysisType.MODE_DA_XIAO:
//                println("GetRecommend ===> 大小")
//                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiDaXiaoResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
//                recommendResult = MultiDaXiaoResult
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendSingleDaXiaoString()
//            case Enum_GameAnalysisType.MODE_DAN_SHUANG:
//                println("GetRecommend ===> 单双")
//                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiDanShuangResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
//                recommendResult = MultiDanShuangResult
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendSingleDanShuangString()
//            case Enum_GameAnalysisType.MODE_DAN_SHI:
//                println("GetRecommend ===> 单式")
//                HashMap<Integer, BettingGameDetails> DanShiResult = numTrendAnalysis.getBestChooseNumber_DanShiLV(GMITEM.getGameItem_GameRequireNumAmount())
//                recommendResult.put(0, DanShiResult)
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendDanShiString()
//            case Enum_GameAnalysisType.MODE_SHU_LIU:
//            case Enum_GameAnalysisType.MODE_SHU_SAN:
//                println("GetRecommend ===> 组三 组六")
//                HashMap<Integer, BettingGameDetails> ZuSanLiuResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
//                recommendResult.put(0, ZuSanLiuResult)
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendZuSanZuLiuString()
//            case Enum_GameAnalysisType.MODE_HER_DA_XIAO:
//                println("GetRecommend ===> 合直大小")
//            case Enum_GameAnalysisType.MODE_HER_DAN_SHUANG:
//                println("GetRecommend ===> 合直单双")
//            case Enum_GameAnalysisType.MODE_HER_ZHI:
//            case Enum_GameAnalysisType.MODE_HER_ZHI_DA_XIAO:
//            case Enum_GameAnalysisType.MODE_HER_ZHI_DAN_SHUANG:
//            case Enum_GameAnalysisType.MODE_K3_HER_ZHI:
//            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DA_XIAO:
//            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DAN_SHUANG:
//                println("GetRecommend ===> 合直")
//                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiHerZhiResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
//                recommendResult = MultiHerZhiResult
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendHeiZhiString(Enum_GameAnalysisType.MODE_HER_ZHI)
//            case Enum_GameAnalysisType.MODE_KUA_DU:
//                println("GetRecommend ===> 跨度")
//                HashMap<Integer, HashMap<Integer, BettingGameDetails>> KuaDoResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
//                recommendResult = KuaDoResult
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendKauDoString()
//            case Enum_GameAnalysisType.MODE_PK10_CAI_SHUZI:
//            case Enum_GameAnalysisType.MODE_PK10_CAI_DAN_SHUANG:
//            case Enum_GameAnalysisType.MODE_PK10_CAI_DA_XIAO:
//                println("GetRecommend ===> 猜单双")
//                HashMap<Integer, BettingGameDetails> CaiShuZiResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
//                recommendResult.put(0, CaiShuZiResult)
//
//                numRecommond.initData(GMITEM, recommendResult)
//                return numRecommond.RecommendZuXuanString()
//            case Enum_GameAnalysisType.MODE_X:
//            default:
//
//                return new String[2]
//        }
//    }
}
