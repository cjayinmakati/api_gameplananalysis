package com.game_plan.EnumList

import com.common.enumList.Enum_Type_PK10

class Common_Method_PK10 {


    int[] Plan_LevelStartAndEnd(String PlanCode) {
        // 将这些随基数放入maps内
        int InitFirstLevel = 0
        int InitLastLevel = 9
        int[] NumAmount = new int[2]

        switch (Enum_Type_PK10.getEnumNameByCode(PlanCode)) {


            case Enum_Type_PK10.YiXing_DingWeiDan_1:
                NumAmount[Common_Method_Define.FirstNumShow] = 0
                NumAmount[Common_Method_Define.LastNumShow] = 0
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_2:
                NumAmount[Common_Method_Define.FirstNumShow] = 1
                NumAmount[Common_Method_Define.LastNumShow] = 1
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_3:
                NumAmount[Common_Method_Define.FirstNumShow] = 2
                NumAmount[Common_Method_Define.LastNumShow] = 2
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_4:
                NumAmount[Common_Method_Define.FirstNumShow] = 3
                NumAmount[Common_Method_Define.LastNumShow] = 3
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_5:
                NumAmount[Common_Method_Define.FirstNumShow] = 4
                NumAmount[Common_Method_Define.LastNumShow] = 4
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_6:
                NumAmount[Common_Method_Define.FirstNumShow] = 5
                NumAmount[Common_Method_Define.LastNumShow] = 5
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_7:
                NumAmount[Common_Method_Define.FirstNumShow] = 6
                NumAmount[Common_Method_Define.LastNumShow] = 6
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_8:
                NumAmount[Common_Method_Define.FirstNumShow] = 7
                NumAmount[Common_Method_Define.LastNumShow] = 7
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_9:
                NumAmount[Common_Method_Define.FirstNumShow] = 8
                NumAmount[Common_Method_Define.LastNumShow] = 8
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_10:
                NumAmount[Common_Method_Define.FirstNumShow] = 9
                NumAmount[Common_Method_Define.LastNumShow] = 9
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_QianWu:
                break
            case Enum_Type_PK10.YiXing_DingWeiDan_HouWu:
                break
            case Enum_Type_PK10.QianSan_DanShi:
                break
            case Enum_Type_PK10.QianSan_FuShi:
                break
            case Enum_Type_PK10.QianEr_DanShi:
                break
            case Enum_Type_PK10.QianEr_FuShi:
                break
            case Enum_Type_PK10.CaiDanShuang_CaiDanShuang1:
                break
            case Enum_Type_PK10.CaiDanShuang_CaiDanShuang2:
                break
            case Enum_Type_PK10.CaiDanShuang_CaiDanShuang3:
                break
            case Enum_Type_PK10.CaiDaXiao_CaiDaXiao1:
                break
            case Enum_Type_PK10.CaiDaXiao_CaiDaXiao2:
                break
            case Enum_Type_PK10.CaiDaXiao_CaiDaXiao3:
                break
            case Enum_Type_PK10.ShouWei_FuShi:
                break

            default:
                NumAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                NumAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }

        return NumAmount
    }

    int[] Plan_NumStartAndEnd(String MethodCode) {

        int InitFirstLevel = 1
        int InitLastLevel = 10
        int[] LevelAmount = new int[2]
        // Log.e("NumberStartAndEnd", "start")
        switch (Enum_Type_PK10.getEnumNameByCode(MethodCode)) {

            default:
                LevelAmount[Common_Method_Define.FirstNumShow] = InitFirstLevel
                LevelAmount[Common_Method_Define.LastNumShow] = InitLastLevel
                break
        }
        return LevelAmount
    }


}
