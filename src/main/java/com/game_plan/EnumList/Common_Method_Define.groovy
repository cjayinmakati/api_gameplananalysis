package com.game_plan.EnumList

import com.common.enumList.Enum_GameDefine
import com.common.enumList.Enum_Type_11X5
import com.common.enumList.Enum_Type_PK10
import com.common.enumList.Enum_Type_SSC
import com.game_plan.Utils.NumBallRecommend
import com.game_plan.Utils.NumWonAnalysis
import com.game_plan.beans.GameItem
import com.game_plan.beans.OpenDrawInfo

/***
 * 定义所有彩种下 玩法 对应的功能及算法
 */

@Singleton
class Common_Method_Define {
    /***
     * 玩法组
     */
    static final String GAME_GROUP_SSC_WU_XIN = "五星"
    static final String GAME_GROUP_SSC_SI_XIN = "四星"
    static final String GAME_GROUP_SSC_SAN_XIN = "三星"
    static final String GAME_GROUP_SSC_ER_XIN = "二星"
    static final String GAME_GROUP_SSC_YI_XIN = "一星"
    static final String GAME_GROUP_SSC_BAO_DAN = "胆码"
    static final String GAME_GROUP_SSC_ZHU_SAN_LIU = "组选"

    /**
     * 此区块定义所有的 玩法 内 是否中奖机制的算法类型
     */

    /***
     * 第一区块的号码POSITION
     * 时时采0 - 9
     */
    public static final int FirstNumShow = 0
    public static final int LastNumShow = 1


    public static final String GAME_SSC = "SSC"
    public static final String GAME_11X5 = "11X5"
    public static final String GAME_K3 = "K3"
    public static final String GAME_PK10 = "PK10"

    String CurrentGameCategory, CurrentGameMethod

    void initData(String CurrentGameCategory, CurrentGameMethod) {
        this.CurrentGameCategory = CurrentGameCategory
        this.CurrentGameMethod = CurrentGameMethod
    }

    /***
     * 传入彩种代码 和玩法 取得 玩法名称
     * @param GameCategory
     * @param GameMethod
     * @return
     */
    String getMethodNameByCode(String GameCategory, String MethodCode) {
        for (Enum_GameDefine code : Enum_GameDefine.values()) {
            if (code.getCode() == GameCategory) {
                switch (code) {
                    case Enum_GameDefine.GAME_SSC:
                        return Enum_Type_SSC.getEnumNameByCode(MethodCode).getMethodName().toString()
                    case Enum_GameDefine.GAME_PK10:
                        return Enum_Type_PK10.getEnumNameByCode(MethodCode).getMethodName().toString()
                    case Enum_GameDefine.GAME_11X5:
                        return Enum_Type_11X5.getEnumNameByCode(MethodCode).getMethodName().toString()
                }
            }
        }
    }

/***
 * 取得每个彩种 号码球 开始到最后的号码 CQSSC 0 ~ 9
 * @param GameCategory
 * @param GameMethod
 * @return
 */
    int[] Plan_NumStartAndEnd() {
        int[] showNum
        switch (Enum_GameDefine.getEnumNameByCode(CurrentGameCategory)) {
            case Enum_GameDefine.GAME_11X5:
                Common_Method_11X5 Item_11X5 = new Common_Method_11X5()
                showNum = Item_11X5.Plan_NumStartAndEnd(CurrentGameMethod)
                break
            case Enum_GameDefine.GAME_PK10:
                Common_Method_PK10 Item_PK10 = new Common_Method_PK10()
                showNum = Item_PK10.Plan_NumStartAndEnd(CurrentGameMethod)
                break
            case Enum_GameDefine.GAME_SSC:
                Common_Method_SSC Item_SSC = new Common_Method_SSC()
                showNum = Item_SSC.Plan_NumStartAndEnd(CurrentGameMethod)
                break
            default:
                break
        }
        return showNum
    }

/***
 * 取得每个彩种 第几区 开始到最后的区域 中三 1 ~ 4
 * @param GameCategory
 * @param GameMethod
 * @return
 */
    int[] Plan_LevelStartAndEnd() {
        int[] showLV
        switch (Enum_GameDefine.getEnumNameByCode(CurrentGameCategory)) {
            case Enum_GameDefine.GAME_11X5:
                Common_Method_11X5 Item_11X5 = new Common_Method_11X5()
                showLV = Item_11X5.Plan_LevelStartAndEnd(CurrentGameMethod)
                break
            case Enum_GameDefine.GAME_PK10:
                Common_Method_PK10 Item_PK10 = new Common_Method_PK10()
                showLV = Item_PK10.Plan_LevelStartAndEnd(CurrentGameMethod)
                break
            case Enum_GameDefine.GAME_SSC:
                Common_Method_SSC Item_SSC = new Common_Method_SSC()
                showLV = Item_SSC.Plan_LevelStartAndEnd(CurrentGameMethod)
                break
            default:
                Common_Method_SSC Item_SSC = new Common_Method_SSC()
                showLV = Item_SSC.Plan_LevelStartAndEnd(CurrentGameMethod)
                break
        }
        return showLV
    }

/***
 * 输入奖期 资讯列表
 * 取得 推荐号码 阵列
 * @param analysisIssueInfo
 * @return
 */
    String[] getRecommendNumberArray(GameItem GMITEM, ArrayList<OpenDrawInfo> analysisIssueInfo) {
        NumBallRecommend numRecommend = NumBallRecommend.instance
        numRecommend.initData(GMITEM)
        return numRecommend.getRecommendList(GMITEM, analysisIssueInfo)

    }

/***
 * 计算前几期推荐号码是否中奖
 * 取得 推荐号码 阵列
 * @param analysisIssueInfo
 * @return
 */
    def matchOpenDrawWithNumberRecommendResult(GameItem GMITEM, Map<String, OpenDrawInfo> opendrawMap, String[] RecommendString,
                                               double startIssue) throws NullPointerException {

        NumWonAnalysis RecommendMatchRecommend = NumWonAnalysis.instance
        RecommendMatchRecommend.initData(GMITEM, opendrawMap, RecommendString, startIssue)
        return RecommendMatchRecommend.getIssueWONInfo()
    }

}
