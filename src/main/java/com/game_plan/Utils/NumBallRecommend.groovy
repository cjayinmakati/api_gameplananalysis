package com.game_plan.Utils

import com.game_plan.EnumList.Enum_GameAnalysisType
import com.game_plan.beans.BettingGameDetails
import com.game_plan.beans.GameItem
import com.game_plan.beans.OpenDrawInfo


@Singleton
/***
 * 从趋势中 分析 推荐号码
 */
class NumBallRecommend {
    def CurrentGameCategory
    def CurrentMethodCode
    def CurrentGameGapIndex
    def CurrentGameContainNum
    def BetString = new StringBuffer()
    def ShowString = new StringBuffer()
    def LvS = 0
    def LvE = 0
    def NumS = 0
    def NumE = 0

    public static final int RECOMMEND_NUM_BET = 0
    public static final int RECOMMEND_NUM_SHOW = 1


    void initData(GameItem GMITEM) {
        this.CurrentGameCategory = GMITEM.getGameItem_CategoryCode()
        this.CurrentMethodCode = GMITEM.getGameItem_MethodCode()
        this.CurrentGameGapIndex = GMITEM.getGameItem_GameIssueGapNumAmount()
        this.CurrentGameContainNum = GMITEM.getGameItem_GameRequireNumAmount()
        this.LvS = GMITEM.getGameItem_AnalysisData_ShowLV()[0]
        this.LvE = GMITEM.getGameItem_AnalysisData_ShowLV()[1]
        this.NumS = GMITEM.getGameItem_AnalysisData_ShowNum()[0]
        this.NumE = GMITEM.getGameItem_AnalysisData_ShowNum()[1]
    }

    String[] getRecommendList(GameItem GMITEM, ArrayList<OpenDrawInfo> AnalysisIssueInfo) {

        HashMap<Integer, HashMap<Integer, BettingGameDetails>> recommendResult = new HashMap<Integer, HashMap<Integer, BettingGameDetails>>()


        NumTrendAnalysis numTrendAnalysis = NumTrendAnalysis.instance
        numTrendAnalysis.initData(GMITEM, AnalysisIssueInfo)

        switch (Enum_GameAnalysisType.getEnumNameByCode(GMITEM.getGameItem_GameRecommendType())) {
            case Enum_GameAnalysisType.MODE_ZHU_XUAN_FU_SHI:
                println("GetRecommend ===> 组选复式")
                HashMap<Integer, BettingGameDetails> SingalResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
                recommendResult.put(0, SingalResult)

                return RecommendZuXuanString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI:
                println("GetRecommend ===> 直选复式")
                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
                recommendResult = MultiResult

                return RecommendZiXuanString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_REN_XUAN:
                println("GetRecommend ===> 任选")
                HashMap<Integer, BettingGameDetails> RenXuanResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
                recommendResult.put(0, RenXuanResult)

                return RecommendZuXuanString.call(recommendResult)

            case Enum_GameAnalysisType.MODE_DIN_WEI_DAN:
            case Enum_GameAnalysisType.MODE_BU_DIN_WEI:
            case Enum_GameAnalysisType.MODE_BAO_DAN:
                println("GetRecommend ===> 定位胆")
                HashMap<Integer, BettingGameDetails> DinWeiResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
                recommendResult.put(0, DinWeiResult)

                return RecommendDinWeiDan_BuDinWeiString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_DA_XIAO:
                println("GetRecommend ===> 大小")
                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiDaXiaoResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
                recommendResult = MultiDaXiaoResult

                return RecommendSingleDaXiaoString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_DAN_SHUANG:
                println("GetRecommend ===> 单双")
                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiDanShuangResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
                recommendResult = MultiDanShuangResult

                return RecommendSingleDanShuangString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_DAN_SHI:
                println("GetRecommend ===> 单式")
                HashMap<Integer, BettingGameDetails> DanShiResult = numTrendAnalysis.getBestChooseNumber_DanShiLV(GMITEM.getGameItem_GameRequireNumAmount())
                recommendResult.put(0, DanShiResult)

                return RecommendDanShiString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_SHU_LIU:
            case Enum_GameAnalysisType.MODE_SHU_SAN:
                println("GetRecommend ===> 组三 组六")
                HashMap<Integer, BettingGameDetails> ZuSanLiuResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
                recommendResult.put(0, ZuSanLiuResult)

                return RecommendZuSanZuLiuString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_HER_DA_XIAO:
                println("GetRecommend ===> 合直大小")
            case Enum_GameAnalysisType.MODE_HER_DAN_SHUANG:
                println("GetRecommend ===> 合直单双")
            case Enum_GameAnalysisType.MODE_HER_ZHI:
            case Enum_GameAnalysisType.MODE_HER_ZHI_DA_XIAO:
            case Enum_GameAnalysisType.MODE_HER_ZHI_DAN_SHUANG:
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI:
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DA_XIAO:
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DAN_SHUANG:
                println("GetRecommend ===> 合直")
                HashMap<Integer, HashMap<Integer, BettingGameDetails>> MultiHerZhiResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
                recommendResult = MultiHerZhiResult

                return RecommendHeiZhiString.call(recommendResult, Enum_GameAnalysisType.MODE_HER_ZHI)
            case Enum_GameAnalysisType.MODE_KUA_DU:
                println("GetRecommend ===> 跨度")
                HashMap<Integer, HashMap<Integer, BettingGameDetails>> KuaDoResult = numTrendAnalysis.getBestChooseNumber_MultiLV()
                recommendResult = KuaDoResult

                return RecommendKauDoString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_PK10_CAI_SHUZI:
            case Enum_GameAnalysisType.MODE_PK10_CAI_DAN_SHUANG:
            case Enum_GameAnalysisType.MODE_PK10_CAI_DA_XIAO:
                println("GetRecommend ===> 猜单双")
                HashMap<Integer, BettingGameDetails> CaiShuZiResult = numTrendAnalysis.getBestChooseNumber_ZhuXuanResult()
                recommendResult.put(0, CaiShuZiResult)

                return RecommendZuXuanString.call(recommendResult)
            case Enum_GameAnalysisType.MODE_X:
            default:
                return new String[2]
        }
    }

    /***
     * 推荐号码 分成 可投注号码 及 显示号码
     * @param BetString
     * @param ShowString
     * @return
     */
    private sentBetAndShowString = { BetString, ShowString ->
        def betAndShow = new String[2]
        betAndShow[RECOMMEND_NUM_BET] = BetString.toString()

        StringBuffer newStr = new StringBuffer()
        String showStr = ShowString.toString()
        if (showStr.contains("|")) {
            String[] LVArr = showStr.split("\\|")
            LVArr.each {
                String[] numberArr = it.split(",").sort()
                numberArr.each {
                    newStr.append(it)
                    newStr.append(",")
                }
                newStr.setLength(newStr.length() - 1)
                newStr.append("|")
            }
            newStr.setLength(newStr.length() - 1)
        } else if (showStr.contains("*")) {
            String[] LVArr = showStr.split("\\*")
            LVArr.each {
                String[] numberArr = it.split(",").sort()
                numberArr.each {
                    newStr.append(it)
                    newStr.append(",")
                }
                newStr.setLength(newStr.length() - 1)
                newStr.append("|")
            }
            newStr.setLength(newStr.length() - 1)
        } else {
            String[] numberArr = showStr.split(",").sort()
            numberArr.each {
                newStr.append(it)
                newStr.append(",")
            }
            newStr.setLength(newStr.length() - 1)
        }

        betAndShow[RECOMMEND_NUM_SHOW] = newStr.toString()
        return betAndShow
    }

    /***
     * 推荐号码－不定位
     * @return
     */
    private RecommendDinWeiDan_BuDinWeiString = { RecommendResult ->
        int Final = CurrentGameContainNum + NumS > NumE ? NumE : CurrentGameContainNum + NumS
        BetString = new StringBuffer()
        ShowString = new StringBuffer()
        for (i in NumS..<Final) {
            String Divider = (i != Final - 1 ? "," : "")
            BettingGameDetails GameNum = RecommendResult.get(0).get(i)

            BetString.append(GameNum.number + Divider)
            ShowString.append(GameNum.Title + Divider)
        }
        return sentBetAndShowString.call(BetString, ShowString)
    }

    /***
     * 推荐号码－组选
     * @return
     */
    private RecommendZuXuanString = { RecommendResult ->
        int Final = CurrentGameContainNum + NumS > NumE ? NumE : CurrentGameContainNum + NumS
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        for (i in NumS..<Final) {
            BettingGameDetails GameNum = RecommendResult.get(0).get(i)
            String Divider = (i != Final - 1 ? "," : "")
            BetString.append(GameNum.number + Divider)
            ShowString.append(GameNum.Title + Divider)
        }

        return sentBetAndShowString.call(BetString, ShowString)
    }

    /***
     * 推荐号码－直选
     * @return
     */
    private RecommendZiXuanString = { RecommendResult ->
        int Final = CurrentGameContainNum + NumS > NumE ? NumE : CurrentGameContainNum + NumS
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        for (LV in LvS..LvE) {
            for (Num in NumS..<Final) {
                String Divider = (Num != Final - 1 ? "," : "")
                BettingGameDetails GameNum = RecommendResult.get(LV).get(Num)

                BetString.append(GameNum.number + Divider)
                ShowString.append(GameNum.Title + Divider)
            }
            if (LV != LvE) {
                BetString.append("|")
                ShowString.append("|")
            }
        }
        return sentBetAndShowString.call(BetString, ShowString)
    }

    /***
     * 推荐号码－大小
     * @return
     */
    private RecommendSingleDaXiaoString = { RecommendResult ->
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        int Final = NumS + 1// 只会推荐一个
        for (LV in LvS..LvE) {
            for (Num in NumS..<Final) {
                println("RecommendSingleDaXiaoString = " + LV)
                BettingGameDetails GameNum = RecommendResult.get(LV).get(Num)
                BetString.append(NumDaXiaoDanZhung.DeterminDaXiao(CurrentGameCategory, GameNum.number))
                ShowString.append(NumDaXiaoDanZhung.DeterminDaXiao(CurrentGameCategory, GameNum.number))
            }
            if (LV != LvE) {
                BetString.append("*")
                ShowString.append("*")
            }
        }
        return sentBetAndShowString.call(BetString, ShowString)
    }

    /***
     * 推荐号码－单双
     * @return
     */
    private RecommendSingleDanShuangString = { RecommendResult ->
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        int Final = NumS + 1
        for (LV in LvS..LvE) {
            for (Num in NumS..<Final) {
                BettingGameDetails GameNum = RecommendResult.get(LV).get(Num)
                BetString.append(NumDaXiaoDanZhung.DeterminDanShuang(GameNum.number))
                ShowString.append(NumDaXiaoDanZhung.DeterminDanShuang(GameNum.number))
            }
            if (LV != LvE) {
                BetString.append("*")
                ShowString.append("*")
            }
        }
        return sentBetAndShowString.call(BetString, ShowString)
    }

    /***
     * 推荐号码－单式
     * @return
     */
    private RecommendDanShiString = { RecommendResult ->
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        RecommendResult.get(0).each { key, value ->
            String Divider = CurrentGameCategory.contains("11X5") ? "," : " "
            BetString.append(value.number + Divider)
            ShowString.append(value.Title + Divider)
        }
        return sentBetAndShowString.call(BetString, ShowString)
    }

    /***
     * 推荐号码－组三 及 组六
     * @return
     */
    private RecommendZuSanZuLiuString = { RecommendResult ->
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        int Final = CurrentGameContainNum + NumS > NumE ? NumE : CurrentGameContainNum + NumS
        for (Num in NumS..<Final) {
            BettingGameDetails GameNum = RecommendResult.get(0).get(Num)
            String Divider = (Num != Final - 1 ? "," : "")
            BetString.append(GameNum.number + Divider)
            ShowString.append(GameNum.Title + Divider)
        }
        return sentBetAndShowString.call(BetString, ShowString)
    }

    /***
     * 推荐号码－跨度
     * @return
     */
    private RecommendKauDoString = { RecommendResult ->
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        String[] RecommendResult_Show = new String[2]// 这边
        ArrayList<String> KuaDoList = new ArrayList<String>()

        for (i in NumS..<NumE) {
            for (j in NumS..<NumE) {
                for (k in NumS..<NumE) {
                    if (KuaDoList.size() >= CurrentGameContainNum) {
                        break
                    } else {
                        def NumAreaA = Integer.valueOf(RecommendResult.get(LvS).get(i).number)
                        def NumAreaB = Integer.valueOf(RecommendResult.get(LvS + 1).get(j).number)
                        def NumAreaC = Integer.valueOf(RecommendResult.get(LvS + 2).get(k).number)

                        println("NumAreaA=" + NumAreaA)
                        println("NumAreaB=" + NumAreaB)
                        println("NumAreaC=" + NumAreaC)
                        if (NumAreaA != NumAreaB && NumAreaB != NumAreaC && NumAreaA != NumAreaC) {
                            Integer[] ary = new Integer[3]
                            ary[0] = NumAreaA
                            ary[1] = NumAreaB
                            ary[2] = NumAreaC
                            Arrays.sort(ary)
                            String KuaDo = String.valueOf(ary[2] - ary[0])

                            if (!KuaDoList.contains(KuaDo)) {
                                KuaDoList.add(KuaDo)
                                println("HerZhi = " + KuaDo)
                            }
                        }
                    }
                }
                if (KuaDoList.size() >= CurrentGameContainNum) {
                    break
                }
            }
            if (KuaDoList.size() >= CurrentGameContainNum) {
                break
            }
        }

        for (L in 0..KuaDoList.size() - 1) {
            BetString.append(KuaDoList.get(L))
            ShowString.append(KuaDoList.get(L))
            if (L != KuaDoList.size() - 1) {
                BetString.append(",")
                ShowString.append(",")
            }
        }
        RecommendResult_Show[0] = BetString.toString()
        RecommendResult_Show[1] = ShowString.toString()

        return RecommendResult_Show
    }

    /***
     * 推荐号码－和值
     * @return
     */
    private RecommendHeiZhiString = { RecommendResult, HerZhiType ->
        BetString = new StringBuffer()
        ShowString = new StringBuffer()

        ArrayList<String> HerZhiList = new ArrayList<String>()

        for (i in NumS..<NumE) {
            for (j in NumS..<NumE) {
                for (k in NumS..<NumE) {
                    if (HerZhiList.size() >= CurrentGameContainNum) {
                        break
                    } else {
                        int NumAreaA = Integer.valueOf(RecommendResult.get(LvS).get(i).number)
                        int NumAreaB = Integer.valueOf(RecommendResult.get(LvS + 1).get(j).number)
                        int NumAreaC = Integer.valueOf(RecommendResult.get(LvS + 2).get(k).number)

                        println("NumAreaA=" + NumAreaA)
                        println("NumAreaB=" + NumAreaB)
                        println("NumAreaC=" + NumAreaC)

                        String HerZhi = String.valueOf(NumAreaA + NumAreaB + NumAreaC)
                        if (!HerZhiList.contains(HerZhi)) {
                            HerZhiList.add(HerZhi)
                            println("HerZhi = " + HerZhi)
                        }
                    }
                }
                if (HerZhiList.size() >= CurrentGameContainNum) {
                    break
                }
            }
            if (HerZhiList.size() >= CurrentGameContainNum) {
                break
            }
        }

        switch (HerZhiType) {
            case Enum_GameAnalysisType.MODE_HER_ZHI:
                for (L in 0..HerZhiList.size() - 1) {
                    BetString.append(HerZhiList.get(L))
                    ShowString.append(HerZhiList.get(L))
                    if (L != HerZhiList.size() - 1) {
                        BetString.append(",")
                        ShowString.append(",")
                    }
                }
                println("MODE_HER_ZHI = " + ShowString.toString())
                break
            case Enum_GameAnalysisType.MODE_HER_ZHI_DA_XIAO:
                BetString.append(NumDaXiaoDanZhung.DeterminDaXiao(CurrentGameCategory, HerZhiList.get(0)))
                ShowString.append(NumDaXiaoDanZhung.DeterminDaXiao(CurrentGameCategory, HerZhiList.get(0)))
                println("MODE_HER_ZHI_DA_XIAO = " + ShowString.toString())
                break
            case Enum_GameAnalysisType.MODE_HER_ZHI_DAN_SHUANG:
                BetString.append(NumDaXiaoDanZhung.DeterminDanShuang(HerZhiList.get(0)))
                ShowString.append(NumDaXiaoDanZhung.DeterminDanShuang(HerZhiList.get(0)))
                println("MODE_HER_ZHI_DAN_SHUANG = " + ShowString.toString())
                break

            case Enum_GameAnalysisType.MODE_K3_HER_ZHI:
                for (L in 0..HerZhiList.size() - 1) {
                    // 取个位数 当做判断的依据
                    String RecommendNum = HerZhiList.get(L)
                    String GeWei = String.valueOf(RecommendNum.charAt(RecommendNum.length() - 1))
                    BetString.append(GeWei)
                    ShowString.append(GeWei)
                    println("GeWei=" + GeWei)
                    if (L != HerZhiList.size() - 1) {
                        BetString.append(",")
                        ShowString.append(",")
                    }
                }
                println("MODE_K3_HER_ZHI = " + ShowString.toString())
                break
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DA_XIAO:
                String RecommendDaXiaoNum = HerZhiList.get(0)

                BetString.append(NumDaXiaoDanZhung.DeterminDaXiao(CurrentGameCategory,
                        String.valueOf(RecommendDaXiaoNum.charAt(RecommendDaXiaoNum.length() - 1))))
                ShowString.append(NumDaXiaoDanZhung.DeterminDaXiao(CurrentGameCategory,
                        String.valueOf(RecommendDaXiaoNum.charAt(RecommendDaXiaoNum.length() - 1))))

                println("MODE_K3_HER_ZHI_DA_XIAO = " + ShowString.toString())
                break
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DAN_SHUANG:
                String RecommendDanShuangNum = HerZhiList.get(0)

                BetString.append(NumDaXiaoDanZhung.DeterminDanShuang(
                        String.valueOf(RecommendDanShuangNum.charAt(RecommendDanShuangNum.length() - 1))))
                ShowString.append(NumDaXiaoDanZhung.DeterminDanShuang(
                        String.valueOf(RecommendDanShuangNum.charAt(RecommendDanShuangNum.length() - 1))))
                println("MODE_K3_HER_ZHI_DAN_SHUANG = " + ShowString.toString())
                break

        }

        return sentBetAndShowString.call(BetString, ShowString)

    }

    // public String[] RecommendMultiDanShuangString() {
    // int LvS = showLV[0]
    // int LvE = showLV[1]
    // int NumS = showNum[0]
    // int NumE = showNum[1]
    //
    // StringBuffer BetString = new StringBuffer()
    // StringBuffer ShowString = new StringBuffer()
    // int Final = CurrentGameContainNum + NumS > NumE ? NumE :
    // CurrentGameContainNum + NumS
    // for (int LV = LvS LV <= LvE LV++) {
    // for (int i = NumS i < Final i++) {
    // BettingGameDetails GameNum = RecommendResult.get(LV).get(i)
    // BetString.append(NumWonAnalysis.DeterminDanShuang(GameNum.number))
    // ShowString.append(NumWonAnalysis.DeterminDanShuang(GameNum.number))
    // }
    // if (LV != LvE) {
    // BetString.append("*")
    // ShowString.append("*")
    // }
    // }
    //
    // return sentBetAndShowString(BetString, ShowString)
    // }

    // public String[] RecommendMultiDaXiaoString() {
    // int LvS = showLV[0]
    // int LvE = showLV[1]
    // int NumS = showNum[0]
    // int NumE = showNum[1]
    //
    // int Final = CurrentGameContainNum + NumS > NumE ? NumE :
    // CurrentGameContainNum + NumS
    //
    // StringBuffer BetString = new StringBuffer()
    // StringBuffer ShowString = new StringBuffer()
    // for (int LV = LvS LV <= LvE LV++) {
    // for (int i = NumS i < Final i++) {
    // BettingGameDetails GameNum = RecommendResult.get(LV).get(i)
    //
    // BetString.append(NumWonAnalysis.DeterminDaXiao(CurrentGameCategory,
    // GameNum.number))
    // ShowString.append(NumWonAnalysis.DeterminDaXiao(CurrentGameCategory,
    // GameNum.number))
    // }
    // if (LV != LvE) {
    // BetString.append("*")
    // ShowString.append("*")
    // }
    // }
    // return sentBetAndShowString(BetString, ShowString)
    // }
}
