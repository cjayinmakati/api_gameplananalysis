package com.game_plan.Utils

import com.common.enumList.Enum_GameDefine
import com.common.enumList.Enum_Type_PK10
import com.game_plan.EnumList.Enum_GameAnalysisType
import com.game_plan.beans.GameItem
import com.game_plan.beans.OpenDrawInfo

@Singleton
class NumWonAnalysis {
    private OpenDrawMap = new HashMap<String, OpenDrawInfo>()

    private String currentCategoryCode
    private String currentMethodCode
    private double FirstIssue
    private int currentIssueGap
    private int containNumAmount
    private int[] showLV
    private int[] showNum
    private String RecommendString_Show
    private String RecommendString_Bet
    int LvS, LvE
    private GameItem GMITEM
    boolean is11X5orPK10


    void initData(GameItem GMITEM, Map<String, OpenDrawInfo> OpenDrawMap, String[] RecommendString, double FirstIssue) {
        this.GMITEM = GMITEM
        this.OpenDrawMap = OpenDrawMap
        this.FirstIssue = FirstIssue

        this.currentIssueGap = GMITEM.getGameItem_GameIssueGapNumAmount()
        this.containNumAmount = GMITEM.getGameItem_GameRequireNumAmount()
        this.currentCategoryCode = GMITEM.getGameItem_CategoryCode()
        this.currentMethodCode = GMITEM.getGameItem_MethodCode()
        this.showLV = GMITEM.getGameItem_AnalysisData_ShowLV()
        this.showNum = GMITEM.getGameItem_AnalysisData_ShowNum()

        this.LvS = showLV[0]
        this.LvE = showLV[1]

        String[] RecommendStringArray = RecommendString
        RecommendString_Show = RecommendStringArray[NumBallRecommend.RECOMMEND_NUM_SHOW]
        RecommendString_Bet = RecommendStringArray[NumBallRecommend.RECOMMEND_NUM_BET]

        is11X5orPK10 = GMITEM.getGameItem_CategoryCode() == Enum_GameDefine.GAME_11X5.getCode() || (GMITEM.getGameItem_CategoryCode() == Enum_GameDefine.GAME_PK10.getCode())

    }


    def getIssueWONInfo() {
        ArrayList<String> RecommendArray = new ArrayList<String>(Arrays.asList(is11X5orPK10 ? RecommendString_Show.split(",") : RecommendString_Bet.split(",")))
        def RecommendCorrectTime = 0
        def RecommendErrorTime = 0
        double FirstIssueNum = FirstIssue
        LinkedHashMap<String, Serializable> winInfo

        def LastIssue = FirstIssueNum + currentIssueGap - 1
        for (double IssueIndex in FirstIssueNum..LastIssue) {
            switch (Enum_GameAnalysisType.getEnumNameByCode(GMITEM.getGameItem_GameRecommendType())) {
                case Enum_GameAnalysisType.MODE_ZHU_XUAN_FU_SHI:
                    println("matchOpenDrawWithRecommend ===> 组选 复式")
                    winInfo = getIssueWonInfo_ZhuXuanRecommend.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_ZHI_XUAN_FU_SHI:
                    println("matchOpenDrawWithRecommend ===> 直选复式")
                    winInfo = getIssueWonInfo_ZhiXuanRecommend.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_REN_XUAN:
                    println("matchOpenDrawWithRecommend ===> 任选")
                    winInfo = getIssueWonInfo_RenXuanRecommend.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_DIN_WEI_DAN:
                case Enum_GameAnalysisType.MODE_BU_DIN_WEI:
                case Enum_GameAnalysisType.MODE_BAO_DAN:
                    winInfo = getIssueWonInfo_DinWeiDan.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_DA_XIAO:
                    println("matchOpenDrawWithRecommend ===> 大小")
                    winInfo = getIssueWonInfo_DaXiaoRecommend.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_DAN_SHUANG:
                    println("matchOpenDrawWithRecommend ===> 单双")
                    winInfo = getIssueWonInfo_DanShuangRecommend.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_SHU_LIU:
                    println("matchOpenDrawWithRecommend ===> 组六")
                    winInfo = getIssueWonInfo_ShuLiuOrSanRecommend.call(RecommendArray, IssueIndex, LastIssue, false)
                    break
                case Enum_GameAnalysisType.MODE_SHU_SAN:
                    println("matchOpenDrawWithRecommend ===> 组三")
                    winInfo = getIssueWonInfo_ShuLiuOrSanRecommend.call(RecommendArray, IssueIndex, LastIssue, true)
                    break
                case Enum_GameAnalysisType.MODE_DAN_SHI:
                    println("matchOpenDrawWithRecommend ===> 单式")
                    winInfo = getIssueWonInfo_DanShiRecommend.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_HER_ZHI:
                    println("matchOpenDrawWithRecommend ===> 和直")
                    winInfo = getIssueWonInfo_HerZhiRecommend.call(RecommendArray, IssueIndex, LastIssue, Enum_GameAnalysisType.MODE_HER_ZHI)
                    break
                case Enum_GameAnalysisType.MODE_HER_ZHI_DA_XIAO:
                    println("matchOpenDrawWithRecommend ===> 和直大小")
                    winInfo = getIssueWonInfo_HerZhiRecommend.call(RecommendArray, IssueIndex, LastIssue, Enum_GameAnalysisType.MODE_HER_ZHI_DA_XIAO)
                    break
                case Enum_GameAnalysisType.MODE_HER_ZHI_DAN_SHUANG:
                    println("matchOpenDrawWithRecommend ===> 和直单双")
                    winInfo = getIssueWonInfo_HerZhiRecommend.call(RecommendArray, IssueIndex, LastIssue, Enum_GameAnalysisType.MODE_HER_ZHI_DAN_SHUANG)
                    break
                case Enum_GameAnalysisType.MODE_K3_HER_ZHI:
                    println("matchOpenDrawWithRecommend ===> 合直")
                    winInfo = getIssueWonInfo_HerZhiRecommend.call(RecommendArray, IssueIndex, LastIssue, Enum_GameAnalysisType.MODE_K3_HER_ZHI)
                    break
                case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DA_XIAO:
                    println("matchOpenDrawWithRecommend ===> 合直大小")
                    winInfo = getIssueWonInfo_HerZhiRecommend.call(RecommendArray, IssueIndex, LastIssue, Enum_GameAnalysisType.MODE_K3_HER_ZHI_DA_XIAO)
                    break
                case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DAN_SHUANG:
                    println("matchOpenDrawWithRecommend ===> 合直单双")
                    winInfo = getIssueWonInfo_HerZhiRecommend.call(RecommendArray, IssueIndex, LastIssue, Enum_GameAnalysisType.MODE_K3_HER_ZHI_DAN_SHUANG)
                    break
                case Enum_GameAnalysisType.MODE_KUA_DU:
                    println("matchOpenDrawWithRecommend ===> 胆拖")
                    winInfo = getIssueWonInfo_KuaDoRecommend.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_PK10_CAI_SHUZI:
                case Enum_GameAnalysisType.MODE_PK10_CAI_DAN_SHUANG:
                case Enum_GameAnalysisType.MODE_PK10_CAI_DA_XIAO:
                    println("matchOpenDrawWithRecommend ===> 猜数字")
                    winInfo = getIssueWonInfo_Pk10_CaiShuZi.call(RecommendArray, IssueIndex, LastIssue)
                    break
                case Enum_GameAnalysisType.MODE_X:
                    return null
                default:
                    return null
            }

            if (winInfo) {
                if (winInfo.get("isWon") as boolean) {
                    RecommendCorrectTime += 1
                } else {
                    RecommendErrorTime += 1
                }
                if (NumDaXiaoDanZhung.DFMT(IssueIndex) == NumDaXiaoDanZhung.DFMT(LastIssue)) {
                    if (winInfo.get("isWon") as boolean) {
                        return WinOrLossInfoClosure.call(true, winInfo.get("wonStatus"), winInfo.get("winIssue"), RecommendCorrectTime, RecommendErrorTime)
                    } else {
                        return WinOrLossInfoClosure.call(false, winInfo.get("wonStatus"), winInfo.get("winIssue"), RecommendCorrectTime, RecommendErrorTime)
                    }
                } else {
                    println("ISWON尚未结束！")
                }
            }

        }
    }
/***
 * 回传盈亏资讯 必包
 */
    def WinOrLossInfoClosure = {
        isWon, winIssue, winStatus, RecommendCorrectTime, RecommendErrorTime ->
            if (isWon) {
                return [isWon           : isWon,
                        wonStatus       : winStatus,
                        wonIssue        : winIssue,
                        recommendString : RecommendString_Show,
                        wonMSG          : "中" + "(" + RecommendCorrectTime + ")",
                        PlanCorrectTimes: RecommendCorrectTime,
                        PlanErrorTimes  : RecommendErrorTime,
                ]
            } else {
                return [isWon           : isWon,
                        wonStatus       : winStatus,
                        wonIssue        : winIssue,
                        recommendString : RecommendString_Show,
                        wonMSG          : "挂",
                        PlanCorrectTimes: RecommendCorrectTime,
                        PlanErrorTimes  : RecommendErrorTime,
                ]
            }
    }
    /***
     * 定位胆玩法
     * 前一 后一 单胆 双胆 类
     */
    def getIssueWonInfo_DinWeiDan = { RecommendArray, IssueIndex, LastIssue ->

        def IsCorrectAnalysis = false
        StringBuffer CorrectNum = new StringBuffer()
        String currentIssue = NumDaXiaoDanZhung.DFMT(IssueIndex)
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(currentIssue)
        if (DrawInfoItem == null || DrawInfoItem.game_Code == null) {
            return null
        }
        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))

        println("OpenCode = " + OpenCode)
        //只要有一个中，就中奖
        for (LV in LvS..LvE) {
            String OpenNum = OpenCodeList.get(LV)
            if (RecommendArray.contains(OpenNum)) {
                CorrectNum.append(OpenNum)
                IsCorrectAnalysis = true
                break
            } else {
                IsCorrectAnalysis = false
            }
        }


        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }
    /***
     * 组选玩法
     */
    def getIssueWonInfo_ZhuXuanRecommend = { RecommendArray, IssueIndex, LastIssue ->

        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))

        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue

        boolean IsCorrectAnalysis = false
        StringBuffer CorrectNum = new StringBuffer()
        for (LV in LvS..LvE) {
            String OpenNum = String.valueOf(OpenCodeList.get(LV))
            if (RecommendArray.contains(OpenNum)) {
                IsCorrectAnalysis = true
                CorrectNum.append(OpenNum)
            } else {
                // 只要有一个不中，全部不中
                IsCorrectAnalysis = false
                break
            }
        }


        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)

    }

    /***
     * 任选玩法
     */
    def getIssueWonInfo_RenXuanRecommend = { RecommendArray, IssueIndex, LastIssue ->
        int RenXuanAmount = Integer.parseInt(String.valueOf(currentMethodCode.charAt(currentMethodCode.length() - 1)))
        println("RenXuanAmount=" + RenXuanAmount)

        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))

        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue

        StringBuffer CorrectNum = new StringBuffer()
        int CorrectAmount = 0
        for (LV in LvS..LvE) {
            String OpenNum = String.valueOf(OpenCodeList.get(LV))
            if (RecommendArray.contains(OpenNum)) {
                CorrectNum.append(OpenNum)
                CorrectAmount++
            } else {
                // 只要有一个不中，全部不中
            }
        }
//        if (CorrectAmount >= RenXuanAmount) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//
//        return winInfo

        //譬如 任选三 代表至少要中三个号码
        boolean IsCorrectAnalysis = CorrectAmount >= RenXuanAmount

        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)

    }

    /***
     * Pk10
     */
    def getIssueWonInfo_Pk10_CaiShuZi = { RecommendArray, IssueIndex, LastIssue ->
        boolean IsCorrectAnalysis

        int PositionIndex = 0
        String FinalResult = ""
        StringBuffer CorrectNum = new StringBuffer()
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))

        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue

        ArrayList<String> RecommendArr = new ArrayList<String>()
        switch (Enum_Type_PK10.getEnumNameByCode(currentMethodCode)) {
            case Enum_Type_PK10.CaiDaXiao_CaiDaXiao1:
            case Enum_Type_PK10.CaiDaXiao_CaiDaXiao2:
            case Enum_Type_PK10.CaiDaXiao_CaiDaXiao3:
                println("CaiDaXiao_1~11")
                PositionIndex = Integer.parseInt(currentMethodCode.substring(7))
                println("PositionIndex = " + PositionIndex)
                FinalResult = NumDaXiaoDanZhung.DeterminDaXiao(currentCategoryCode, OpenCodeList.get(PositionIndex))
                RecommendArr.add(NumDaXiaoDanZhung.DeterminDaXiao(currentCategoryCode, RecommendString_Bet.split(",")[0]))
                break
            case Enum_Type_PK10.CaiDanShuang_CaiDanShuang1:
            case Enum_Type_PK10.CaiDanShuang_CaiDanShuang2:
            case Enum_Type_PK10.CaiDanShuang_CaiDanShuang3:
                println("CaiDanShuang_1~11")
                PositionIndex = Integer.parseInt(currentMethodCode.substring(7))
                println("PositionIndex = " + PositionIndex)
                FinalResult = NumDaXiaoDanZhung.DeterminDanShuang(OpenCodeList.get(PositionIndex))
                RecommendArr.add(NumDaXiaoDanZhung.DeterminDanShuang(RecommendString_Bet.split(",")[0]))
                break
            default:
                println("CaiShuZi_1~11")
                PositionIndex = Integer.parseInt(currentMethodCode.substring(7))
                println("PositionIndex = " + PositionIndex)
                println("RecommendString_Bet = " + RecommendString_Bet)
                FinalResult = OpenCodeList.get(PositionIndex)
                RecommendArr = new ArrayList<String>(Arrays.asList(RecommendString_Bet.split(",")))
                break
        }

        println("RecommendArr = " + RecommendArr.size())
        println("FinalResult = " + FinalResult)
        if (RecommendArr.contains(FinalResult.trim())) {
            IsCorrectAnalysis = true
            CorrectNum.append(FinalResult)
        } else {
            IsCorrectAnalysis = false
        }

//        if (IsCorrectAnalysis) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        return winInfo

        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)

    }

    /***
     *
     */
    def getIssueWonInfo_HerZhiRecommend = { RecommendArray, IssueIndex, LastIssue, HerZhiType ->
        boolean IsCorrectAnalysis

        StringBuffer CorrectNum = new StringBuffer()
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }
        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))

        int Opendraw_HerZhi_AreaA = Integer.valueOf(OpenCodeList.get(showLV[0]))
        int Opendraw_HerZhi_AreaB = Integer.valueOf(OpenCodeList.get(showLV[0] + 1))
        int Opendraw_HerZhi_AreaC = Integer.valueOf(OpenCodeList.get(showLV[0] + 2))
        String TotalHerZhi = ""
        String HerZhi = String.valueOf(Opendraw_HerZhi_AreaA + Opendraw_HerZhi_AreaB + Opendraw_HerZhi_AreaC)
        String DaXiao = NumDaXiaoDanZhung.DeterminDaXiao(currentCategoryCode, HerZhi)
        String DanShuang = NumDaXiaoDanZhung.DeterminDanShuang(HerZhi)

        String HerZhiS = String.valueOf(HerZhi.charAt(HerZhi.length() - 1))
        String DaXiaoS = NumDaXiaoDanZhung.DeterminDaXiao(currentCategoryCode, HerZhiS)
        String DanShuangS = NumDaXiaoDanZhung.DeterminDanShuang(HerZhiS)

        switch (HerZhiType) {
            case Enum_GameAnalysisType.MODE_HER_ZHI:
                TotalHerZhi = HerZhi
                break
            case Enum_GameAnalysisType.MODE_HER_ZHI_DA_XIAO:
                TotalHerZhi = DaXiao
                break
            case Enum_GameAnalysisType.MODE_HER_ZHI_DAN_SHUANG:
                TotalHerZhi = DanShuang
                break
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI:
                TotalHerZhi = HerZhiS
                break
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DA_XIAO:
                TotalHerZhi = DaXiaoS
                break
            case Enum_GameAnalysisType.MODE_K3_HER_ZHI_DAN_SHUANG:
                TotalHerZhi = DanShuangS
                break
        }

        println("OpenDraw_HerZhi = " + TotalHerZhi)
        println("RecommendResult_Bet = " + RecommendString_Bet + "_____")
        println("RecommendResult_Bet = " + (RecommendString_Bet.contains(TotalHerZhi) ? "T" : "F"))

        ArrayList<String> RecommendArr = new ArrayList<String>(Arrays.asList(RecommendString_Bet.split(",")))
        if (RecommendArr.contains(TotalHerZhi)) {
            IsCorrectAnalysis = true
            CorrectNum.append(TotalHerZhi)
        } else {
            IsCorrectAnalysis = false
        }

//        if (IsCorrectAnalysis == true) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        return winInfo


        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }

    /***
     * 跨度玩法
     */
    def getIssueWonInfo_KuaDoRecommend = { RecommendArray, IssueIndex, LastIssue ->
        boolean IsCorrectAnalysis

        println("DFMTFirstIssueNum = " + NumDaXiaoDanZhung.DFMT(FirstIssue))
        StringBuffer CorrectNum = new StringBuffer()
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }
        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))

        int[] IntArray = new int[3]
        IntArray[0] = Integer.valueOf(OpenCodeList.get(showLV[0]))
        IntArray[1] = Integer.valueOf(OpenCodeList.get(showLV[0] + 1))
        IntArray[2] = Integer.valueOf(OpenCodeList.get(showLV[1]))
        Arrays.sort(IntArray)

        String KuaDo = String.valueOf(IntArray[2] - IntArray[0])

        println("OpenDraw_KuaDo = " + KuaDo)
        println("RecommendResult_Bet = " + RecommendString_Bet + "_____")
        println("RecommendResult_Bet = " + (RecommendString_Bet.contains(KuaDo) ? "T" : "F"))

        ArrayList<String> RecommendArr = new ArrayList<String>(Arrays.asList(RecommendString_Bet.split(",")))
        if (RecommendArr.contains(KuaDo)) {
            IsCorrectAnalysis = true
            CorrectNum.append(KuaDo)
        } else {
            IsCorrectAnalysis = false
        }

//        if (IsCorrectAnalysis == true) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        return winInfo

        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }

    /***
     * 直选玩法
     */
    def getIssueWonInfo_ZhiXuanRecommend = { RecommendArray, IssueIndex, LastIssue ->

        boolean IsCorrectAnalysis = false
        StringBuffer CorrectNum = new StringBuffer()
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))

        if (DrawInfoItem == null) {
            return null
        }
        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue

        println("RecommendString = " + RecommendString_Bet)
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))
        ArrayList<String> RecommendLVCode = new ArrayList<String>(Arrays.asList(is11X5orPK10 ? RecommendString_Show.split("\\|") : RecommendString_Bet.split("\\|")))
        // 格式是1,1,1,1|2,2,2,2|3,3,3,3
        println("RecommendLVCode = " + RecommendLVCode.toString())

        int RecommendIndexCount = 0
        for (LV in LvS..LvE) {
            String OpenNum = String.valueOf(OpenCodeList.get(LV))

            String[] recommendArr = RecommendLVCode.get(RecommendIndexCount).split(",")
            ArrayList<String> RecommendNumCode = new ArrayList<String>(Arrays.asList(recommendArr))


            println("RecommendNumCode = " + RecommendNumCode.toString())
            println("OpenNum = " + OpenNum)
            if (RecommendNumCode.contains(OpenNum)) {
                IsCorrectAnalysis = true
                CorrectNum.append(OpenNum + "|")
            } else {// 有一个没有中 即没中
                IsCorrectAnalysis = false
                break
            }
            RecommendIndexCount++
        }
//        if (IsCorrectAnalysis == true) {
//            RecommendCorrectTime += 1
//            CorrectNum.setLength(CorrectNum.length() - 1)
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        return winInfo

        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }

    /***
     * 大小
     */
    def getIssueWonInfo_DaXiaoRecommend = { RecommendArray, IssueIndex, LastIssue ->
        boolean IsCorrectAnalysis = false

        StringBuffer CorrectNum = new StringBuffer()
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))

        if (DrawInfoItem == null) {
            return null
        }
        String OpenCode = DrawInfoItem.game_Code
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))
        String OpenIssue = DrawInfoItem.game_Issue

        for (int LV in LvS..LvE) {

            String OpenNum = NumDaXiaoDanZhung.DeterminDaXiao(currentCategoryCode, String.valueOf(OpenCodeList.get(LV)))
            if (RecommendString_Bet.contains(OpenNum)) {
                IsCorrectAnalysis = true
                CorrectNum.append(OpenNum)
            } else {// 有一个没有中 即没中
                IsCorrectAnalysis = false
                break
            }
        }
//        if (IsCorrectAnalysis == true) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        return winInfo


        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }

    /***
     * 大小單雙
     */
    def getIssueWonInfo_DanShuangRecommend = { RecommendArray, IssueIndex, LastIssue ->

        boolean IsCorrectAnalysis = false
        StringBuffer CorrectNum = new StringBuffer()
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }

        String OpenCode = DrawInfoItem.game_Code
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))
        String OpenIssue = DrawInfoItem.game_Issue

        for (LV in LvS..LvE) {
            String OpenNum = NumDaXiaoDanZhung.DeterminDanShuang(String.valueOf(OpenCodeList.get(LV)))
            if (RecommendString_Bet.contains(OpenNum)) {
                IsCorrectAnalysis = true
                CorrectNum.append(OpenNum)
            } else {// 有一个没有中 即没中
                IsCorrectAnalysis = false
                break
            }
        }
//        if (IsCorrectAnalysis == true) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        return winInfo


        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }

    /***
     * 這個是單一推薦碼 適用 後一 後二 後三 DAN SHI
     */
    def getIssueWonInfo_DanShiRecommend = { RecommendArray, IssueIndex, LastIssue ->

        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }
        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue

        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(OpenCode.split(",")))
        StringBuffer OpenCodeItem = new StringBuffer()
        for (i in LvS..LvE) {
            OpenCodeItem.append(OpenCodeList.get(i))
            OpenCodeItem.append(is11X5orPK10 ? " " : "")
        }

//        if (RecommendArray.contains(OpenCodeItem.toString().trim())) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + OpenCodeItem.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        println("____END_____")
//        return winInfo

        boolean IsCorrectAnalysis = RecommendArray.contains(OpenCodeItem.toString().trim())
        String WonStatus = OpenCode + "(${OpenCodeItem.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }

    /***
     * 适用直选
     */
    def getIssueWonInfo_ShuLiuOrSanRecommend = { RecommendArray, IssueIndex, LastIssue, IsZuSanType ->
        String ZuSanOrZuLiu = IsZuSanType ? "组三" : "组六"
        boolean IsCorrectAnalysis = false
        StringBuffer CorrectNum = new StringBuffer()
        OpenDrawInfo DrawInfoItem = OpenDrawMap.get(NumDaXiaoDanZhung.DFMT(IssueIndex))
        if (DrawInfoItem == null) {
            return null
        }
        ArrayList<String> OpenCodeList = new ArrayList<String>(Arrays.asList(DrawInfoItem.game_Code.split(",")))

        ArrayList<String> RecommendNumCode = new ArrayList<String>(Arrays.asList(RecommendString_Bet.split(",")))
        String OpenCode = DrawInfoItem.game_Code
        String OpenIssue = DrawInfoItem.game_Issue


        for (LV in LvS..LvE) {
            String OpenNum = String.valueOf(OpenCodeList.get(LV))
            if (RecommendNumCode.contains(OpenNum)) {
                IsCorrectAnalysis = true
                CorrectNum.append(OpenNum)
            } else {// 有一个没有中 即没中
                IsCorrectAnalysis = false
                break
            }
        }

        if (IsCorrectAnalysis) {
            String FirstN = String.valueOf(CorrectNum.charAt(0))
            String SecondN = String.valueOf(CorrectNum.charAt(1))
            String ThirdN = String.valueOf(CorrectNum.charAt(2))
            if (FirstN.equals(SecondN) && FirstN.equals(ThirdN) && SecondN.equals(ThirdN)) {
                // 豹子号
                IsCorrectAnalysis = false
            } else if (!FirstN.equals(SecondN) && !FirstN.equals(ThirdN) && !SecondN.equals(ThirdN)) {
                // 组六
                IsCorrectAnalysis = IsZuSanType ? false : true
            } else {
                // 组三
                IsCorrectAnalysis = IsZuSanType ? true : false
            }
        }

//        if (IsCorrectAnalysis) {
//            RecommendCorrectTime += 1
//            winInfo = WinOrLossInfoClosure.call(true, OpenCode + "(" + CorrectNum.toString() + ")", OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//        } else {
//            RecommendErrorTime += 1
//            if (RecommendCorrectTime == 0 && IssueIndex == LastIssue) {
//                winInfo = WinOrLossInfoClosure.call(false, OpenCode, OpenIssue, RecommendCorrectTime, RecommendErrorTime)
//            }
//        }
//        return winInfo


        String WonStatus = OpenCode + "(${CorrectNum.toString()})"
        return WinOrLossInfoClosure(IsCorrectAnalysis, WonStatus, OpenIssue, 0, 0)
    }

}
