package com.game_plan.Utils

import com.common.enumList.Enum_GameDefine
import com.game_plan.beans.BettingGameDetails
import com.game_plan.beans.GameItem
import com.game_plan.beans.OpenDrawInfo

import java.util.*

@Singleton
class NumTrendAnalysis {

    private String CurrentGameCategory
    private ArrayList<OpenDrawInfo> AllIssueInfo = new ArrayList<OpenDrawInfo>()
    def showLV = new int[2]
    def NumSAE = new int[2]
    def LvS = 0
    def LvE = 0
    def NumS = 0
    def NumE = 0


    boolean is11X5orPK10 = false

    void initData(GameItem GMITEM, ArrayList<OpenDrawInfo> AllIssueInfo) {
        this.CurrentGameCategory = GMITEM.getGameItem_CategoryCode()
        this.AllIssueInfo = AllIssueInfo
        this.showLV = GMITEM.getGameItem_AnalysisData_ShowLV()
        this.NumSAE = GMITEM.getGameItem_AnalysisData_ShowNum()
        this.LvS = showLV[0]
        this.LvE = showLV[1]
        this.NumS = NumSAE[0]
        this.NumE = NumSAE[1]


        is11X5orPK10 = CurrentGameCategory == Enum_GameDefine.GAME_11X5.getCode() || (CurrentGameCategory == Enum_GameDefine.GAME_PK10.getCode())

    }
    /***
     * 将开奖号资讯转换成号码列表
     *
     * @param AllIssueInfo
     * @return
     */
    private ArrayList<int[]> addOpenCodeArray(ArrayList<OpenDrawInfo> AllIssueInfo) {
        def OpenCodeArray = new ArrayList<int[]>()
        for (index in 0..AllIssueInfo.size() - 1) {
            def Code = AllIssueInfo.get(index).game_Code
            if (AllIssueInfo.get(index).game_Code == null) {
                continue
            }
            def codeArray = Code.split(",")
            def numbers = new int[codeArray.length]

            for (i in 0..codeArray.length - 1) {
                numbers[i] = Integer.parseInt(codeArray[i])
            }
            OpenCodeArray.add(numbers)
        }
        return OpenCodeArray
    }

    /**
     * 取得 最佳 号码 - 组选
     * @return
     */
    HashMap<Integer, BettingGameDetails> getBestChooseNumber_ZhuXuanResult() {
        ArrayList<int[]> CodeArray = addOpenCodeArray(AllIssueInfo)
        HashMap<String, HashMap<Integer, Integer>> ShowingTimesEachArray = NumberBallShowingTimes(CodeArray)
        HashMap<String, HashMap<Integer, Integer>> ShowingMaxMissingArray = NumberBallMaxMissing(CodeArray)
        HashMap<String, HashMap<Integer, Integer>> ShowingMaxContinuousShowArray = NumberBallMaxContinuousShowAmount(CodeArray)

        def Signal_RecommendNum_EachArray = new int[NumE + 1]

        for (LvIndex in LvS..LvE) {
            for (NbIndex in NumS..NumE) {// 只算到需要的个数
                double BeiShu = (NbIndex == 0 ? 5
                        : (NbIndex == 1 ? 4.5
                        : (NbIndex == 2 ? 4
                        : (NbIndex == 3 ? 3.5
                        : (NbIndex == 4 ? 3
                        : (NbIndex == 5 ? 2.5
                        : (NbIndex == 6 ? 2
                        : (NbIndex == 7 ? 1.5
                        : (NbIndex == 8 ? 1 : 0.5)))))))))
                def aaa = ShowingTimesEachArray.get(LvIndex)
                def bbb = aaa.get(NbIndex)
                Signal_RecommendNum_EachArray[bbb] += BeiShu

                Signal_RecommendNum_EachArray[ShowingMaxMissingArray.get(LvIndex).get(NbIndex)] += BeiShu

                Signal_RecommendNum_EachArray[ShowingMaxContinuousShowArray.get(LvIndex).get(NbIndex)] += BeiShu

                println("getBestChooseNumber_ZhuXuanResult[" + LvIndex + "][" + NbIndex + "] = "
                        + ShowingTimesEachArray.get(LvIndex).get(NbIndex))
            }
        }
        // 出现次数 排序 最少出现到 最大出现
        return SorttingValueToString(Signal_RecommendNum_EachArray, true)
    }

    /**
     * 取得 最佳 号码 - 直选
     * @return
     */
    HashMap<Integer, HashMap<Integer, BettingGameDetails>> getBestChooseNumber_MultiLV() {

        ArrayList<int[]> CodeArray = addOpenCodeArray(AllIssueInfo)
        HashMap<String, HashMap<Integer, Integer>> ShowingTimesEachArray = NumberBallShowingTimes(CodeArray)
        HashMap<String, HashMap<Integer, Integer>> ShowingMaxMissingArray = NumberBallMaxMissing(CodeArray)
        HashMap<String, HashMap<Integer, Integer>> ShowingMaxContinuousShowArray = NumberBallMaxContinuousShowAmount(CodeArray)

        def FinalResultArray = new ArrayList<int[]>()
        def WAN_RecommendNum_EachArray = new int[NumE + 1]
        def Qian_RecommendNum_EachArray = new int[NumE + 1]
        def Bai_RecommendNum_EachArray = new int[NumE + 1]
        def Shi_RecommendNum_EachArray = new int[NumE + 1]
        def Ge_RecommendNum_EachArray = new int[NumE + 1]

        for (LvIndex in LvS..LvE) {
            for (NbIndex in NumS..NumE) {// 只算到需要的个数
                double BeiShu = (NbIndex == 0 ? 5
                        : (NbIndex == 1 ? 4.5
                        : (NbIndex == 2 ? 4
                        : (NbIndex == 3 ? 3.5
                        : (NbIndex == 4 ? 3
                        : (NbIndex == 5 ? 2.5
                        : (NbIndex == 6 ? 2
                        : (NbIndex == 7 ? 1.5 : (NbIndex == 8 ? 1 : 0.5)))))))))
                switch (LvIndex) {// 万 千 百
                    case 0:// 万
                        WAN_RecommendNum_EachArray[ShowingTimesEachArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 1:
                        Qian_RecommendNum_EachArray[ShowingTimesEachArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 2:
                        Bai_RecommendNum_EachArray[ShowingTimesEachArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 3:
                        Shi_RecommendNum_EachArray[ShowingTimesEachArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 4:
                        Ge_RecommendNum_EachArray[ShowingTimesEachArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    default:
                        break
                }

                switch (LvIndex) {// 万 千 百
                    case 0:// 万
                        WAN_RecommendNum_EachArray[ShowingMaxMissingArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 1:
                        Qian_RecommendNum_EachArray[ShowingMaxMissingArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 2:
                        Bai_RecommendNum_EachArray[ShowingMaxMissingArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 3:
                        Shi_RecommendNum_EachArray[ShowingMaxMissingArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 4:
                        Ge_RecommendNum_EachArray[ShowingMaxMissingArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    default:
                        break
                }

                switch (LvIndex) {// 万 千 百
                    case 0:// 万
                        WAN_RecommendNum_EachArray[ShowingMaxContinuousShowArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 1:
                        Qian_RecommendNum_EachArray[ShowingMaxContinuousShowArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 2:
                        Bai_RecommendNum_EachArray[ShowingMaxContinuousShowArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 3:
                        Shi_RecommendNum_EachArray[ShowingMaxContinuousShowArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    case 4:
                        Ge_RecommendNum_EachArray[ShowingMaxContinuousShowArray.get(LvIndex).get(NbIndex)] += BeiShu
                        break
                    default:
                        break
                }
            }
        }
        FinalResultArray.add(WAN_RecommendNum_EachArray)
        FinalResultArray.add(Qian_RecommendNum_EachArray)
        FinalResultArray.add(Bai_RecommendNum_EachArray)
        FinalResultArray.add(Shi_RecommendNum_EachArray)
        FinalResultArray.add(Ge_RecommendNum_EachArray)

        // 出现次数 排序 最少出现到 最大出现
        return SorttingValueToStringList(FinalResultArray, true)

    }

    /**
     * 取得 最佳 号码 - 单式
     * @return
     */
    HashMap<Integer, BettingGameDetails> getBestChooseNumber_DanShiLV(int requireNum) {
        def DanShiResult = new HashMap<Integer, BettingGameDetails>()
        String Divider = is11X5orPK10 ? " " : ""
        HashMap<Integer, HashMap<Integer, BettingGameDetails>> SortingResult = getBestChooseNumber_MultiLV()

        switch (LvE - LvS) {
            case 1:
                // 代表 后二 前二 单式
                for (i in NumS..NumE) {
                    for (j in NumS..NumE) {
                        if (DanShiResult.size() >= requireNum) {
                            break
                        } else {
                            BettingGameDetails FirstNum = SortingResult.get(LvS).get(i)
                            BettingGameDetails LastNum = SortingResult.get(LvE).get(j)

                            String HaoMa_Bet = FirstNum.getNumber() + LastNum.getNumber()
                            String HaoMa_Show = FirstNum.getTitle() + Divider + LastNum.getTitle()


                            if (!DanShiResult.containsKey(HaoMa_Bet)) {
                                /***
                                 * 11選五 單式 不可重複
                                 * 不可為0
                                 */
                                if (is11X5orPK10) {
                                    if (FirstNum.getNumber() == LastNum.getNumber()) {
                                        continue
                                    } else if (Integer.parseInt(FirstNum.getNumber()) == 0 || Integer.parseInt(LastNum.getNumber()) == 0) {
                                        continue
                                    }
                                }
                                BettingGameDetails BettingGame = new BettingGameDetails()
                                BettingGame.is_active = true
                                BettingGame.is_select = false
                                BettingGame.number = HaoMa_Bet
                                BettingGame.Title = HaoMa_Show
                                DanShiResult.put(Integer.parseInt(HaoMa_Bet), BettingGame)
                            }
                        }
                    }
                }
                break
            case 2:
                // 代表后三前三单式
                for (i in NumS..NumE) {
                    for (j in NumS..NumE) {
                        for (k in NumS..NumE) {
                            if (DanShiResult.size() >= requireNum) {
                                break
                            } else {
                                BettingGameDetails FirstNum = SortingResult.get(LvS).get(i)
                                BettingGameDetails MidNum = SortingResult.get(LvS + 1).get(j)
                                BettingGameDetails LastNum = SortingResult.get(LvE).get(k)

                                String HaoMa_Bet = FirstNum.getNumber() + MidNum.getNumber() + LastNum.getNumber()
                                String HaoMa_Show = FirstNum.getTitle() + Divider + MidNum.getTitle() + Divider + LastNum.getTitle()

                                if (!DanShiResult.containsKey(HaoMa_Bet)) {
                                    /***
                                     * 11選五 單式 不可重複
                                     * 不可為0
                                     */
                                    if (is11X5orPK10) {
                                        if (FirstNum.getNumber() == MidNum.getNumber() ||
                                                FirstNum.getNumber() == LastNum.getNumber() ||
                                                MidNum.getNumber() == LastNum.getNumber()) {
                                            continue
                                        } else if (Integer.parseInt(FirstNum.getNumber()) == 0 ||
                                                Integer.parseInt(MidNum.getNumber()) == 0 ||
                                                Integer.parseInt(LastNum.getNumber()) == 0) {
                                            continue
                                        }
                                    }
                                    BettingGameDetails BettingGame = new BettingGameDetails()
                                    BettingGame.is_active = true
                                    BettingGame.is_select = false
                                    BettingGame.number = HaoMa_Bet
                                    BettingGame.Title = HaoMa_Show
                                    DanShiResult.put(Integer.parseInt(HaoMa_Bet), BettingGame)

                                }
                            }
                        }
                    }
                }
                break
            case 3:
                /***
                 * 11選五沒有四 五星單式
                 */
                // 代表四星单式
                for (h in NumS..NumE) {
                    for (i in NumS..NumE) {
                        for (j in NumS..NumE) {
                            for (k in NumS..NumE) {
                                if (DanShiResult.size() >= requireNum) {
                                    break;
                                } else {
                                    BettingGameDetails FirstNum = SortingResult.get(LvS).get(h)
                                    BettingGameDetails SecondNum = SortingResult.get(LvS + 1).get(i)
                                    BettingGameDetails ThirdNum = SortingResult.get(LvS + 2).get(j)
                                    BettingGameDetails FourthNum = SortingResult.get(LvE).get(k)

                                    String HaoMa_Bet = FirstNum.getNumber() + SecondNum.getNumber() + ThirdNum.getNumber() + FourthNum.getNumber()

                                    String HaoMa_Show = FirstNum.getTitle() + Divider + SecondNum.getTitle() + Divider + ThirdNum.getTitle() + Divider
                                    +FourthNum.getTitle()

                                    if (!DanShiResult.containsKey(HaoMa_Bet)) {
                                        BettingGameDetails BettingGame = new BettingGameDetails()
                                        BettingGame.is_active = true
                                        BettingGame.is_select = false
                                        BettingGame.number = HaoMa_Bet
                                        BettingGame.Title = HaoMa_Show
                                        DanShiResult.put(Integer.parseInt(HaoMa_Bet), BettingGame)

                                    }
                                }
                            }
                        }
                    }
                }
                break
            case 4:
                /***
                 * 11選五沒有四 五星單式
                 */
                // 代表五星单式
                for (e in NumS..NumE) {
                    for (h in NumS..NumE) {
                        for (i in NumS..NumE) {
                            for (j in NumS..NumE) {
                                for (k in NumS..NumE) {
                                    if (DanShiResult.size() >= requireNum) {
                                        break
                                    } else {
                                        BettingGameDetails FirstNum = SortingResult.get(LvS).get(e)
                                        BettingGameDetails SecondNum = SortingResult.get(LvS + 1).get(h)
                                        BettingGameDetails ThirdNum = SortingResult.get(LvS + 2).get(i)
                                        BettingGameDetails FourthNum = SortingResult.get(LvS + 3).get(j)
                                        BettingGameDetails FifthNum = SortingResult.get(LvE).get(k)

                                        String HaoMa_Bet = FirstNum.getNumber() + SecondNum.getNumber() + ThirdNum.getNumber() + FourthNum.getNumber() + FifthNum.getNumber()

                                        String HaoMa_Show = FirstNum.getTitle() + Divider + SecondNum.getTitle() + Divider + ThirdNum.getTitle() + Divider
                                        +FourthNum.getTitle() + Divider + FifthNum.getTitle()

                                        if (!DanShiResult.containsKey(HaoMa_Bet)) {
                                            BettingGameDetails BettingGame = new BettingGameDetails()
                                            BettingGame.is_active = true
                                            BettingGame.is_select = false
                                            BettingGame.number = HaoMa_Bet
                                            BettingGame.Title = HaoMa_Show
                                            DanShiResult.put(Integer.parseInt(HaoMa_Bet), BettingGame)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break
        }
        return DanShiResult
    }

/***
 * 將取得的號碼重新排序
 * @param BeginArray
 * @param UpgradeOrDowngrade
 * @return
 */
    private HashMap<Integer, BettingGameDetails> SorttingValueToString(int[] BeginArray,
                                                                       boolean UpgradeOrDowngrade) {
        // 出现次数 排序 最少出现到 最大出现
        Map<Integer, Integer> numberInfo = new HashMap<Integer, Integer>()

        for (i in 0..BeginArray.length - 1) {
            numberInfo.put(i, BeginArray[i])
        }
        if (UpgradeOrDowngrade) {
            numberInfo = numberInfo.sort { it.value }
        } else {
            numberInfo = numberInfo.sort { -it.value }
        }

        // 格式是这样
        // //println("FinalResult为 ＝" + linkedHashMap)
        // {
        // 0=1,
        // 3=1,
        // 7=1,
        // 9=1,
        // 2=2,
        // 6=3,
        // 8=3,
        // 4=4,
        // 5=4,
        // 1=5}//前面是位置
        // 后面是出现数量
//        String[] NumSortArray = numberInfo.toString().replace("{", "").replace("}", "").split(",")
        def NumIndex = NumS
        HashMap<Integer, BettingGameDetails> GameItem = new HashMap<Integer, BettingGameDetails>()
        numberInfo.each { key, value ->
            BettingGameDetails sortingItem = new BettingGameDetails()
            if (is11X5orPK10) {
                sortingItem.is_active = true
                sortingItem.number = String.format("%02d", key)
                sortingItem.Title = String.format("%02d", key)
            } else {

                sortingItem.is_active = true
                sortingItem.number = Integer.valueOf(key)
                sortingItem.Title = String.valueOf(key)
            }

            GameItem.put(NumIndex, sortingItem)
            NumIndex++
        }
        return GameItem
    }

    private HashMap<Integer, HashMap<Integer, BettingGameDetails>> SorttingValueToStringList(ArrayList<int[]> BeginArray, boolean UpgradeOrDowngrade) {
        // 出现次数 排序 最少出现到 最大出现
        def SortNumberGroupMap = new HashMap<Integer, HashMap<Integer, BettingGameDetails>>()
        for (LVindex in LvS..LvE) {

            def numberInfo = new HashMap<Integer, Integer>()

            int[] LVShow = BeginArray.get(LVindex)
            for (i in 0..LVShow.length - 1) {
                numberInfo.put(i, LVShow[i])
            }

            if (UpgradeOrDowngrade) {
                numberInfo = numberInfo.sort { it.value }
            } else {
                numberInfo = numberInfo.sort { -it.value }
            }

            // 格式是这样
            // //println("FinalResult为 ＝" + linkedHashMap)//
            // {0=1,
            // 3=1,
            // 7=1,
            // 9=1,
            // 2=2,
            // 6=3,
            // 8=3,
            // 4=4,
            // 5=4,
            // 1=5}//前面是位置
            // 后面是出现数量
            def NumIndex = NumS
            HashMap<Integer, BettingGameDetails> GameItem = new HashMap<Integer, BettingGameDetails>()
            numberInfo.each { key, value ->
                BettingGameDetails sortingItem = new BettingGameDetails()
                if (is11X5orPK10) {
                    sortingItem.is_active = true
                    sortingItem.number = String.format("%02d", key)
                    sortingItem.Title = String.format("%02d", key)
                } else {
                    sortingItem.is_active = true
                    sortingItem.number = Integer.valueOf(key)
                    sortingItem.Title = String.valueOf(key)
                }
                GameItem.put(NumIndex, sortingItem)
                NumIndex++
            }
            SortNumberGroupMap.put(LVindex, GameItem)
        }

        return SortNumberGroupMap
    }

    /****
     * -----------------------------------------------------------------------------------------------------------------------------------------
     * ---------------------------------------------以下計算遺漏 出現次數 及各方面運算---------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------------------------------
     * @param CodeArr
     * @return
     */

    /// 出现次数
    private HashMap<String, HashMap<Integer, Integer>> NumberBallShowingTimes(ArrayList<int[]> CodeArr) {
        println("出现次数")
        // 有可能 0~9, 1~11(11选五),1~6(快三)
        def NumberShowTimesArray = new HashMap<Integer, int[]>()

        def LV0_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV1_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV2_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV3_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV4_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV5_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV6_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV7_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV8_BallShowingTimes_EachArray = new int[NumE + 1]
        def LV9_BallShowingTimes_EachArray = new int[NumE + 1]

        for (openNumLv in 0..CodeArr.size() - 1) {
            int[] codeString = CodeArr.get(openNumLv)// 第1層 1,2,3,4,5 万一 千二 百三 十四 个五
            for (danwei in 0..codeString.length - 1) {
                switch (danwei) {// 万 千 百
                    case 0:// 万
                        LV0_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 1:
                        LV1_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 2:
                        LV2_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 3:
                        LV3_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 4:
                        LV4_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 5:
                        LV5_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 6:
                        LV6_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 7:
                        LV7_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 8:
                        LV8_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    case 9:
                        LV9_BallShowingTimes_EachArray[codeString[danwei]] += 1
                        break
                    default:
                        break
                }
            }
        }

        NumberShowTimesArray.put(0, LV0_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(1, LV1_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(2, LV2_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(3, LV3_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(4, LV4_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(5, LV5_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(6, LV6_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(7, LV7_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(8, LV8_BallShowingTimes_EachArray)
        NumberShowTimesArray.put(9, LV9_BallShowingTimes_EachArray)

        NumberShowTimesArray.each { index, value ->
            println("第" + index + "层")
            int[] debug = NumberShowTimesArray.get(index)
            for (int number : debug) {
                println("Number = " + number)
            }
        }
        println("END")
        return SorttingValueToIntArray(NumberShowTimesArray, true)
    }

    /// 最大遗漏值
    private HashMap<String, HashMap<Integer, Integer>> NumberBallMaxMissing(ArrayList<int[]> CodeArr) {
        println("最大遗漏值")
        def NumberMaxMissingArray = new HashMap<Integer, int[]>()

        def LV0_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV1_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV2_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV3_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV4_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV5_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV6_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV7_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV8_BallMaxMissing_EachArray = new int[NumE + 1]
        def LV9_BallMaxMissing_EachArray = new int[NumE + 1]


        for (openNumLv in 0..CodeArr.size() - 1) {
            int[] codeString = CodeArr.get(openNumLv)// 第1層 1,2,3,4,5 万一 千二 百三 十四
            // 个五
            for (danwei in 0..codeString.length - 1) {
                switch (danwei) {// 万 千 百
                    case 0:// 万
                        for (allNum in 0..LV0_BallMaxMissing_EachArray.length - 1) {
                            LV0_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV0_BallMaxMissing_EachArray[codeString[danwei]] = 0// 当万某一个数被选到时，归零
                        break
                    case 1:// 千

                        for (allNum in 0..LV1_BallMaxMissing_EachArray.length - 1) {
                            LV1_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV1_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 2:// 百
                        for (allNum in 0..LV2_BallMaxMissing_EachArray.length - 1) {
                            LV2_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV2_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 3:// 十
                        for (allNum in 0..LV3_BallMaxMissing_EachArray.length - 1) {
                            LV3_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV3_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 4:// 个
                        for (allNum in 0..LV4_BallMaxMissing_EachArray.length - 1) {
                            LV4_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV4_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 5:
                        for (allNum in 0..LV5_BallMaxMissing_EachArray.length - 1) {
                            LV5_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV5_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 6:
                        for (allNum in 0..LV6_BallMaxMissing_EachArray.length - 1) {
                            LV6_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV6_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 7:
                        for (allNum in 0..LV7_BallMaxMissing_EachArray.length - 1) {
                            LV7_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV7_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 8:
                        for (allNum in 0..LV8_BallMaxMissing_EachArray.length - 1) {
                            LV8_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV8_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    case 9:
                        for (allNum in 0..LV9_BallMaxMissing_EachArray.length - 1) {
                            LV9_BallMaxMissing_EachArray[allNum] += 1// 当万某一个数被选到时，归零
                        }
                        LV9_BallMaxMissing_EachArray[codeString[danwei]] = 0
                        break
                    default:
                        break
                }
            }
        }

        NumberMaxMissingArray.put(0, LV0_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(1, LV1_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(2, LV2_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(3, LV3_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(4, LV4_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(5, LV5_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(6, LV6_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(7, LV7_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(8, LV8_BallMaxMissing_EachArray)
        NumberMaxMissingArray.put(9, LV9_BallMaxMissing_EachArray)

        NumberMaxMissingArray.each { index, value ->
            println("第" + index + "层")
            int[] debug = NumberMaxMissingArray.get(index)
            for (int number : debug) {
                println("Number = " + number)
            }
        }
        return SorttingValueToIntArray(NumberMaxMissingArray, false)
    }

    /// 最大遗漏值
    private HashMap<String, HashMap<Integer, Integer>> NumberBallMaxContinuousShowAmount(ArrayList<int[]> CodeArr) {
        println("最大连出号值")
        def NumberMaxContinuousArray = new HashMap<Integer, int[]>()

        def LastNum_LV0, LastNum_LV1, LastNum_LV2, LastNum_LV3, LastNum_LV4 = -1// 记录
        def LastNum_LV5, LastNum_LV6, LastNum_LV7, LastNum_LV8, LastNum_LV9 = -1// 记录


        def LV0_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV1_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV2_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV3_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV4_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV5_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV6_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV7_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV8_BallMaxContinuous_EachArray = new int[NumE + 1]
        def LV9_BallMaxContinuous_EachArray = new int[NumE + 1]


        for (openNumLv in 0..CodeArr.size() - 1) {
            int[] codeString = CodeArr.get(openNumLv)
            // 第1層 1,2,3,4,5 万一 千二 百三 十四 个五
            for (danwei in 0..codeString.length - 1) {
                switch (danwei) {// 万 千 百
                    case 0:// 万

                        if (LV0_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV0) {
                            // 如果被选到的 等于 上一期 开奖的号马 连出值加一
                            LV0_BallMaxContinuous_EachArray[codeString[danwei]] += 1// 当万某一个数被选到时，归零
                        } else {
                            if (LV0_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV0_BallMaxContinuous_EachArray[codeString[danwei]] = 1// 当万某一个数被选到时，并先前没有被重复选取时，连出为1
                            } else {
                                // 最大连出值不变
                            }
                        }
                        // 更换上一期号码
                        LastNum_LV0 = LV0_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 1:// 千

                        if (LV1_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV1) {
                            // 如果被选到的 等于 上一期 开奖的号马 连出值加一
                            LV1_BallMaxContinuous_EachArray[codeString[danwei]] += 1// 当万某一个数被选到时，归零
                        } else {
                            if (LV1_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV1_BallMaxContinuous_EachArray[codeString[danwei]] = 1// 当万某一个数被选到时，并先前没有被重复选取时，连出为1
                            } else {
                                // 最大连出值不变
                            }
                        }
                        // 更换上一期号码
                        LastNum_LV1 = LV1_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 2:// 百

                        if (LV2_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV2) {
                            // 如果被选到的 等于 上一期 开奖的号马 连出值加一
                            LV2_BallMaxContinuous_EachArray[codeString[danwei]] += 1// 当万某一个数被选到时，归零
                        } else {
                            if (LV2_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV2_BallMaxContinuous_EachArray[codeString[danwei]] = 1// 当万某一个数被选到时，并先前没有被重复选取时，连出为1
                            } else {
                                // 最大连出值不变
                            }
                        }
                        // 更换上一期号码
                        LastNum_LV2 = LV2_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 3:// 十

                        if (LV3_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV3) {
                            // 如果被选到的 等于 上一期 开奖的号马 连出值加一
                            LV3_BallMaxContinuous_EachArray[codeString[danwei]] += 1// 当万某一个数被选到时，归零
                        } else {
                            if (LV3_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV3_BallMaxContinuous_EachArray[codeString[danwei]] = 1// 当万某一个数被选到时，并先前没有被重复选取时，连出为1
                            } else {
                                // 最大连出值不变
                            }
                        }
                        // 更换上一期号码
                        LastNum_LV3 = LV3_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 4:// 个

                        if (LV4_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV4) {
                            // 如果被选到的 等于 上一期 开奖的号马 连出值加一
                            LV4_BallMaxContinuous_EachArray[codeString[danwei]] += 1// 当万某一个数被选到时，归零
                        } else {
                            if (LV4_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV4_BallMaxContinuous_EachArray[codeString[danwei]] = 1// 当万某一个数被选到时，并先前没有被重复选取时，连出为1
                            } else {
                                // 最大连出值不变
                            }
                        }
                        // 更换上一期号码
                        LastNum_LV4 = LV4_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 5:

                        if (LV5_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV5) {
                            LV5_BallMaxContinuous_EachArray[codeString[danwei]] += 1
                        } else {
                            if (LV5_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV5_BallMaxContinuous_EachArray[codeString[danwei]] = 1
                            } else {
                            }
                        }
                        LastNum_LV5 = LV5_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 6:

                        if (LV6_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV6) {
                            LV6_BallMaxContinuous_EachArray[codeString[danwei]] += 1
                        } else {
                            if (LV6_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV6_BallMaxContinuous_EachArray[codeString[danwei]] = 1
                            } else {
                            }
                        }
                        LastNum_LV6 = LV6_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 7:

                        if (LV7_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV7) {
                            LV7_BallMaxContinuous_EachArray[codeString[danwei]] += 1
                        } else {
                            if (LV7_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV7_BallMaxContinuous_EachArray[codeString[danwei]] = 1
                            } else {
                            }
                        }
                        LastNum_LV7 = LV7_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 8:

                        if (LV8_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV8) {
                            LV8_BallMaxContinuous_EachArray[codeString[danwei]] += 1
                        } else {
                            if (LV8_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV8_BallMaxContinuous_EachArray[codeString[danwei]] = 1
                            } else {
                            }
                        }
                        LastNum_LV8 = LV8_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    case 9:

                        if (LV9_BallMaxContinuous_EachArray[codeString[danwei]] == LastNum_LV9) {
                            LV9_BallMaxContinuous_EachArray[codeString[danwei]] += 1
                        } else {
                            if (LV9_BallMaxContinuous_EachArray[codeString[danwei]] <= 1) {
                                LV9_BallMaxContinuous_EachArray[codeString[danwei]] = 1
                            } else {
                            }
                        }
                        LastNum_LV9 = LV9_BallMaxContinuous_EachArray[codeString[danwei]]
                        break
                    default:
                        break
                }
            }
        }

        NumberMaxContinuousArray.put(0, LV0_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(1, LV1_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(2, LV2_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(3, LV3_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(4, LV4_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(5, LV5_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(6, LV6_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(7, LV7_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(8, LV8_BallMaxContinuous_EachArray)
        NumberMaxContinuousArray.put(9, LV9_BallMaxContinuous_EachArray)

        NumberMaxContinuousArray.each { index, value ->
            println("第" + index + "层")
            int[] debug = NumberMaxContinuousArray.get(index)
            for (int number : debug) {
                println("Number = " + number)
            }
        }
        return SorttingValueToIntArray(NumberMaxContinuousArray, false)
    }

    private HashMap<String, HashMap<Integer, Integer>> SorttingValueToIntArray(HashMap<Integer, int[]> BeginArray, boolean UpgradeOrDowngrade) {
        // 出现次数 排序 最少出现到 最大出现
        def ShowNumSortEachArray = new HashMap<String, HashMap<Integer, Integer>>()
        for (LVindex in LvS..LvE) {
            def numberInfo = new HashMap<>()

            int[] LVShow = BeginArray.get(LVindex)
            for (i in 0..LVShow.length - 1) {
                numberInfo.put(i, LVShow[i])
            }

            if (UpgradeOrDowngrade) {
                numberInfo = numberInfo.sort { it.value }
            } else {
                numberInfo = numberInfo.sort { -it.value }
            }
            // 格式是这样
            // //println("FinalResult为 ＝" + linkedHashMap)//
            // {0=1,
            // 3=1,
            // 7=1,
            // 9=1,
            // 2=2,
            // 6=3,
            // 8=3,
            // 4=4,
            // 5=4,
            // 1=5}//前面是位置
            // 后面是出现数量
            def NumIndex = NumS
            HashMap<Integer, Integer> NumSortInLV = new HashMap<Integer, Integer>()
            numberInfo.each { key, value ->
                NumSortInLV.put(NumIndex, key)
                NumIndex++
            }
            ShowNumSortEachArray.put(LVindex, NumSortInLV)
        }
        return ShowNumSortEachArray
    }
}
