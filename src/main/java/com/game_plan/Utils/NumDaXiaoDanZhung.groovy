package com.game_plan.Utils

import com.game_plan.EnumList.Common_Method_Define

/**
 * Created by cjay on 2017/2/27.
 */
class NumDaXiaoDanZhung {

    static String DFMT(double d) {
        if (d == (long) d)
            return String.format("%d", (long) d)
        else
            return String.format("%s", d)
    }

    static String DeterminDaXiao(String GameCategory, String Num) {
        //
        if (GameCategory.contains(Common_Method_Define.GAME_K3.toString())) {
            if (Integer.valueOf(Num) >= 11) {
                return "大"
            } else {
                return "小"
            }
        } else if (GameCategory.contains(Common_Method_Define.GAME_SSC.toString())) {
            if (Integer.valueOf(Num) >= 5) {
                return "大"
            } else {
                return "小"
            }
        } else {
            if (Integer.valueOf(Num) >= 5) {
                return "大"
            } else {
                return "小"
            }
        }

        // } catch (NumberFormatException ex) {
        // return "小"
        // }
    }

    static String DeterminDanShuang(String Num) {
        //
        if (Integer.valueOf(Num) % 2 == 0) {
            return "双"
        } else {
            return "单"
        }
        // } catch (NumberFormatException ex) {
        // return "小"
        // }
    }

    static String DeterminShuSanOrLiu(String Num) {
        String FirstNumber = String.valueOf(Num.charAt(0))
        String MiddleNumber = String.valueOf(Num.charAt(1))
        String LastNumber = String.valueOf(Num.charAt(2))
        if (FirstNumber.equals(MiddleNumber) && FirstNumber.equals(LastNumber)) {
            return "豹子"
        } else if ((!FirstNumber.equals(MiddleNumber)) && (!FirstNumber.equals(LastNumber))) {
            return "组六"
        } else {
            return "组三"
        }
    }
}
